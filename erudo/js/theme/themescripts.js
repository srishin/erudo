/*
 * jQuery Superfish Menu Plugin - v1.7.4
 * Copyright (c) 2013 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 *	http://www.opensource.org/licenses/mit-license.php
 *	http://www.gnu.org/licenses/gpl.html
 */

;
(function($) {
    "use strict";

    var methods = (function() {
        // private properties and methods go here
        var c = {
                bcClass: 'sf-breadcrumb',
                menuClass: 'sf-js-enabled',
                anchorClass: 'sf-with-ul',
                menuArrowClass: 'sf-arrows'
            },
            ios = (function() {
                var ios = /iPhone|iPad|iPod/i.test(navigator.userAgent);
                if (ios) {
                    // iOS clicks only bubble as far as body children
                    $(window).load(function() {
                        $('body').children().on('click', $.noop);
                    });
                }
                return ios;
            })(),
            wp7 = (function() {
                var style = document.documentElement.style;
                return ('behavior' in style && 'fill' in style && /iemobile/i.test(navigator.userAgent));
            })(),
            toggleMenuClasses = function($menu, o) {
                var classes = c.menuClass;
                if (o.cssArrows) {
                    classes += ' ' + c.menuArrowClass;
                }
                $menu.toggleClass(classes);
            },
            setPathToCurrent = function($menu, o) {
                return $menu.find('li.' + o.pathClass).slice(0, o.pathLevels)
                    .addClass(o.hoverClass + ' ' + c.bcClass)
                    .filter(function() {
                        return ($(this).children(o.popUpSelector).hide().show().length);
                    }).removeClass(o.pathClass);
            },
            toggleAnchorClass = function($li) {
                $li.children('a').toggleClass(c.anchorClass);
            },
            toggleTouchAction = function($menu) {
                var touchAction = $menu.css('ms-touch-action');
                touchAction = (touchAction === 'pan-y') ? 'auto' : 'pan-y';
                $menu.css('ms-touch-action', touchAction);
            },
            applyHandlers = function($menu, o) {
                var targets = 'li:has(' + o.popUpSelector + ')';
                if ($.fn.hoverIntent && !o.disableHI) {
                    $menu.hoverIntent(over, out, targets);
                } else {
                    $menu
                        .on('mouseenter.superfish', targets, over)
                        .on('mouseleave.superfish', targets, out);
                }
                var touchevent = 'MSPointerDown.superfish';
                if (!ios) {
                    touchevent += ' touchend.superfish';
                }
                if (wp7) {
                    touchevent += ' mousedown.superfish';
                }
                $menu
                    .on('focusin.superfish', 'li', over)
                    .on('focusout.superfish', 'li', out)
                    .on(touchevent, 'a', o, touchHandler);
            },
            touchHandler = function(e) {
                var $this = $(this),
                    $ul = $this.siblings(e.data.popUpSelector);

                if ($ul.length > 0 && $ul.is(':hidden')) {
                    $this.one('click.superfish', false);
                    if (e.type === 'MSPointerDown') {
                        $this.trigger('focus');
                    } else {
                        $.proxy(over, $this.parent('li'))();
                    }
                }
            },
            over = function() {
                var $this = $(this),
                    o = getOptions($this);
                clearTimeout(o.sfTimer);
                $this.siblings().superfish('hide').end().superfish('show');
            },
            out = function() {
                var $this = $(this),
                    o = getOptions($this);
                if (ios) {
                    $.proxy(close, $this, o)();
                } else {
                    clearTimeout(o.sfTimer);
                    o.sfTimer = setTimeout($.proxy(close, $this, o), o.delay);
                }
            },
            close = function(o) {
                o.retainPath = ($.inArray(this[0], o.$path) > -1);
                this.superfish('hide');

                if (!this.parents('.' + o.hoverClass).length) {
                    o.onIdle.call(getMenu(this));
                    if (o.$path.length) {
                        $.proxy(over, o.$path)();
                    }
                }
            },
            getMenu = function($el) {
                return $el.closest('.' + c.menuClass);
            },
            getOptions = function($el) {
                return getMenu($el).data('sf-options');
            };

        return {
            // public methods
            hide: function(instant) {
                if (this.length) {
                    var $this = this,
                        o = getOptions($this);
                    if (!o) {
                        return this;
                    }
                    var not = (o.retainPath === true) ? o.$path : '',
                        $ul = $this.find('li.' + o.hoverClass).add(this).not(not).removeClass(o.hoverClass).children(o.popUpSelector),
                        speed = o.speedOut;

                    if (instant) {
                        $ul.show();
                        speed = 0;
                    }
                    o.retainPath = false;
                    o.onBeforeHide.call($ul);
                    $ul.stop(true, true).animate(o.animationOut, speed, function() {
                        var $this = $(this);
                        o.onHide.call($this);
                    });
                }
                return this;
            },
            show: function() {
                var o = getOptions(this);
                if (!o) {
                    return this;
                }
                var $this = this.addClass(o.hoverClass),
                    $ul = $this.children(o.popUpSelector);

                o.onBeforeShow.call($ul);
                $ul.stop(true, true).animate(o.animation, o.speed, function() {
                    o.onShow.call($ul);
                });
                return this;
            },
            destroy: function() {
                return this.each(function() {
                    var $this = $(this),
                        o = $this.data('sf-options'),
                        $hasPopUp;
                    if (!o) {
                        return false;
                    }
                    $hasPopUp = $this.find(o.popUpSelector).parent('li');
                    clearTimeout(o.sfTimer);
                    toggleMenuClasses($this, o);
                    toggleAnchorClass($hasPopUp);
                    toggleTouchAction($this);
                    // remove event handlers
                    $this.off('.superfish').off('.hoverIntent');
                    // clear animation's inline display style
                    $hasPopUp.children(o.popUpSelector).attr('style', function(i, style) {
                        return style.replace(/display[^;]+;?/g, '');
                    });
                    // reset 'current' path classes
                    o.$path.removeClass(o.hoverClass + ' ' + c.bcClass).addClass(o.pathClass);
                    $this.find('.' + o.hoverClass).removeClass(o.hoverClass);
                    o.onDestroy.call($this);
                    $this.removeData('sf-options');
                });
            },
            init: function(op) {
                return this.each(function() {
                    var $this = $(this);
                    if ($this.data('sf-options')) {
                        return false;
                    }
                    var o = $.extend({}, $.fn.superfish.defaults, op),
                        $hasPopUp = $this.find(o.popUpSelector).parent('li');
                    o.$path = setPathToCurrent($this, o);

                    $this.data('sf-options', o);

                    toggleMenuClasses($this, o);
                    toggleAnchorClass($hasPopUp);
                    toggleTouchAction($this);
                    applyHandlers($this, o);

                    $hasPopUp.not('.' + c.bcClass).superfish('hide', true);

                    o.onInit.call(this);
                });
            }
        };
    })();

    $.fn.superfish = function(method, args) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            return $.error('Method ' + method + ' does not exist on jQuery.fn.superfish');
        }
    };

    $.fn.superfish.defaults = {
        popUpSelector: 'ul,.sf-mega', // within menu context
        hoverClass: 'sfHover',
        pathClass: 'overrideThisToUse',
        pathLevels: 1,
        delay: 800,
        animation: {
            opacity: 'show'
        },
        animationOut: {
            opacity: 'hide'
        },
        speed: 'normal',
        speedOut: 'fast',
        cssArrows: true,
        disableHI: false,
        onInit: $.noop,
        onBeforeShow: $.noop,
        onShow: $.noop,
        onBeforeHide: $.noop,
        onHide: $.noop,
        onIdle: $.noop,
        onDestroy: $.noop
    };

    // soon to be deprecated
    $.fn.extend({
        hideSuperfishUl: methods.hide,
        showSuperfishUl: methods.show
    });

})(jQuery);
(function($) {

    $.fn.spasticNav = function(options) {

        options = $.extend({
            overlap: 0,
            speed: 500,
            reset: 50,
            color: '#00c6ff',
            easing: 'swing' //'easeOutExpo'
        }, options);

        return this.each(function() {

            var nav = $(this),
                currentPageItem = nav.find('>.current-menu-item,>.current-menu-parent,>.current-menu-ancestor'), //>.current_page_parent
                hidden = true, //false
                blob,
                reset;
            if (currentPageItem.length === 0) {
                currentPageItem = nav.find('li').eq(0);
                //hidden = true;
            }

            $('<li id="blob"></li>').css({
                width: currentPageItem.css('width'), //.outerWidth(),
                height: currentPageItem.css('height'), //.outerHeight() + options.overlap,
                left: currentPageItem.position().left,
                top: currentPageItem.position().top - options.overlap / 2,
                backgroundColor: hidden ? options.color : currentPageItem.find('a').css('backgroundColor'),
                opacity: hidden ? 0 : 1
            }).appendTo(this);
            blob = $('#blob', nav);

            nav.find('>li:not(#blob)').hover(function() {
                // mouse over
                clearTimeout(reset);
                var bg = $(this).css('backgroundColor');
                $(this).addClass('blob_over');
                blob.css({
                    backgroundColor: bg
                }).animate({
                    left: $(this).position().left,
                    top: $(this).position().top - options.overlap / 2,
                    width: $(this).css('width'), //.outerWidth(),
                    height: $(this).css('height') + options.overlap, //.outerHeight() + options.overlap,
                    opacity: 1
                }, {
                    duration: options.speed,
                    easing: options.easing,
                    queue: false
                });
            }, function() {
                // mouse out	
                reset = setTimeout(function() {
                    /*
                    var a = currentPageItem.find('a');
                    var bg = a.css('backgroundColor');
                    */
                    blob.animate({
                            /*
                            width : currentPageItem.outerWidth(),
                            left : currentPageItem.position().left,
                            */
                            opacity: 0 //hidden ? 0 : 1,
                        }, options.speed)
                        //.css({backgroundColor: bg})
                }, options.reset);
                $(this).removeClass('blob_over');
            });


        }); // end each

    };

})(jQuery);
/*!
 * Isotope PACKAGED v2.0.0
 * Filter & sort magical layouts
 * http://isotope.metafizzy.co
 */

(function(t) {
    function e() {}

    function i(t) {
        function i(e) {
            e.prototype.option || (e.prototype.option = function(e) {
                t.isPlainObject(e) && (this.options = t.extend(!0, this.options, e))
            })
        }

        function n(e, i) {
            t.fn[e] = function(n) {
                if ("string" == typeof n) {
                    for (var s = o.call(arguments, 1), a = 0, u = this.length; u > a; a++) {
                        var p = this[a],
                            h = t.data(p, e);
                        if (h)
                            if (t.isFunction(h[n]) && "_" !== n.charAt(0)) {
                                var f = h[n].apply(h, s);
                                if (void 0 !== f) return f
                            } else r("no such method '" + n + "' for " + e + " instance");
                        else r("cannot call methods on " + e + " prior to initialization; " + "attempted to call '" + n + "'")
                    }
                    return this
                }
                return this.each(function() {
                    var o = t.data(this, e);
                    o ? (o.option(n), o._init()) : (o = new i(this, n), t.data(this, e, o))
                })
            }
        }
        if (t) {
            var r = "undefined" == typeof console ? e : function(t) {
                console.error(t)
            };
            return t.bridget = function(t, e) {
                i(e), n(t, e)
            }, t.bridget
        }
    }
    var o = Array.prototype.slice;
    "function" == typeof define && define.amd ? define("jquery-bridget/jquery.bridget", ["jquery"], i) : i(t.jQuery)
})(window),
function(t) {
    function e(e) {
        var i = t.event;
        return i.target = i.target || i.srcElement || e, i
    }
    var i = document.documentElement,
        o = function() {};
    i.addEventListener ? o = function(t, e, i) {
        t.addEventListener(e, i, !1)
    } : i.attachEvent && (o = function(t, i, o) {
        t[i + o] = o.handleEvent ? function() {
            var i = e(t);
            o.handleEvent.call(o, i)
        } : function() {
            var i = e(t);
            o.call(t, i)
        }, t.attachEvent("on" + i, t[i + o])
    });
    var n = function() {};
    i.removeEventListener ? n = function(t, e, i) {
        t.removeEventListener(e, i, !1)
    } : i.detachEvent && (n = function(t, e, i) {
        t.detachEvent("on" + e, t[e + i]);
        try {
            delete t[e + i]
        } catch (o) {
            t[e + i] = void 0
        }
    });
    var r = {
        bind: o,
        unbind: n
    };
    "function" == typeof define && define.amd ? define("eventie/eventie", r) : "object" == typeof exports ? module.exports = r : t.eventie = r
}(this),
function(t) {
    function e(t) {
        "function" == typeof t && (e.isReady ? t() : r.push(t))
    }

    function i(t) {
        var i = "readystatechange" === t.type && "complete" !== n.readyState;
        if (!e.isReady && !i) {
            e.isReady = !0;
            for (var o = 0, s = r.length; s > o; o++) {
                var a = r[o];
                a()
            }
        }
    }

    function o(o) {
        return o.bind(n, "DOMContentLoaded", i), o.bind(n, "readystatechange", i), o.bind(t, "load", i), e
    }
    var n = t.document,
        r = [];
    e.isReady = !1, "function" == typeof define && define.amd ? (e.isReady = "function" == typeof requirejs, define("doc-ready/doc-ready", ["eventie/eventie"], o)) : t.docReady = o(t.eventie)
}(this),
function() {
    function t() {}

    function e(t, e) {
        for (var i = t.length; i--;)
            if (t[i].listener === e) return i;
        return -1
    }

    function i(t) {
        return function() {
            return this[t].apply(this, arguments)
        }
    }
    var o = t.prototype,
        n = this,
        r = n.EventEmitter;
    o.getListeners = function(t) {
        var e, i, o = this._getEvents();
        if (t instanceof RegExp) {
            e = {};
            for (i in o) o.hasOwnProperty(i) && t.test(i) && (e[i] = o[i])
        } else e = o[t] || (o[t] = []);
        return e
    }, o.flattenListeners = function(t) {
        var e, i = [];
        for (e = 0; t.length > e; e += 1) i.push(t[e].listener);
        return i
    }, o.getListenersAsObject = function(t) {
        var e, i = this.getListeners(t);
        return i instanceof Array && (e = {}, e[t] = i), e || i
    }, o.addListener = function(t, i) {
        var o, n = this.getListenersAsObject(t),
            r = "object" == typeof i;
        for (o in n) n.hasOwnProperty(o) && -1 === e(n[o], i) && n[o].push(r ? i : {
            listener: i,
            once: !1
        });
        return this
    }, o.on = i("addListener"), o.addOnceListener = function(t, e) {
        return this.addListener(t, {
            listener: e,
            once: !0
        })
    }, o.once = i("addOnceListener"), o.defineEvent = function(t) {
        return this.getListeners(t), this
    }, o.defineEvents = function(t) {
        for (var e = 0; t.length > e; e += 1) this.defineEvent(t[e]);
        return this
    }, o.removeListener = function(t, i) {
        var o, n, r = this.getListenersAsObject(t);
        for (n in r) r.hasOwnProperty(n) && (o = e(r[n], i), -1 !== o && r[n].splice(o, 1));
        return this
    }, o.off = i("removeListener"), o.addListeners = function(t, e) {
        return this.manipulateListeners(!1, t, e)
    }, o.removeListeners = function(t, e) {
        return this.manipulateListeners(!0, t, e)
    }, o.manipulateListeners = function(t, e, i) {
        var o, n, r = t ? this.removeListener : this.addListener,
            s = t ? this.removeListeners : this.addListeners;
        if ("object" != typeof e || e instanceof RegExp)
            for (o = i.length; o--;) r.call(this, e, i[o]);
        else
            for (o in e) e.hasOwnProperty(o) && (n = e[o]) && ("function" == typeof n ? r.call(this, o, n) : s.call(this, o, n));
        return this
    }, o.removeEvent = function(t) {
        var e, i = typeof t,
            o = this._getEvents();
        if ("string" === i) delete o[t];
        else if (t instanceof RegExp)
            for (e in o) o.hasOwnProperty(e) && t.test(e) && delete o[e];
        else delete this._events;
        return this
    }, o.removeAllListeners = i("removeEvent"), o.emitEvent = function(t, e) {
        var i, o, n, r, s = this.getListenersAsObject(t);
        for (n in s)
            if (s.hasOwnProperty(n))
                for (o = s[n].length; o--;) i = s[n][o], i.once === !0 && this.removeListener(t, i.listener), r = i.listener.apply(this, e || []), r === this._getOnceReturnValue() && this.removeListener(t, i.listener);
        return this
    }, o.trigger = i("emitEvent"), o.emit = function(t) {
        var e = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(t, e)
    }, o.setOnceReturnValue = function(t) {
        return this._onceReturnValue = t, this
    }, o._getOnceReturnValue = function() {
        return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
    }, o._getEvents = function() {
        return this._events || (this._events = {})
    }, t.noConflict = function() {
        return n.EventEmitter = r, t
    }, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function() {
        return t
    }) : "object" == typeof module && module.exports ? module.exports = t : this.EventEmitter = t
}.call(this),
    function(t) {
        function e(t) {
            if (t) {
                if ("string" == typeof o[t]) return t;
                t = t.charAt(0).toUpperCase() + t.slice(1);
                for (var e, n = 0, r = i.length; r > n; n++)
                    if (e = i[n] + t, "string" == typeof o[e]) return e
            }
        }
        var i = "Webkit Moz ms Ms O".split(" "),
            o = document.documentElement.style;
        "function" == typeof define && define.amd ? define("get-style-property/get-style-property", [], function() {
            return e
        }) : "object" == typeof exports ? module.exports = e : t.getStyleProperty = e
    }(window),
    function(t) {
        function e(t) {
            var e = parseFloat(t),
                i = -1 === t.indexOf("%") && !isNaN(e);
            return i && e
        }

        function i() {
            for (var t = {
                    width: 0,
                    height: 0,
                    innerWidth: 0,
                    innerHeight: 0,
                    outerWidth: 0,
                    outerHeight: 0
                }, e = 0, i = s.length; i > e; e++) {
                var o = s[e];
                t[o] = 0
            }
            return t
        }

        function o(t) {
            function o(t) {
                if ("string" == typeof t && (t = document.querySelector(t)), t && "object" == typeof t && t.nodeType) {
                    var o = r(t);
                    if ("none" === o.display) return i();
                    var n = {};
                    n.width = t.offsetWidth, n.height = t.offsetHeight;
                    for (var h = n.isBorderBox = !(!p || !o[p] || "border-box" !== o[p]), f = 0, c = s.length; c > f; f++) {
                        var d = s[f],
                            l = o[d];
                        l = a(t, l);
                        var y = parseFloat(l);
                        n[d] = isNaN(y) ? 0 : y
                    }
                    var m = n.paddingLeft + n.paddingRight,
                        g = n.paddingTop + n.paddingBottom,
                        v = n.marginLeft + n.marginRight,
                        _ = n.marginTop + n.marginBottom,
                        I = n.borderLeftWidth + n.borderRightWidth,
                        L = n.borderTopWidth + n.borderBottomWidth,
                        z = h && u,
                        S = e(o.width);
                    S !== !1 && (n.width = S + (z ? 0 : m + I));
                    var b = e(o.height);
                    return b !== !1 && (n.height = b + (z ? 0 : g + L)), n.innerWidth = n.width - (m + I), n.innerHeight = n.height - (g + L), n.outerWidth = n.width + v, n.outerHeight = n.height + _, n
                }
            }

            function a(t, e) {
                if (n || -1 === e.indexOf("%")) return e;
                var i = t.style,
                    o = i.left,
                    r = t.runtimeStyle,
                    s = r && r.left;
                return s && (r.left = t.currentStyle.left), i.left = e, e = i.pixelLeft, i.left = o, s && (r.left = s), e
            }
            var u, p = t("boxSizing");
            return function() {
                if (p) {
                    var t = document.createElement("div");
                    t.style.width = "200px", t.style.padding = "1px 2px 3px 4px", t.style.borderStyle = "solid", t.style.borderWidth = "1px 2px 3px 4px", t.style[p] = "border-box";
                    var i = document.body || document.documentElement;
                    i.appendChild(t);
                    var o = r(t);
                    u = 200 === e(o.width), i.removeChild(t)
                }
            }(), o
        }
        var n = t.getComputedStyle,
            r = n ? function(t) {
                return n(t, null)
            } : function(t) {
                return t.currentStyle
            },
            s = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
        "function" == typeof define && define.amd ? define("get-size/get-size", ["get-style-property/get-style-property"], o) : "object" == typeof exports ? module.exports = o(require("get-style-property")) : t.getSize = o(t.getStyleProperty)
    }(window),
    function(t, e) {
        function i(t, e) {
            return t[a](e)
        }

        function o(t) {
            if (!t.parentNode) {
                var e = document.createDocumentFragment();
                e.appendChild(t)
            }
        }

        function n(t, e) {
            o(t);
            for (var i = t.parentNode.querySelectorAll(e), n = 0, r = i.length; r > n; n++)
                if (i[n] === t) return !0;
            return !1
        }

        function r(t, e) {
            return o(t), i(t, e)
        }
        var s, a = function() {
            if (e.matchesSelector) return "matchesSelector";
            for (var t = ["webkit", "moz", "ms", "o"], i = 0, o = t.length; o > i; i++) {
                var n = t[i],
                    r = n + "MatchesSelector";
                if (e[r]) return r
            }
        }();
        if (a) {
            var u = document.createElement("div"),
                p = i(u, "div");
            s = p ? i : r
        } else s = n;
        "function" == typeof define && define.amd ? define("matches-selector/matches-selector", [], function() {
            return s
        }) : window.matchesSelector = s
    }(this, Element.prototype),
    function(t) {
        function e(t, e) {
            for (var i in e) t[i] = e[i];
            return t
        }

        function i(t) {
            for (var e in t) return !1;
            return e = null, !0
        }

        function o(t) {
            return t.replace(/([A-Z])/g, function(t) {
                return "-" + t.toLowerCase()
            })
        }

        function n(t, n, r) {
            function a(t, e) {
                t && (this.element = t, this.layout = e, this.position = {
                    x: 0,
                    y: 0
                }, this._create())
            }
            var u = r("transition"),
                p = r("transform"),
                h = u && p,
                f = !!r("perspective"),
                c = {
                    WebkitTransition: "webkitTransitionEnd",
                    MozTransition: "transitionend",
                    OTransition: "otransitionend",
                    transition: "transitionend"
                }[u],
                d = ["transform", "transition", "transitionDuration", "transitionProperty"],
                l = function() {
                    for (var t = {}, e = 0, i = d.length; i > e; e++) {
                        var o = d[e],
                            n = r(o);
                        n && n !== o && (t[o] = n)
                    }
                    return t
                }();
            e(a.prototype, t.prototype), a.prototype._create = function() {
                this._transn = {
                    ingProperties: {},
                    clean: {},
                    onEnd: {}
                }, this.css({
                    position: "absolute"
                })
            }, a.prototype.handleEvent = function(t) {
                var e = "on" + t.type;
                this[e] && this[e](t)
            }, a.prototype.getSize = function() {
                this.size = n(this.element)
            }, a.prototype.css = function(t) {
                var e = this.element.style;
                for (var i in t) {
                    var o = l[i] || i;
                    e[o] = t[i]
                }
            }, a.prototype.getPosition = function() {
                var t = s(this.element),
                    e = this.layout.options,
                    i = e.isOriginLeft,
                    o = e.isOriginTop,
                    n = parseInt(t[i ? "left" : "right"], 10),
                    r = parseInt(t[o ? "top" : "bottom"], 10);
                n = isNaN(n) ? 0 : n, r = isNaN(r) ? 0 : r;
                var a = this.layout.size;
                n -= i ? a.paddingLeft : a.paddingRight, r -= o ? a.paddingTop : a.paddingBottom, this.position.x = n, this.position.y = r
            }, a.prototype.layoutPosition = function() {
                var t = this.layout.size,
                    e = this.layout.options,
                    i = {};
                e.isOriginLeft ? (i.left = this.position.x + t.paddingLeft + "px", i.right = "") : (i.right = this.position.x + t.paddingRight + "px", i.left = ""), e.isOriginTop ? (i.top = this.position.y + t.paddingTop + "px", i.bottom = "") : (i.bottom = this.position.y + t.paddingBottom + "px", i.top = ""), this.css(i), this.emitEvent("layout", [this])
            };
            var y = f ? function(t, e) {
                return "translate3d(" + t + "px, " + e + "px, 0)"
            } : function(t, e) {
                return "translate(" + t + "px, " + e + "px)"
            };
            a.prototype._transitionTo = function(t, e) {
                this.getPosition();
                var i = this.position.x,
                    o = this.position.y,
                    n = parseInt(t, 10),
                    r = parseInt(e, 10),
                    s = n === this.position.x && r === this.position.y;
                if (this.setPosition(t, e), s && !this.isTransitioning) return this.layoutPosition(), void 0;
                var a = t - i,
                    u = e - o,
                    p = {},
                    h = this.layout.options;
                a = h.isOriginLeft ? a : -a, u = h.isOriginTop ? u : -u, p.transform = y(a, u), this.transition({
                    to: p,
                    onTransitionEnd: {
                        transform: this.layoutPosition
                    },
                    isCleaning: !0
                })
            }, a.prototype.goTo = function(t, e) {
                this.setPosition(t, e), this.layoutPosition()
            }, a.prototype.moveTo = h ? a.prototype._transitionTo : a.prototype.goTo, a.prototype.setPosition = function(t, e) {
                this.position.x = parseInt(t, 10), this.position.y = parseInt(e, 10)
            }, a.prototype._nonTransition = function(t) {
                this.css(t.to), t.isCleaning && this._removeStyles(t.to);
                for (var e in t.onTransitionEnd) t.onTransitionEnd[e].call(this)
            }, a.prototype._transition = function(t) {
                if (!parseFloat(this.layout.options.transitionDuration)) return this._nonTransition(t), void 0;
                var e = this._transn;
                for (var i in t.onTransitionEnd) e.onEnd[i] = t.onTransitionEnd[i];
                for (i in t.to) e.ingProperties[i] = !0, t.isCleaning && (e.clean[i] = !0);
                if (t.from) {
                    this.css(t.from);
                    var o = this.element.offsetHeight;
                    o = null
                }
                this.enableTransition(t.to), this.css(t.to), this.isTransitioning = !0
            };
            var m = p && o(p) + ",opacity";
            a.prototype.enableTransition = function() {
                this.isTransitioning || (this.css({
                    transitionProperty: m,
                    transitionDuration: this.layout.options.transitionDuration
                }), this.element.addEventListener(c, this, !1))
            }, a.prototype.transition = a.prototype[u ? "_transition" : "_nonTransition"], a.prototype.onwebkitTransitionEnd = function(t) {
                this.ontransitionend(t)
            }, a.prototype.onotransitionend = function(t) {
                this.ontransitionend(t)
            };
            var g = {
                "-webkit-transform": "transform",
                "-moz-transform": "transform",
                "-o-transform": "transform"
            };
            a.prototype.ontransitionend = function(t) {
                if (t.target === this.element) {
                    var e = this._transn,
                        o = g[t.propertyName] || t.propertyName;
                    if (delete e.ingProperties[o], i(e.ingProperties) && this.disableTransition(), o in e.clean && (this.element.style[t.propertyName] = "", delete e.clean[o]), o in e.onEnd) {
                        var n = e.onEnd[o];
                        n.call(this), delete e.onEnd[o]
                    }
                    this.emitEvent("transitionEnd", [this])
                }
            }, a.prototype.disableTransition = function() {
                this.removeTransitionStyles(), this.element.removeEventListener(c, this, !1), this.isTransitioning = !1
            }, a.prototype._removeStyles = function(t) {
                var e = {};
                for (var i in t) e[i] = "";
                this.css(e)
            };
            var v = {
                transitionProperty: "",
                transitionDuration: ""
            };
            return a.prototype.removeTransitionStyles = function() {
                this.css(v)
            }, a.prototype.removeElem = function() {
                this.element.parentNode.removeChild(this.element), this.emitEvent("remove", [this])
            }, a.prototype.remove = function() {
                if (!u || !parseFloat(this.layout.options.transitionDuration)) return this.removeElem(), void 0;
                var t = this;
                this.on("transitionEnd", function() {
                    return t.removeElem(), !0
                }), this.hide()
            }, a.prototype.reveal = function() {
                delete this.isHidden, this.css({
                    display: ""
                });
                var t = this.layout.options;
                this.transition({
                    from: t.hiddenStyle,
                    to: t.visibleStyle,
                    isCleaning: !0
                })
            }, a.prototype.hide = function() {
                this.isHidden = !0, this.css({
                    display: ""
                });
                var t = this.layout.options;
                this.transition({
                    from: t.visibleStyle,
                    to: t.hiddenStyle,
                    isCleaning: !0,
                    onTransitionEnd: {
                        opacity: function() {
                            this.isHidden && this.css({
                                display: "none"
                            })
                        }
                    }
                })
            }, a.prototype.destroy = function() {
                this.css({
                    position: "",
                    left: "",
                    right: "",
                    top: "",
                    bottom: "",
                    transition: "",
                    transform: ""
                })
            }, a
        }
        var r = t.getComputedStyle,
            s = r ? function(t) {
                return r(t, null)
            } : function(t) {
                return t.currentStyle
            };
        "function" == typeof define && define.amd ? define("outlayer/item", ["eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property"], n) : (t.Outlayer = {}, t.Outlayer.Item = n(t.EventEmitter, t.getSize, t.getStyleProperty))
    }(window),
    function(t) {
        function e(t, e) {
            for (var i in e) t[i] = e[i];
            return t
        }

        function i(t) {
            return "[object Array]" === f.call(t)
        }

        function o(t) {
            var e = [];
            if (i(t)) e = t;
            else if (t && "number" == typeof t.length)
                for (var o = 0, n = t.length; n > o; o++) e.push(t[o]);
            else e.push(t);
            return e
        }

        function n(t, e) {
            var i = d(e, t); - 1 !== i && e.splice(i, 1)
        }

        function r(t) {
            return t.replace(/(.)([A-Z])/g, function(t, e, i) {
                return e + "-" + i
            }).toLowerCase()
        }

        function s(i, s, f, d, l, y) {
            function m(t, i) {
                if ("string" == typeof t && (t = a.querySelector(t)), !t || !c(t)) return u && u.error("Bad " + this.constructor.namespace + " element: " + t), void 0;
                this.element = t, this.options = e({}, this.constructor.defaults), this.option(i);
                var o = ++g;
                this.element.outlayerGUID = o, v[o] = this, this._create(), this.options.isInitLayout && this.layout()
            }
            var g = 0,
                v = {};
            return m.namespace = "outlayer", m.Item = y, m.defaults = {
                containerStyle: {
                    position: "relative"
                },
                isInitLayout: !0,
                isOriginLeft: !0,
                isOriginTop: !0,
                isResizeBound: !0,
                isResizingContainer: !0,
                transitionDuration: "0.4s",
                hiddenStyle: {
                    opacity: 0,
                    transform: "scale(0.001)"
                },
                visibleStyle: {
                    opacity: 1,
                    transform: "scale(1)"
                }
            }, e(m.prototype, f.prototype), m.prototype.option = function(t) {
                e(this.options, t)
            }, m.prototype._create = function() {
                this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), e(this.element.style, this.options.containerStyle), this.options.isResizeBound && this.bindResize()
            }, m.prototype.reloadItems = function() {
                this.items = this._itemize(this.element.children)
            }, m.prototype._itemize = function(t) {
                for (var e = this._filterFindItemElements(t), i = this.constructor.Item, o = [], n = 0, r = e.length; r > n; n++) {
                    var s = e[n],
                        a = new i(s, this);
                    o.push(a)
                }
                return o
            }, m.prototype._filterFindItemElements = function(t) {
                t = o(t);
                for (var e = this.options.itemSelector, i = [], n = 0, r = t.length; r > n; n++) {
                    var s = t[n];
                    if (c(s))
                        if (e) {
                            l(s, e) && i.push(s);
                            for (var a = s.querySelectorAll(e), u = 0, p = a.length; p > u; u++) i.push(a[u])
                        } else i.push(s)
                }
                return i
            }, m.prototype.getItemElements = function() {
                for (var t = [], e = 0, i = this.items.length; i > e; e++) t.push(this.items[e].element);
                return t
            }, m.prototype.layout = function() {
                this._resetLayout(), this._manageStamps();
                var t = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
                this.layoutItems(this.items, t), this._isLayoutInited = !0
            }, m.prototype._init = m.prototype.layout, m.prototype._resetLayout = function() {
                this.getSize()
            }, m.prototype.getSize = function() {
                this.size = d(this.element)
            }, m.prototype._getMeasurement = function(t, e) {
                var i, o = this.options[t];
                o ? ("string" == typeof o ? i = this.element.querySelector(o) : c(o) && (i = o), this[t] = i ? d(i)[e] : o) : this[t] = 0
            }, m.prototype.layoutItems = function(t, e) {
                t = this._getItemsForLayout(t), this._layoutItems(t, e), this._postLayout()
            }, m.prototype._getItemsForLayout = function(t) {
                for (var e = [], i = 0, o = t.length; o > i; i++) {
                    var n = t[i];
                    n.isIgnored || e.push(n)
                }
                return e
            }, m.prototype._layoutItems = function(t, e) {
                function i() {
                    o.emitEvent("layoutComplete", [o, t])
                }
                var o = this;
                if (!t || !t.length) return i(), void 0;
                this._itemsOn(t, "layout", i);
                for (var n = [], r = 0, s = t.length; s > r; r++) {
                    var a = t[r],
                        u = this._getItemLayoutPosition(a);
                    u.item = a, u.isInstant = e || a.isLayoutInstant, n.push(u)
                }
                this._processLayoutQueue(n)
            }, m.prototype._getItemLayoutPosition = function() {
                return {
                    x: 0,
                    y: 0
                }
            }, m.prototype._processLayoutQueue = function(t) {
                for (var e = 0, i = t.length; i > e; e++) {
                    var o = t[e];
                    this._positionItem(o.item, o.x, o.y, o.isInstant)
                }
            }, m.prototype._positionItem = function(t, e, i, o) {
                o ? t.goTo(e, i) : t.moveTo(e, i)
            }, m.prototype._postLayout = function() {
                this.resizeContainer()
            }, m.prototype.resizeContainer = function() {
                if (this.options.isResizingContainer) {
                    var t = this._getContainerSize();
                    t && (this._setContainerMeasure(t.width, !0), this._setContainerMeasure(t.height, !1))
                }
            }, m.prototype._getContainerSize = h, m.prototype._setContainerMeasure = function(t, e) {
                if (void 0 !== t) {
                    var i = this.size;
                    i.isBorderBox && (t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth), t = Math.max(t, 0), this.element.style[e ? "width" : "height"] = t + "px"
                }
            }, m.prototype._itemsOn = function(t, e, i) {
                function o() {
                    return n++, n === r && i.call(s), !0
                }
                for (var n = 0, r = t.length, s = this, a = 0, u = t.length; u > a; a++) {
                    var p = t[a];
                    p.on(e, o)
                }
            }, m.prototype.ignore = function(t) {
                var e = this.getItem(t);
                e && (e.isIgnored = !0)
            }, m.prototype.unignore = function(t) {
                var e = this.getItem(t);
                e && delete e.isIgnored
            }, m.prototype.stamp = function(t) {
                if (t = this._find(t)) {
                    this.stamps = this.stamps.concat(t);
                    for (var e = 0, i = t.length; i > e; e++) {
                        var o = t[e];
                        this.ignore(o)
                    }
                }
            }, m.prototype.unstamp = function(t) {
                if (t = this._find(t))
                    for (var e = 0, i = t.length; i > e; e++) {
                        var o = t[e];
                        n(o, this.stamps), this.unignore(o)
                    }
            }, m.prototype._find = function(t) {
                return t ? ("string" == typeof t && (t = this.element.querySelectorAll(t)), t = o(t)) : void 0
            }, m.prototype._manageStamps = function() {
                if (this.stamps && this.stamps.length) {
                    this._getBoundingRect();
                    for (var t = 0, e = this.stamps.length; e > t; t++) {
                        var i = this.stamps[t];
                        this._manageStamp(i)
                    }
                }
            }, m.prototype._getBoundingRect = function() {
                var t = this.element.getBoundingClientRect(),
                    e = this.size;
                this._boundingRect = {
                    left: t.left + e.paddingLeft + e.borderLeftWidth,
                    top: t.top + e.paddingTop + e.borderTopWidth,
                    right: t.right - (e.paddingRight + e.borderRightWidth),
                    bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth)
                }
            }, m.prototype._manageStamp = h, m.prototype._getElementOffset = function(t) {
                var e = t.getBoundingClientRect(),
                    i = this._boundingRect,
                    o = d(t),
                    n = {
                        left: e.left - i.left - o.marginLeft,
                        top: e.top - i.top - o.marginTop,
                        right: i.right - e.right - o.marginRight,
                        bottom: i.bottom - e.bottom - o.marginBottom
                    };
                return n
            }, m.prototype.handleEvent = function(t) {
                var e = "on" + t.type;
                this[e] && this[e](t)
            }, m.prototype.bindResize = function() {
                this.isResizeBound || (i.bind(t, "resize", this), this.isResizeBound = !0)
            }, m.prototype.unbindResize = function() {
                this.isResizeBound && i.unbind(t, "resize", this), this.isResizeBound = !1
            }, m.prototype.onresize = function() {
                function t() {
                    e.resize(), delete e.resizeTimeout
                }
                this.resizeTimeout && clearTimeout(this.resizeTimeout);
                var e = this;
                this.resizeTimeout = setTimeout(t, 100)
            }, m.prototype.resize = function() {
                this.isResizeBound && this.needsResizeLayout() && this.layout()
            }, m.prototype.needsResizeLayout = function() {
                var t = d(this.element),
                    e = this.size && t;
                return e && t.innerWidth !== this.size.innerWidth
            }, m.prototype.addItems = function(t) {
                var e = this._itemize(t);
                return e.length && (this.items = this.items.concat(e)), e
            }, m.prototype.appended = function(t) {
                var e = this.addItems(t);
                e.length && (this.layoutItems(e, !0), this.reveal(e))
            }, m.prototype.prepended = function(t) {
                var e = this._itemize(t);
                if (e.length) {
                    var i = this.items.slice(0);
                    this.items = e.concat(i), this._resetLayout(), this._manageStamps(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(i)
                }
            }, m.prototype.reveal = function(t) {
                var e = t && t.length;
                if (e)
                    for (var i = 0; e > i; i++) {
                        var o = t[i];
                        o.reveal()
                    }
            }, m.prototype.hide = function(t) {
                var e = t && t.length;
                if (e)
                    for (var i = 0; e > i; i++) {
                        var o = t[i];
                        o.hide()
                    }
            }, m.prototype.getItem = function(t) {
                for (var e = 0, i = this.items.length; i > e; e++) {
                    var o = this.items[e];
                    if (o.element === t) return o
                }
            }, m.prototype.getItems = function(t) {
                if (t && t.length) {
                    for (var e = [], i = 0, o = t.length; o > i; i++) {
                        var n = t[i],
                            r = this.getItem(n);
                        r && e.push(r)
                    }
                    return e
                }
            }, m.prototype.remove = function(t) {
                t = o(t);
                var e = this.getItems(t);
                if (e && e.length) {
                    this._itemsOn(e, "remove", function() {
                        this.emitEvent("removeComplete", [this, e])
                    });
                    for (var i = 0, r = e.length; r > i; i++) {
                        var s = e[i];
                        s.remove(), n(s, this.items)
                    }
                }
            }, m.prototype.destroy = function() {
                var t = this.element.style;
                t.height = "", t.position = "", t.width = "";
                for (var e = 0, i = this.items.length; i > e; e++) {
                    var o = this.items[e];
                    o.destroy()
                }
                this.unbindResize(), delete this.element.outlayerGUID, p && p.removeData(this.element, this.constructor.namespace)
            }, m.data = function(t) {
                var e = t && t.outlayerGUID;
                return e && v[e]
            }, m.create = function(t, i) {
                function o() {
                    m.apply(this, arguments)
                }
                return Object.create ? o.prototype = Object.create(m.prototype) : e(o.prototype, m.prototype), o.prototype.constructor = o, o.defaults = e({}, m.defaults), e(o.defaults, i), o.prototype.settings = {}, o.namespace = t, o.data = m.data, o.Item = function() {
                    y.apply(this, arguments)
                }, o.Item.prototype = new y, s(function() {
                    for (var e = r(t), i = a.querySelectorAll(".js-" + e), n = "data-" + e + "-options", s = 0, h = i.length; h > s; s++) {
                        var f, c = i[s],
                            d = c.getAttribute(n);
                        try {
                            f = d && JSON.parse(d)
                        } catch (l) {
                            u && u.error("Error parsing " + n + " on " + c.nodeName.toLowerCase() + (c.id ? "#" + c.id : "") + ": " + l);
                            continue
                        }
                        var y = new o(c, f);
                        p && p.data(c, t, y)
                    }
                }), p && p.bridget && p.bridget(t, o), o
            }, m.Item = y, m
        }
        var a = t.document,
            u = t.console,
            p = t.jQuery,
            h = function() {},
            f = Object.prototype.toString,
            c = "object" == typeof HTMLElement ? function(t) {
                return t instanceof HTMLElement
            } : function(t) {
                return t && "object" == typeof t && 1 === t.nodeType && "string" == typeof t.nodeName
            },
            d = Array.prototype.indexOf ? function(t, e) {
                return t.indexOf(e)
            } : function(t, e) {
                for (var i = 0, o = t.length; o > i; i++)
                    if (t[i] === e) return i;
                return -1
            };
        "function" == typeof define && define.amd ? define("outlayer/outlayer", ["eventie/eventie", "doc-ready/doc-ready", "eventEmitter/EventEmitter", "get-size/get-size", "matches-selector/matches-selector", "./item"], s) : t.Outlayer = s(t.eventie, t.docReady, t.EventEmitter, t.getSize, t.matchesSelector, t.Outlayer.Item)
    }(window),
    function(t) {
        function e(t) {
            function e() {
                t.Item.apply(this, arguments)
            }
            return e.prototype = new t.Item, e.prototype._create = function() {
                this.id = this.layout.itemGUID++, t.Item.prototype._create.call(this), this.sortData = {}
            }, e.prototype.updateSortData = function() {
                if (!this.isIgnored) {
                    this.sortData.id = this.id, this.sortData["original-order"] = this.id, this.sortData.random = Math.random();
                    var t = this.layout.options.getSortData,
                        e = this.layout._sorters;
                    for (var i in t) {
                        var o = e[i];
                        this.sortData[i] = o(this.element, this)
                    }
                }
            }, e
        }
        "function" == typeof define && define.amd ? define("isotope/js/item", ["outlayer/outlayer"], e) : (t.Isotope = t.Isotope || {}, t.Isotope.Item = e(t.Outlayer))
    }(window),
    function(t) {
        function e(t, e) {
            function i(t) {
                this.isotope = t, t && (this.options = t.options[this.namespace], this.element = t.element, this.items = t.filteredItems, this.size = t.size)
            }
            return function() {
                function t(t) {
                    return function() {
                        return e.prototype[t].apply(this.isotope, arguments)
                    }
                }
                for (var o = ["_resetLayout", "_getItemLayoutPosition", "_manageStamp", "_getContainerSize", "_getElementOffset", "needsResizeLayout"], n = 0, r = o.length; r > n; n++) {
                    var s = o[n];
                    i.prototype[s] = t(s)
                }
            }(), i.prototype.needsVerticalResizeLayout = function() {
                var e = t(this.isotope.element),
                    i = this.isotope.size && e;
                return i && e.innerHeight !== this.isotope.size.innerHeight
            }, i.prototype._getMeasurement = function() {
                this.isotope._getMeasurement.apply(this, arguments)
            }, i.prototype.getColumnWidth = function() {
                this.getSegmentSize("column", "Width")
            }, i.prototype.getRowHeight = function() {
                this.getSegmentSize("row", "Height")
            }, i.prototype.getSegmentSize = function(t, e) {
                var i = t + e,
                    o = "outer" + e;
                if (this._getMeasurement(i, o), !this[i]) {
                    var n = this.getFirstItemSize();
                    this[i] = n && n[o] || this.isotope.size["inner" + e]
                }
            }, i.prototype.getFirstItemSize = function() {
                var e = this.isotope.filteredItems[0];
                return e && e.element && t(e.element)
            }, i.prototype.layout = function() {
                this.isotope.layout.apply(this.isotope, arguments)
            }, i.prototype.getSize = function() {
                this.isotope.getSize(), this.size = this.isotope.size
            }, i.modes = {}, i.create = function(t, e) {
                function o() {
                    i.apply(this, arguments)
                }
                return o.prototype = new i, e && (o.options = e), o.prototype.namespace = t, i.modes[t] = o, o
            }, i
        }
        "function" == typeof define && define.amd ? define("isotope/js/layout-mode", ["get-size/get-size", "outlayer/outlayer"], e) : (t.Isotope = t.Isotope || {}, t.Isotope.LayoutMode = e(t.getSize, t.Outlayer))
    }(window),
    function(t) {
        function e(t, e) {
            var o = t.create("masonry");
            return o.prototype._resetLayout = function() {
                this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns();
                var t = this.cols;
                for (this.colYs = []; t--;) this.colYs.push(0);
                this.maxY = 0
            }, o.prototype.measureColumns = function() {
                if (this.getContainerWidth(), !this.columnWidth) {
                    var t = this.items[0],
                        i = t && t.element;
                    this.columnWidth = i && e(i).outerWidth || this.containerWidth
                }
                this.columnWidth += this.gutter, this.cols = Math.floor((this.containerWidth + this.gutter) / this.columnWidth), this.cols = Math.max(this.cols, 1)
            }, o.prototype.getContainerWidth = function() {
                var t = this.options.isFitWidth ? this.element.parentNode : this.element,
                    i = e(t);
                this.containerWidth = i && i.innerWidth
            }, o.prototype._getItemLayoutPosition = function(t) {
                t.getSize();
                var e = t.size.outerWidth % this.columnWidth,
                    o = e && 1 > e ? "round" : "ceil",
                    n = Math[o](t.size.outerWidth / this.columnWidth);
                n = Math.min(n, this.cols);
                for (var r = this._getColGroup(n), s = Math.min.apply(Math, r), a = i(r, s), u = {
                        x: this.columnWidth * a,
                        y: s
                    }, p = s + t.size.outerHeight, h = this.cols + 1 - r.length, f = 0; h > f; f++) this.colYs[a + f] = p;
                return u
            }, o.prototype._getColGroup = function(t) {
                if (2 > t) return this.colYs;
                for (var e = [], i = this.cols + 1 - t, o = 0; i > o; o++) {
                    var n = this.colYs.slice(o, o + t);
                    e[o] = Math.max.apply(Math, n)
                }
                return e
            }, o.prototype._manageStamp = function(t) {
                var i = e(t),
                    o = this._getElementOffset(t),
                    n = this.options.isOriginLeft ? o.left : o.right,
                    r = n + i.outerWidth,
                    s = Math.floor(n / this.columnWidth);
                s = Math.max(0, s);
                var a = Math.floor(r / this.columnWidth);
                a -= r % this.columnWidth ? 0 : 1, a = Math.min(this.cols - 1, a);
                for (var u = (this.options.isOriginTop ? o.top : o.bottom) + i.outerHeight, p = s; a >= p; p++) this.colYs[p] = Math.max(u, this.colYs[p])
            }, o.prototype._getContainerSize = function() {
                this.maxY = Math.max.apply(Math, this.colYs);
                var t = {
                    height: this.maxY
                };
                return this.options.isFitWidth && (t.width = this._getContainerFitWidth()), t
            }, o.prototype._getContainerFitWidth = function() {
                for (var t = 0, e = this.cols; --e && 0 === this.colYs[e];) t++;
                return (this.cols - t) * this.columnWidth - this.gutter
            }, o.prototype.needsResizeLayout = function() {
                var t = this.containerWidth;
                return this.getContainerWidth(), t !== this.containerWidth
            }, o
        }
        var i = Array.prototype.indexOf ? function(t, e) {
            return t.indexOf(e)
        } : function(t, e) {
            for (var i = 0, o = t.length; o > i; i++) {
                var n = t[i];
                if (n === e) return i
            }
            return -1
        };
        "function" == typeof define && define.amd ? define("masonry/masonry", ["outlayer/outlayer", "get-size/get-size"], e) : t.Masonry = e(t.Outlayer, t.getSize)
    }(window),
    function(t) {
        function e(t, e) {
            for (var i in e) t[i] = e[i];
            return t
        }

        function i(t, i) {
            var o = t.create("masonry"),
                n = o.prototype._getElementOffset,
                r = o.prototype.layout,
                s = o.prototype._getMeasurement;
            e(o.prototype, i.prototype), o.prototype._getElementOffset = n, o.prototype.layout = r, o.prototype._getMeasurement = s;
            var a = o.prototype.measureColumns;
            o.prototype.measureColumns = function() {
                this.items = this.isotope.filteredItems, a.call(this)
            };
            var u = o.prototype._manageStamp;
            return o.prototype._manageStamp = function() {
                this.options.isOriginLeft = this.isotope.options.isOriginLeft, this.options.isOriginTop = this.isotope.options.isOriginTop, u.apply(this, arguments)
            }, o
        }
        "function" == typeof define && define.amd ? define("isotope/js/layout-modes/masonry", ["../layout-mode", "masonry/masonry"], i) : i(t.Isotope.LayoutMode, t.Masonry)
    }(window),
    function(t) {
        function e(t) {
            var e = t.create("fitRows");
            return e.prototype._resetLayout = function() {
                this.x = 0, this.y = 0, this.maxY = 0
            }, e.prototype._getItemLayoutPosition = function(t) {
                t.getSize(), 0 !== this.x && t.size.outerWidth + this.x > this.isotope.size.innerWidth && (this.x = 0, this.y = this.maxY);
                var e = {
                    x: this.x,
                    y: this.y
                };
                return this.maxY = Math.max(this.maxY, this.y + t.size.outerHeight), this.x += t.size.outerWidth, e
            }, e.prototype._getContainerSize = function() {
                return {
                    height: this.maxY
                }
            }, e
        }
        "function" == typeof define && define.amd ? define("isotope/js/layout-modes/fit-rows", ["../layout-mode"], e) : e(t.Isotope.LayoutMode)
    }(window),
    function(t) {
        function e(t) {
            var e = t.create("vertical", {
                horizontalAlignment: 0
            });
            return e.prototype._resetLayout = function() {
                this.y = 0
            }, e.prototype._getItemLayoutPosition = function(t) {
                t.getSize();
                var e = (this.isotope.size.innerWidth - t.size.outerWidth) * this.options.horizontalAlignment,
                    i = this.y;
                return this.y += t.size.outerHeight, {
                    x: e,
                    y: i
                }
            }, e.prototype._getContainerSize = function() {
                return {
                    height: this.y
                }
            }, e
        }
        "function" == typeof define && define.amd ? define("isotope/js/layout-modes/vertical", ["../layout-mode"], e) : e(t.Isotope.LayoutMode)
    }(window),
    function(t) {
        function e(t, e) {
            for (var i in e) t[i] = e[i];
            return t
        }

        function i(t) {
            return "[object Array]" === h.call(t)
        }

        function o(t) {
            var e = [];
            if (i(t)) e = t;
            else if (t && "number" == typeof t.length)
                for (var o = 0, n = t.length; n > o; o++) e.push(t[o]);
            else e.push(t);
            return e
        }

        function n(t, e) {
            var i = f(e, t); - 1 !== i && e.splice(i, 1)
        }

        function r(t, i, r, u, h) {
            function f(t, e) {
                return function(i, o) {
                    for (var n = 0, r = t.length; r > n; n++) {
                        var s = t[n],
                            a = i.sortData[s],
                            u = o.sortData[s];
                        if (a > u || u > a) {
                            var p = void 0 !== e[s] ? e[s] : e,
                                h = p ? 1 : -1;
                            return (a > u ? 1 : -1) * h
                        }
                    }
                    return 0
                }
            }
            var c = t.create("isotope", {
                layoutMode: "masonry",
                isJQueryFiltering: !0,
                sortAscending: !0
            });
            c.Item = u, c.LayoutMode = h, c.prototype._create = function() {
                this.itemGUID = 0, this._sorters = {}, this._getSorters(), t.prototype._create.call(this), this.modes = {}, this.filteredItems = this.items, this.sortHistory = ["original-order"];
                for (var e in h.modes) this._initLayoutMode(e)
            }, c.prototype.reloadItems = function() {
                this.itemGUID = 0, t.prototype.reloadItems.call(this)
            }, c.prototype._itemize = function() {
                for (var e = t.prototype._itemize.apply(this, arguments), i = 0, o = e.length; o > i; i++) {
                    var n = e[i];
                    n.id = this.itemGUID++
                }
                return this._updateItemsSortData(e), e
            }, c.prototype._initLayoutMode = function(t) {
                var i = h.modes[t],
                    o = this.options[t] || {};
                this.options[t] = i.options ? e(i.options, o) : o, this.modes[t] = new i(this)
            }, c.prototype.layout = function() {
                return !this._isLayoutInited && this.options.isInitLayout ? (this.arrange(), void 0) : (this._layout(), void 0)
            }, c.prototype._layout = function() {
                var t = this._getIsInstant();
                this._resetLayout(), this._manageStamps(), this.layoutItems(this.filteredItems, t), this._isLayoutInited = !0
            }, c.prototype.arrange = function(t) {
                this.option(t), this._getIsInstant(), this.filteredItems = this._filter(this.items), this._sort(), this._layout()
            }, c.prototype._init = c.prototype.arrange, c.prototype._getIsInstant = function() {
                var t = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
                return this._isInstant = t, t
            }, c.prototype._filter = function(t) {
                function e() {
                    f.reveal(n), f.hide(r)
                }
                var i = this.options.filter;
                i = i || "*";
                for (var o = [], n = [], r = [], s = this._getFilterTest(i), a = 0, u = t.length; u > a; a++) {
                    var p = t[a];
                    if (!p.isIgnored) {
                        var h = s(p);
                        h && o.push(p), h && p.isHidden ? n.push(p) : h || p.isHidden || r.push(p)
                    }
                }
                var f = this;
                return this._isInstant ? this._noTransition(e) : e(), o
            }, c.prototype._getFilterTest = function(t) {
                return s && this.options.isJQueryFiltering ? function(e) {
                    return s(e.element).is(t)
                } : "function" == typeof t ? function(e) {
                    return t(e.element)
                } : function(e) {
                    return r(e.element, t)
                }
            }, c.prototype.updateSortData = function(t) {
                this._getSorters(), t = o(t);
                var e = this.getItems(t);
                e = e.length ? e : this.items, this._updateItemsSortData(e)
            }, c.prototype._getSorters = function() {
                var t = this.options.getSortData;
                for (var e in t) {
                    var i = t[e];
                    this._sorters[e] = d(i)
                }
            }, c.prototype._updateItemsSortData = function(t) {
                for (var e = 0, i = t.length; i > e; e++) {
                    var o = t[e];
                    o.updateSortData()
                }
            };
            var d = function() {
                function t(t) {
                    if ("string" != typeof t) return t;
                    var i = a(t).split(" "),
                        o = i[0],
                        n = o.match(/^\[(.+)\]$/),
                        r = n && n[1],
                        s = e(r, o),
                        u = c.sortDataParsers[i[1]];
                    return t = u ? function(t) {
                        return t && u(s(t))
                    } : function(t) {
                        return t && s(t)
                    }
                }

                function e(t, e) {
                    var i;
                    return i = t ? function(e) {
                        return e.getAttribute(t)
                    } : function(t) {
                        var i = t.querySelector(e);
                        return i && p(i)
                    }
                }
                return t
            }();
            c.sortDataParsers = {
                parseInt: function(t) {
                    return parseInt(t, 10)
                },
                parseFloat: function(t) {
                    return parseFloat(t)
                }
            }, c.prototype._sort = function() {
                var t = this.options.sortBy;
                if (t) {
                    var e = [].concat.apply(t, this.sortHistory),
                        i = f(e, this.options.sortAscending);
                    this.filteredItems.sort(i), t !== this.sortHistory[0] && this.sortHistory.unshift(t)
                }
            }, c.prototype._mode = function() {
                var t = this.options.layoutMode,
                    e = this.modes[t];
                if (!e) throw Error("No layout mode: " + t);
                return e.options = this.options[t], e
            }, c.prototype._resetLayout = function() {
                t.prototype._resetLayout.call(this), this._mode()._resetLayout()
            }, c.prototype._getItemLayoutPosition = function(t) {
                return this._mode()._getItemLayoutPosition(t)
            }, c.prototype._manageStamp = function(t) {
                this._mode()._manageStamp(t)
            }, c.prototype._getContainerSize = function() {
                return this._mode()._getContainerSize()
            }, c.prototype.needsResizeLayout = function() {
                return this._mode().needsResizeLayout()
            }, c.prototype.appended = function(t) {
                var e = this.addItems(t);
                if (e.length) {
                    var i = this._filterRevealAdded(e);
                    this.filteredItems = this.filteredItems.concat(i)
                }
            }, c.prototype.prepended = function(t) {
                var e = this._itemize(t);
                if (e.length) {
                    var i = this.items.slice(0);
                    this.items = e.concat(i), this._resetLayout(), this._manageStamps();
                    var o = this._filterRevealAdded(e);
                    this.layoutItems(i), this.filteredItems = o.concat(this.filteredItems)
                }
            }, c.prototype._filterRevealAdded = function(t) {
                var e = this._noTransition(function() {
                    return this._filter(t)
                });
                return this.layoutItems(e, !0), this.reveal(e), t
            }, c.prototype.insert = function(t) {
                var e = this.addItems(t);
                if (e.length) {
                    var i, o, n = e.length;
                    for (i = 0; n > i; i++) o = e[i], this.element.appendChild(o.element);
                    var r = this._filter(e);
                    for (this._noTransition(function() {
                            this.hide(r)
                        }), i = 0; n > i; i++) e[i].isLayoutInstant = !0;
                    for (this.arrange(), i = 0; n > i; i++) delete e[i].isLayoutInstant;
                    this.reveal(r)
                }
            };
            var l = c.prototype.remove;
            return c.prototype.remove = function(t) {
                t = o(t);
                var e = this.getItems(t);
                if (l.call(this, t), e && e.length)
                    for (var i = 0, r = e.length; r > i; i++) {
                        var s = e[i];
                        n(s, this.filteredItems)
                    }
            }, c.prototype._noTransition = function(t) {
                var e = this.options.transitionDuration;
                this.options.transitionDuration = 0;
                var i = t.call(this);
                return this.options.transitionDuration = e, i
            }, c
        }
        var s = t.jQuery,
            a = String.prototype.trim ? function(t) {
                return t.trim()
            } : function(t) {
                return t.replace(/^\s+|\s+$/g, "")
            },
            u = document.documentElement,
            p = u.textContent ? function(t) {
                return t.textContent
            } : function(t) {
                return t.innerText
            },
            h = Object.prototype.toString,
            f = Array.prototype.indexOf ? function(t, e) {
                return t.indexOf(e)
            } : function(t, e) {
                for (var i = 0, o = t.length; o > i; i++)
                    if (t[i] === e) return i;
                return -1
            };
        "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size", "matches-selector/matches-selector", "isotope/js/item", "isotope/js/layout-mode", "isotope/js/layout-modes/masonry", "isotope/js/layout-modes/fit-rows", "isotope/js/layout-modes/vertical"], r) : t.Isotope = r(t.Outlayer, t.getSize, t.matchesSelector, t.Isotope.Item, t.Isotope.LayoutMode)
    }(window);
/**
 * jquery.hoverdir.js v1.1.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2012, Codrops
 * http://www.codrops.com
 */
;
(function($, window, undefined) {

    'use strict';

    $.HoverDir = function(options, element) {

        this.$el = $(element);
        this._init(options);
    };

    // the options
    $.HoverDir.defaults = {
        speed: 300,
        easing: 'ease',
        hoverDelay: 0,
        hoverElement: '.info',
        inverse: false
    };

    $.HoverDir.prototype = {

        _init: function(options) {

            // options
            this.options = $.extend(true, {}, $.HoverDir.defaults, options);
            // transition properties
            this.transitionProp = 'all ' + this.options.speed + 'ms ' + this.options.easing;
            // support for CSS transitions
            this.support = window.Modernizr ? Modernizr.csstransitions : false;
            // load the events
            this._loadEvents();

        },
        _loadEvents: function() {

            var self = this;

            this.$el.on('mouseenter.hoverdir, mouseleave.hoverdir', function(event) {

                var $el = $(this),
                    $hoverElem = $el.find(self.options.hoverElement),
                    direction = self._getDir($el, {
                        x: event.pageX,
                        y: event.pageY
                    }),
                    styleCSS = self._getStyle(direction);

                if (event.type === 'mouseenter') {

                    $hoverElem.hide().css(styleCSS.from);
                    clearTimeout(self.tmhover);

                    self.tmhover = setTimeout(function() {

                        $hoverElem.show(0, function() {

                            var $el = $(this);
                            if (self.support) {
                                $el.css('transition', self.transitionProp);
                            }
                            self._applyAnimation($el, styleCSS.to, self.options.speed);

                        });


                    }, self.options.hoverDelay);

                } else {

                    if (self.support) {
                        $hoverElem.css('transition', self.transitionProp);
                    }
                    clearTimeout(self.tmhover);
                    self._applyAnimation($hoverElem, styleCSS.from, self.options.speed);

                }

            });

        },
        // credits : http://stackoverflow.com/a/3647634
        _getDir: function($el, coordinates) {

            // the width and height of the current div
            var w = $el.width(),
                h = $el.height(),

                // calculate the x and y to get an angle to the center of the div from that x and y.
                // gets the x value relative to the center of the DIV and "normalize" it
                x = (coordinates.x - $el.offset().left - (w / 2)) * (w > h ? (h / w) : 1),
                y = (coordinates.y - $el.offset().top - (h / 2)) * (h > w ? (w / h) : 1),

                // the angle and the direction from where the mouse came in/went out clockwise (TRBL=0123);
                // first calculate the angle of the point,
                // add 180 deg to get rid of the negative values
                // divide by 90 to get the quadrant
                // add 3 and do a modulo by 4  to shift the quadrants to a proper clockwise TRBL (top/right/bottom/left) **/
                direction = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;

            return direction;

        },
        _getStyle: function(direction) {

            var fromStyle, toStyle,
                slideFromTop = {
                    left: '0px',
                    top: '-100%'
                },
                slideFromBottom = {
                    left: '0px',
                    top: '100%'
                },
                slideFromLeft = {
                    left: '-100%',
                    top: '0px'
                },
                slideFromRight = {
                    left: '100%',
                    top: '0px'
                },
                slideTop = {
                    top: '0px'
                },
                slideLeft = {
                    left: '0px'
                };

            switch (direction) {
                case 0:
                    // from top
                    fromStyle = !this.options.inverse ? slideFromTop : slideFromBottom;
                    toStyle = slideTop;
                    break;
                case 1:
                    // from right
                    fromStyle = !this.options.inverse ? slideFromRight : slideFromLeft;
                    toStyle = slideLeft;
                    break;
                case 2:
                    // from bottom
                    fromStyle = !this.options.inverse ? slideFromBottom : slideFromTop;
                    toStyle = slideTop;
                    break;
                case 3:
                    // from left
                    fromStyle = !this.options.inverse ? slideFromLeft : slideFromRight;
                    toStyle = slideLeft;
                    break;
            };

            return {
                from: fromStyle,
                to: toStyle
            };

        },
        // apply a transition or fallback to jquery animate based on Modernizr.csstransitions support
        _applyAnimation: function(el, styleCSS, speed) {

            $.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;
            el.stop().applyStyle(styleCSS, $.extend(true, [], {
                duration: speed + 'ms'
            }));

        },

    };

    var logError = function(message) {

        if (window.console) {

            window.console.error(message);

        }

    };

    $.fn.hoverdir = function(options) {

        var instance = $.data(this, 'hoverdir');

        if (typeof options === 'string') {

            var args = Array.prototype.slice.call(arguments, 1);

            this.each(function() {

                if (!instance) {

                    logError("cannot call methods on hoverdir prior to initialization; " +
                        "attempted to call method '" + options + "'");
                    return;

                }

                if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {

                    logError("no such method '" + options + "' for hoverdir instance");
                    return;

                }

                instance[options].apply(instance, args);

            });

        } else {

            this.each(function() {

                if (instance) {

                    instance._init();

                } else {

                    instance = $.data(this, 'hoverdir', new $.HoverDir(options, this));

                }

            });

        }

        return instance;

    };

})(jQuery, window);
/**
 * ThemeREX Framework: global variables storage
 *
 * @package	themerex
 * @since	themerex 1.0
 */


/* Global variables manipulations
---------------------------------------------------------------- */

// Global variables storage
if (typeof THEMEREX_GLOBALS == 'undefined') var THEMEREX_GLOBALS = {};

// Get global variable
function themerex_get_global(var_name) {
    return themerex_isset(THEMEREX_GLOBALS[var_name]) ? THEMEREX_GLOBALS[var_name] : '';
}

// Set global variable
function themerex_set_global(var_name, value) {
    THEMEREX_GLOBALS[var_name] = value;
}

// Inc/Dec global variable with specified value
function themerex_inc_global(var_name) {
    var value = arguments[1] == undefined ? 1 : arguments[1];
    THEMEREX_GLOBALS[var_name] += value;
}

// Concatenate global variable with specified value
function themerex_concat_global(var_name, value) {
    THEMEREX_GLOBALS[var_name] += '' + value;
}

// Get global array element
function themerex_get_global_array(var_name, key) {
    return themerex_isset(THEMEREX_GLOBALS[var_name][key]) ? THEMEREX_GLOBALS[var_name][key] : '';
}

// Set global array element
function themerex_set_global_array(var_name, key, value) {
    if (!themerex_isset(THEMEREX_GLOBALS[var_name])) THEMEREX_GLOBALS[var_name] = {};
    THEMEREX_GLOBALS[var_name][key] = value;
}

// Inc/Dec global array element with specified value
function themerex_inc_global_array(var_name, key) {
    var value = arguments[2] == undefined ? 1 : arguments[2];
    THEMEREX_GLOBALS[var_name][key] += value;
}

// Concatenate global array element with specified value
function themerex_concat_global_array(var_name, key, value) {
    THEMEREX_GLOBALS[var_name][key] += '' + value;
}



/* PHP-style functions
---------------------------------------------------------------- */
function themerex_isset(obj) {
    return obj != undefined;
}

function themerex_empty(obj) {
    return obj == undefined || (typeof(obj) == 'object' && obj == null) || (typeof(obj) == 'array' && obj.length == 0) || (typeof(obj) == 'string' && themerex_alltrim(obj) == '');
}

function themerex_is_array(obj) {
    "use strict";
    return typeof(obj) == 'array';
}

function themerex_is_object(obj) {
    "use strict";
    return typeof(obj) == 'object';
}

function themerex_in_array(val, thearray) {
    "use strict";
    var rez = false;
    for (var i = 0; i < thearray.length - 1; i++) {
        if (thearray[i] == val) {
            rez = true;
            break;
        }
    }
    return rez;
}

function themerex_clone_object(obj) {
    if (obj == null || typeof(obj) != 'object') {
        return obj;
    }
    var temp = {};
    for (var key in obj) {
        temp[key] = themerex_clone_object(obj[key]);
    }
    return temp;
}



/* String functions
---------------------------------------------------------------- */

function themerex_in_list(str, list) {
    "use strict";
    var delim = arguments[2] ? arguments[2] : '|';
    var icase = arguments[3] ? arguments[3] : true;
    var retval = false;
    if (icase) {
        str = str.toLowerCase();
        list = list.toLowerCase();
    }
    var parts = list.split(delim);
    for (var i = 0; i < parts.length; i++) {
        if (parts[i] == str) {
            retval = true;
            break;
        }
    }
    return retval;
}

function themerex_alltrim(str) {
    "use strict";
    var dir = arguments[1] ? arguments[1] : 'a';
    var rez = '';
    var i, start = 0,
        end = str.length - 1;
    if (dir == 'a' || dir == 'l') {
        for (i = 0; i < str.length; i++) {
            if (str.substr(i, 1) != ' ') {
                start = i;
                break;
            }
        }
    }
    if (dir == 'a' || dir == 'r') {
        for (i = str.length - 1; i >= 0; i--) {
            if (str.substr(i, 1) != ' ') {
                end = i;
                break;
            }
        }
    }
    return str.substring(start, end + 1);
}

function themerex_ltrim(str) {
    "use strict";
    return themerex_alltrim(str, 'l');
}

function themerex_rtrim(str) {
    "use strict";
    return themerex_alltrim(str, 'r');
}

function themerex_padl(str, len) {
    "use strict";
    var ch = arguments[2] ? arguments[2] : ' ';
    var rez = str.substr(0, len);
    if (rez.length < len) {
        for (var i = 0; i < len - str.length; i++)
            rez += ch;
    }
    return rez;
}

function themerex_padr(str, len) {
    "use strict";
    var ch = arguments[2] ? arguments[2] : ' ';
    var rez = str.substr(0, len);
    if (rez.length < len) {
        for (var i = 0; i < len - str.length; i++)
            rez = ch + rez;
    }
    return rez;
}

function themerex_padc(str, len) {
    "use strict";
    var ch = arguments[2] ? arguments[2] : ' ';
    var rez = str.substr(0, len);
    if (rez.length < len) {
        for (var i = 0; i < Math.floor((len - str.length) / 2); i++)
            rez = ch + rez + ch;
    }
    return rez + (rez.length < len ? ch : '');
}

function themerex_replicate(str, num) {
    "use strict";
    var rez = '';
    for (var i = 0; i < num; i++) {
        rez += str;
    }
    return rez;
}



/* Numbers functions
---------------------------------------------------------------- */

// Round number to specified precision. 
// For example: num=1.12345, prec=2,  rounded=1.12
//              num=12345,   prec=-2, rounded=12300
function themerex_round_number(num) {
    "use strict";
    var precision = arguments[1] ? arguments[1] : 0;
    var p = Math.pow(10, precision);
    return Math.round(num * p) / p;
}

// Clear number from any characters and append it with 0 to desired precision
// For example: num=test1.12dd, prec=3, cleared=1.120
function themerex_clear_number(num) {
    "use strict";
    var precision = arguments[1] ? arguments[1] : 0;
    var defa = arguments[2] ? arguments[2] : 0;
    var res = '';
    var decimals = -1;
    num = "" + num;
    if (num == "") num = "" + defa;
    for (var i = 0; i < num.length; i++) {
        if (decimals == 0) break;
        else if (decimals > 0) decimals--;
        var ch = num.substr(i, 1);
        if (ch == '.') {
            if (precision > 0) {
                res += ch;
            }
            decimals = precision;
        } else if ((ch >= 0 && ch <= 9) || (ch == '-' && i == 0))
            res += ch;
    }
    if (precision > 0 && decimals != 0) {
        if (decimals == -1) {
            res += '.';
            decimals = precision;
        }
        for (i = decimals; i > 0; i--)
            res += '0';
    }
    //if (isNaN(res)) res = clearNumber(defa, precision, defa);
    return res;
}

// Convert number from decimal to hex
function themerex_dec2hex(n) {
    "use strict";
    return Number(n).toString(16);
}

// Convert number from hex to decimal
function themerex_hex2dec(hex) {
    "use strict";
    return parseInt(hex, 16);
}



/* Array manipulations
---------------------------------------------------------------- */

function themerex_sort_array(thearray) {
    "use strict";
    var caseSensitive = arguments[1] ? arguments[1] : false;
    for (var x = 0; x < thearray.length - 1; x++) {
        for (var y = (x + 1); y < thearray.length; y++) {
            if (caseSensitive) {
                if (thearray[x] > thearray[y]) {
                    tmp = thearray[x];
                    thearray[x] = thearray[y];
                    thearray[y] = tmp;
                }
            } else {
                if (thearray[x].toLowerCase() > thearray[y].toLowerCase()) {
                    tmp = thearray[x];
                    thearray[x] = thearray[y];
                    thearray[y] = tmp;
                }
            }
        }
    }
    return thearray;
}



/* Date manipulations
---------------------------------------------------------------- */

// Return array[Year, Month, Day, Hours, Minutes, Seconds]
// from string: Year[-/.]Month[-/.]Day[T ]Hours:Minutes:Seconds
function themerex_parse_date(dt) {
    "use strict";
    dt = dt.replace(/\//g, '-').replace(/\./g, '-').replace(/T/g, ' ').split('+')[0];
    var dt2 = dt.split(' ');
    var d = dt2[0].split('-');
    var t = dt2[1].split(':');
    d.push(t[0], t[1], t[2]);
    return d;
}

// Return difference string between two dates
function themerex_get_date_difference(dt1) {
    "use strict";
    var dt2 = arguments[1] !== undefined ? arguments[1] : '';
    var short_date = arguments[2] !== undefined ? arguments[2] : true;
    var sec = arguments[3] !== undefined ? arguments[3] : false;
    var a1 = themerex_parse_date(dt1);
    dt1 = Date.UTC(a1[0], a1[1], a1[2], a1[3], a1[4], a1[5]);
    if (dt2 == '') {
        dt2 = new Date();
        var a2 = [dt2.getFullYear(), dt2.getMonth() + 1, dt2.getDate(), dt2.getHours(), dt2.getMinutes(), dt2.getSeconds()];
    } else
        var a2 = themerex_parse_date(dt2);
    dt2 = Date.UTC(a2[0], a2[1], a2[2], a2[3], a2[4], a2[5]);
    var diff = Math.round((dt2 - dt1) / 1000);
    var days = Math.floor(diff / (24 * 3600));
    diff -= days * 24 * 3600;
    var hours = Math.floor(diff / 3600);
    diff -= hours * 3600;
    var minutes = Math.floor(diff / 60);
    diff -= minutes * 60;
    rez = '';
    if (days > 0)
        rez += (rez != '' ? ' ' : '') + days + ' day' + (days > 1 ? 's' : '');
    if ((!short_date || rez == '') && hours > 0)
        rez += (rez != '' ? ' ' : '') + hours + ' hour' + (hours > 1 ? 's' : '');
    if ((!short_date || rez == '') && minutes > 0)
        rez += (rez != '' ? ' ' : '') + minutes + ' minute' + (minutes > 1 ? 's' : '');
    if (sec || rez == '')
        rez += rez != '' || sec ? (' ' + diff + ' second' + (diff > 1 ? 's' : '')) : 'less then minute';
    return rez;
}



/* Colors functions
---------------------------------------------------------------- */

function themerex_hex2rgb(hex) {
    hex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
    return {
        r: hex >> 16,
        g: (hex & 0x00FF00) >> 8,
        b: (hex & 0x0000FF)
    };
}

function themerex_rgb2hex(color) {
    "use strict";
    var aRGB;
    color = color.replace(/\s/g, "").toLowerCase();
    if (color == 'rgba(0,0,0,0)' || color == 'rgba(0%,0%,0%,0%)')
        color = 'transparent';
    if (color.indexOf('rgba(') == 0)
        aRGB = color.match(/^rgba\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);
    else
        aRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);

    if (aRGB) {
        color = '';
        for (var i = 1; i <= 3; i++)
            color += Math.round((aRGB[i][aRGB[i].length - 1] == "%" ? 2.55 : 1) * parseInt(aRGB[i])).toString(16).replace(/^(.)$/, '0$1');
    } else
        color = color.replace(/^#?([\da-f])([\da-f])([\da-f])$/i, '$1$1$2$2$3$3');
    return (color.substr(0, 1) != '#' ? '#' : '') + color;
}

function themerex_components2hex(r, g, b) {
    "use strict";
    return '#' +
        Number(r).toString(16).toUpperCase().replace(/^(.)$/, '0$1') +
        Number(g).toString(16).toUpperCase().replace(/^(.)$/, '0$1') +
        Number(b).toString(16).toUpperCase().replace(/^(.)$/, '0$1');
}

function themerex_rgb2components(color) {
    "use strict";
    color = themerex_rgb2hex(color);
    var matches = color.match(/^#?([\dabcdef]{2})([\dabcdef]{2})([\dabcdef]{2})$/i);
    if (!matches) return false;
    for (var i = 1, rgb = new Array(3); i <= 3; i++)
        rgb[i - 1] = parseInt(matches[i], 16);
    return rgb;
}

function themerex_hex2hsb(hex) {
    "use strict";
    return themerex_rgb2hsb(themerex_hex2rgb(hex));
}

function themerex_hsb2hex(hsb) {
    var rgb = themerex_hsb2rgb(hsb);
    return themerex_components2hex(rgb.r, rgb.g, rgb.b);
}

function themerex_rgb2hsb(rgb) {
    "use strict";
    var hsb = {};
    hsb.b = Math.max(Math.max(rgb.r, rgb.g), rgb.b);
    hsb.s = (hsb.b <= 0) ? 0 : Math.round(100 * (hsb.b - Math.min(Math.min(rgb.r, rgb.g), rgb.b)) / hsb.b);
    hsb.b = Math.round((hsb.b / 255) * 100);
    if ((rgb.r == rgb.g) && (rgb.g == rgb.b)) hsb.h = 0;
    else if (rgb.r >= rgb.g && rgb.g >= rgb.b) hsb.h = 60 * (rgb.g - rgb.b) / (rgb.r - rgb.b);
    else if (rgb.g >= rgb.r && rgb.r >= rgb.b) hsb.h = 60 + 60 * (rgb.g - rgb.r) / (rgb.g - rgb.b);
    else if (rgb.g >= rgb.b && rgb.b >= rgb.r) hsb.h = 120 + 60 * (rgb.b - rgb.r) / (rgb.g - rgb.r);
    else if (rgb.b >= rgb.g && rgb.g >= rgb.r) hsb.h = 180 + 60 * (rgb.b - rgb.g) / (rgb.b - rgb.r);
    else if (rgb.b >= rgb.r && rgb.r >= rgb.g) hsb.h = 240 + 60 * (rgb.r - rgb.g) / (rgb.b - rgb.g);
    else if (rgb.r >= rgb.b && rgb.b >= rgb.g) hsb.h = 300 + 60 * (rgb.r - rgb.b) / (rgb.r - rgb.g);
    else hsb.h = 0;
    hsb.h = Math.round(hsb.h);
    return hsb;
}

function themerex_hsb2rgb(hsb) {
    var rgb = {};
    var h = Math.round(hsb.h);
    var s = Math.round(hsb.s * 255 / 100);
    var v = Math.round(hsb.b * 255 / 100);
    if (s == 0) {
        rgb.r = rgb.g = rgb.b = v;
    } else {
        var t1 = v;
        var t2 = (255 - s) * v / 255;
        var t3 = (t1 - t2) * (h % 60) / 60;
        if (h == 360) h = 0;
        if (h < 60) {
            rgb.r = t1;
            rgb.b = t2;
            rgb.g = t2 + t3;
        } else if (h < 120) {
            rgb.g = t1;
            rgb.b = t2;
            rgb.r = t1 - t3;
        } else if (h < 180) {
            rgb.g = t1;
            rgb.r = t2;
            rgb.b = t2 + t3;
        } else if (h < 240) {
            rgb.b = t1;
            rgb.r = t2;
            rgb.g = t1 - t3;
        } else if (h < 300) {
            rgb.b = t1;
            rgb.g = t2;
            rgb.r = t2 + t3;
        } else if (h < 360) {
            rgb.r = t1;
            rgb.g = t2;
            rgb.b = t1 - t3;
        } else {
            rgb.r = 0;
            rgb.g = 0;
            rgb.b = 0;
        }
    }
    return {
        r: Math.round(rgb.r),
        g: Math.round(rgb.g),
        b: Math.round(rgb.b)
    };
}

function themerex_color_picker() {
    "use strict";
    var id = arguments[0] ? arguments[0] : "iColorPicker" + Math.round(Math.random() * 1000);
    var colors = arguments[1] ? arguments[1] :
        '#f00,#ff0,#0f0,#0ff,#00f,#f0f,#fff,#ebebeb,#e1e1e1,#d7d7d7,#cccccc,#c2c2c2,#b7b7b7,#acacac,#a0a0a0,#959595,' + '#ee1d24,#fff100,#00a650,#00aeef,#2f3192,#ed008c,#898989,#7d7d7d,#707070,#626262,#555,#464646,#363636,#262626,#111,#000,' + '#f7977a,#fbad82,#fdc68c,#fff799,#c6df9c,#a4d49d,#81ca9d,#7bcdc9,#6ccff7,#7ca6d8,#8293ca,#8881be,#a286bd,#bc8cbf,#f49bc1,#f5999d,' + '#f16c4d,#f68e54,#fbaf5a,#fff467,#acd372,#7dc473,#39b778,#16bcb4,#00bff3,#438ccb,#5573b7,#5e5ca7,#855fa8,#a763a9,#ef6ea8,#f16d7e,' + '#ee1d24,#f16522,#f7941d,#fff100,#8fc63d,#37b44a,#00a650,#00a99e,#00aeef,#0072bc,#0054a5,#2f3192,#652c91,#91278f,#ed008c,#ee105a,' + '#9d0a0f,#a1410d,#a36209,#aba000,#588528,#197b30,#007236,#00736a,#0076a4,#004a80,#003370,#1d1363,#450e61,#62055f,#9e005c,#9d0039,' + '#790000,#7b3000,#7c4900,#827a00,#3e6617,#045f20,#005824,#005951,#005b7e,#003562,#002056,#0c004b,#30004a,#4b0048,#7a0045,#7a0026';
    var colorsList = colors.split(',');
    var tbl = '<table class="colorPickerTable"><thead>';
    for (var i = 0; i < colorsList.length; i++) {
        if (i % 16 == 0) tbl += (i > 0 ? '</tr>' : '') + '<tr>';
        tbl += '<td style="background-color:' + colorsList[i] + '">&nbsp;</td>';
    }
    tbl += '</tr></thead><tbody>' + '<tr style="height:60px;">' + '<td colspan="8" id="' + id + '_colorPreview" style="vertical-align:middle;text-align:center;border:1px solid #000;background:#fff;">' + '<input style="width:55px;color:#000;border:1px solid rgb(0, 0, 0);padding:5px;background-color:#fff;font:11px Arial, Helvetica, sans-serif;" maxlength="7" />' + '<a href="#" id="' + id + '_moreColors" class="iColorPicker_moreColors"></a>' + '</td>' + '<td colspan="8" id="' + id + '_colorOriginal" style="vertical-align:middle;text-align:center;border:1px solid #000;background:#fff;">' + '<input style="width:55px;color:#000;border:1px solid rgb(0, 0, 0);padding:5px;background-color:#fff;font:11px Arial, Helvetica, sans-serif;" readonly="readonly" />' + '</td>' + '</tr></tbody></table>';
    //tbl += '<style>#iColorPicker input{margin:2px}</style>';

    jQuery(document.createElement("div"))
        .attr("id", id)
        .css('display', 'none')
        .html(tbl)
        .appendTo("body")
        .addClass("iColorPickerTable")
        .on('mouseover', 'thead td', function() {
            "use strict";
            var aaa = themerex_rgb2hex(jQuery(this).css('background-color'));
            jQuery('#' + id + '_colorPreview').css('background', aaa);
            jQuery('#' + id + '_colorPreview input').val(aaa);
        })
        .on('keypress', '#' + id + '_colorPreview input', function(key) {
            "use strict";
            var aaa = jQuery(this).val()
            if (aaa.length < 7 && ((key.which >= 48 && key.which <= 57) || (key.which >= 97 && key.which <= 102) || (key.which === 35 || aaa.length === 0))) {
                aaa += String.fromCharCode(key.which);
            } else if (key.which == 8 && aaa.length > 0) {
                aaa = aaa.substring(0, aaa.length - 1);
            } else if (key.which === 13 && (aaa.length === 4 || aaa.length === 7)) {
                var fld = jQuery('#' + id).data('field');
                var func = jQuery('#' + id).data('func');
                if (func != null && func != 'undefined') {
                    func(fld, aaa);
                } else {
                    fld.val(aaa).css('backgroundColor', aaa).trigger('change');
                }
                jQuery('#' + id + '_Bg').fadeOut(500);
                jQuery('#' + id).fadeOut(500);

            } else {
                key.preventDefault();
                return false;
            }
            if (aaa.substr(0, 1) === '#' && (aaa.length === 4 || aaa.length === 7)) {
                jQuery('#' + id + '_colorPreview').css('background', aaa);
            }
        })
        .on('click', 'thead td', function(e) {
            "use strict";
            var fld = jQuery('#' + id).data('field');
            var func = jQuery('#' + id).data('func');
            var aaa = themerex_rgb2hex(jQuery(this).css('background-color'));
            if (func != null && func != 'undefined') {
                func(fld, aaa);
            } else {
                fld.val(aaa).css('backgroundColor', aaa).trigger('change');
            }
            jQuery('#' + id + '_Bg').fadeOut(500);
            jQuery('#' + id).fadeOut(500);
            e.preventDefault();
            return false;
        })
        .on('click', 'tbody .iColorPicker_moreColors', function(e) {
            "use strict";
            var thead = jQuery(this).parents('table').find('thead');
            var out = '';
            if (thead.hasClass('more_colors')) {
                for (var i = 0; i < colorsList.length; i++) {
                    if (i % 16 == 0) out += (i > 0 ? '</tr>' : '') + '<tr>';
                    out += '<td style="background-color:' + colorsList[i] + '">&nbsp;</td>';
                }
                thead.removeClass('more_colors').empty().html(out + '</tr>');
                jQuery('#' + id + '_colorPreview').attr('colspan', 8);
                jQuery('#' + id + '_colorOriginal').attr('colspan', 8);
            } else {
                var rgb = [0, 0, 0],
                    i = 0,
                    j = -1; // Set j=-1 or j=0 - show 2 different colors layouts
                while (rgb[0] < 0xF || rgb[1] < 0xF || rgb[2] < 0xF) {
                    if (i % 18 == 0) out += (i > 0 ? '</tr>' : '') + '<tr>';
                    i++;
                    out += '<td style="background-color:' + themerex_components2hex(rgb[0] * 16 + rgb[0], rgb[1] * 16 + rgb[1], rgb[2] * 16 + rgb[2]) + '">&nbsp;</td>';
                    rgb[2] += 3;
                    if (rgb[2] > 0xF) {
                        rgb[1] += 3;
                        if (rgb[1] > (j === 0 ? 6 : 0xF)) {
                            rgb[0] += 3;
                            if (rgb[0] > 0xF) {
                                if (j === 0) {
                                    j = 1;
                                    rgb[0] = 0;
                                    rgb[1] = 9;
                                    rgb[2] = 0;
                                } else {
                                    break;
                                }
                            } else {
                                rgb[1] = (j < 1 ? 0 : 9);
                                rgb[2] = 0;
                            }
                        } else {
                            rgb[2] = 0;
                        }
                    }
                }
                thead.addClass('more_colors').empty().html(out + '<td  style="background-color:#ffffff" colspan="8">&nbsp;</td></tr>');
                jQuery('#' + id + '_colorPreview').attr('colspan', 9);
                jQuery('#' + id + '_colorOriginal').attr('colspan', 9);
            }
            jQuery('#' + id + ' table.colorPickerTable thead td')
                .css({
                    'width': '12px',
                    'height': '14px',
                    'border': '1px solid #000',
                    'cursor': 'pointer'
                });
            e.preventDefault();
            return false;
        });
    jQuery(document.createElement("div"))
        .attr("id", id + "_Bg")
        .click(function(e) {
            "use strict";
            jQuery("#" + id + "_Bg").fadeOut(500);
            jQuery("#" + id).fadeOut(500);
            e.preventDefault();
            return false;
        })
        .appendTo("body");
    jQuery('#' + id + ' table.colorPickerTable thead td')
        .css({
            'width': '12px',
            'height': '14px',
            'border': '1px solid #000',
            'cursor': 'pointer'
        });
    jQuery('#' + id + ' table.colorPickerTable')
        .css({
            'border-collapse': 'collapse'
        });
    jQuery('#' + id)
        .css({
            'border': '1px solid #ccc',
            'background': '#333',
            'padding': '5px',
            'color': '#fff',
            'z-index': 999999
        });
    jQuery('#' + id + '_colorPreview')
        .css({
            'height': '50px'
        });
    return id;
}

function themerex_color_picker_show(id, fld, func) {
    "use strict";
    if (id === null || id === '') {
        id = jQuery('.iColorPickerTable').attr('id');
    }
    var eICP = fld.offset();
    var w = jQuery('#' + id).width();
    var h = jQuery('#' + id).height();
    var l = eICP.left + w < jQuery(window).width() - 10 ? eICP.left : jQuery(window).width() - 10 - w;
    var t = eICP.top + fld.outerHeight() + h < jQuery(document).scrollTop() + jQuery(window).height() - 10 ? eICP.top + fld.outerHeight() : eICP.top - h - 13;
    jQuery("#" + id)
        .data({
            field: fld,
            func: func
        })
        .css({
            'top': t + "px",
            'left': l + "px",
            'position': 'absolute',
            'z-index': 100001
        })
        .fadeIn(500);
    jQuery("#" + id + "_Bg")
        .css({
            'position': 'fixed',
            'z-index': 100000,
            'top': 0,
            'left': 0,
            'width': '100%',
            'height': '100%'
        })
        .fadeIn(500);
    var def = fld.val().substr(0, 1) == '#' ? fld.val() : themerex_rgb2hex(fld.css('backgroundColor'));
    jQuery('#' + id + '_colorPreview input,#' + id + '_colorOriginal input').val(def);
    jQuery('#' + id + '_colorPreview,#' + id + '_colorOriginal').css('background', def);
}



/* Cookies manipulations
---------------------------------------------------------------- */

function themerex_get_cookie(name) {
    "use strict";
    var defa = arguments[1] != undefined ? arguments[1] : null;
    var start = document.cookie.indexOf(name + '=');
    var len = start + name.length + 1;
    if ((!start) && (name != document.cookie.substring(0, name.length))) {
        return defa;
    }
    if (start == -1)
        return defa;
    var end = document.cookie.indexOf(';', len);
    if (end == -1)
        end = document.cookie.length;
    return unescape(document.cookie.substring(len, end));
}


function themerex_set_cookie(name, value, expires, path, domain, secure) {
    "use strict";
    var expires = arguments[2] != undefined ? arguments[2] : 0;
    var path = arguments[3] != undefined ? arguments[3] : '/';
    var domain = arguments[4] != undefined ? arguments[4] : '';
    var secure = arguments[5] != undefined ? arguments[5] : '';
    var today = new Date();
    today.setTime(today.getTime());
    if (expires) {
        expires = expires * 1000 * 60 * 60 * 24;
    }
    var expires_date = new Date(today.getTime() + (expires));
    document.cookie = name + '=' + escape(value) + ((expires) ? ';expires=' + expires_date.toGMTString() : '') + ((path) ? ';path=' + path : '') + ((domain) ? ';domain=' + domain : '') + ((secure) ? ';secure' : '');
}


function themerex_del_cookie(name, path, domain) {
    "use strict";
    var path = arguments[1] != undefined ? arguments[1] : '/';
    var domain = arguments[2] != undefined ? arguments[2] : '';
    if (themerex_get_cookie(name))
        document.cookie = name + '=' + ((path) ? ';path=' + path : '') + ((domain) ? ';domain=' + domain : '') + ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
}



/* ListBox and ComboBox manipulations
---------------------------------------------------------------- */

function themerex_clear_listbox(box) {
    "use strict";
    for (var i = box.options.length - 1; i >= 0; i--)
        box.options[i] = null;
}

function themerex_add_listbox_item(box, val, text) {
    "use strict";
    var item = new Option();
    item.value = val;
    item.text = text;
    box.options.add(item);
}

function themerex_del_listbox_item_by_value(box, val) {
    "use strict";
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].value == val) {
            box.options[i] = null;
            break;
        }
    }
}

function themerex_del_listbox_item_by_text(box, txt) {
    "use strict";
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].text == txt) {
            box.options[i] = null;
            break;
        }
    }
}

function themerex_find_listbox_item_by_value(box, val) {
    "use strict";
    var idx = -1;
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].value == val) {
            idx = i;
            break;
        }
    }
    return idx;
}

function themerex_find_listbox_item_by_text(box, txt) {
    "use strict";
    var idx = -1;
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].text == txt) {
            idx = i;
            break;
        }
    }
    return idx;
}

function themerex_select_listbox_item_by_value(box, val) {
    "use strict";
    for (var i = 0; i < box.options.length; i++) {
        box.options[i].selected = (val == box.options[i].value);
    }
}

function themerex_select_listbox_item_by_text(box, txt) {
    "use strict";
    for (var i = 0; i < box.options.length; i++) {
        box.options[i].selected = (txt == box.options[i].text);
    }
}

function themerex_get_listbox_values(box) {
    "use strict";
    var delim = arguments[1] ? arguments[1] : ',';
    var str = '';
    for (var i = 0; i < box.options.length; i++) {
        str += (str ? delim : '') + box.options[i].value;
    }
    return str;
}

function themerex_get_listbox_texts(box) {
    "use strict";
    var delim = arguments[1] ? arguments[1] : ',';
    var str = '';
    for (var i = 0; i < box.options.length; i++) {
        str += (str ? delim : '') + box.options[i].text;
    }
    return str;
}

function themerex_sort_listbox(box) {
    "use strict";
    var temp_opts = new Array();
    var temp = new Option();
    for (var i = 0; i < box.options.length; i++) {
        temp_opts[i] = box.options[i].clone();
    }
    for (var x = 0; x < temp_opts.length - 1; x++) {
        for (var y = (x + 1); y < temp_opts.length; y++) {
            if (temp_opts[x].text > temp_opts[y].text) {
                temp = temp_opts[x];
                temp_opts[x] = temp_opts[y];
                temp_opts[y] = temp;
            }
        }
    }
    for (var i = 0; i < box.options.length; i++) {
        box.options[i] = temp_opts[i].clone();
    }
}

function themerex_get_listbox_selected_index(box) {
    "use strict";
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].selected)
            return i;
    }
    return -1;
}

function themerex_get_listbox_selected_value(box) {
    "use strict";
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].selected) {
            return box.options[i].value;
        }
    }
    return null;
}

function themerex_get_listbox_selected_text(box) {
    "use strict";
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].selected) {
            return box.options[i].text;
        }
    }
    return null;
}

function themerex_get_listbox_selected_option(box) {
    "use strict";
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].selected) {
            return box.options[i];
        }
    }
    return null;
}



/* Radio buttons manipulations
---------------------------------------------------------------- */

function themerex_get_radio_value(radioGroupObj) {
    "use strict";
    for (var i = 0; i < radioGroupObj.length; i++)
        if (radioGroupObj[i].checked) return radioGroupObj[i].value;
    return null;
}

function themerex_set_radio_checked_by_num(radioGroupObj, num) {
    "use strict";
    for (var i = 0; i < radioGroupObj.length; i++)
        if (radioGroupObj[i].checked && i != num) radioGroupObj[i].checked = false;
        else if (i == num) radioGroupObj[i].checked = true;
}

function themerex_set_radio_checked_by_value(radioGroupObj, val) {
    "use strict";
    for (var i = 0; i < radioGroupObj.length; i++)
        if (radioGroupObj[i].checked && radioGroupObj[i].value != val) radioGroupObj[i].checked = false;
        else if (radioGroupObj[i].value == val) radioGroupObj[i].checked = true;
}



/* Form manipulations
---------------------------------------------------------------- */

/*
// Usage example:
var error = themerex_form_validate(jQuery(form_selector), {				// -------- Options ---------
	error_message_show: true,									// Display or not error message
	error_message_time: 5000,									// Time to display error message
	error_message_class: 'sc_infobox sc_infobox_style_error',	// Class, appended to error message block
	error_message_text: 'Global error text',					// Global error message text (if don't write message in checked field)
	error_fields_class: 'error_fields_class',					// Class, appended to error fields
	exit_after_first_error: false,								// Cancel validation and exit after first error
	rules: [
		{
			field: 'author',																// Checking field name
			min_length: { value: 1,	 message: 'The author name can\'t be empty' },			// Min character count (0 - don't check), message - if error occurs
			max_length: { value: 60, message: 'Too long author name'}						// Max character count (0 - don't check), message - if error occurs
		},
		{
			field: 'email',
			min_length: { value: 7,	 message: 'Too short (or empty) email address' },
			max_length: { value: 60, message: 'Too long email address'},
			mask: { value: '^([a-z0-9_\\-]+\\.)*[a-z0-9_\\-]+@[a-z0-9_\\-]+(\\.[a-z0-9_\\-]+)*\\.[a-z]{2,6}$', message: 'Invalid email address'}
		},
		{
			field: 'comment',
			min_length: { value: 1,	 message: 'The comment text can\'t be empty' },
			max_length: { value: 200, message: 'Too long comment'}
		},
		{
			field: 'pwd1',
			min_length: { value: 5,	 message: 'The password can\'t be less then 5 characters' },
			max_length: { value: 20, message: 'Too long password'}
		},
		{
			field: 'pwd2',
			equal_to: { value: 'pwd1',	 message: 'The passwords in both fields must be equals' }
		}
	]
});
*/

function themerex_form_validate(form, opt) {
    "use strict";
    var error_msg = '';
    form.find(":input").each(function() {
        "use strict";
        if (error_msg != '' && opt.exit_after_first_error) return;
        for (var i = 0; i < opt.rules.length; i++) {
            if (jQuery(this).attr("name") == opt.rules[i].field) {
                var val = jQuery(this).val();
                var error = false;
                if (typeof(opt.rules[i].min_length) == 'object') {
                    if (opt.rules[i].min_length.value > 0 && val.length < opt.rules[i].min_length.value) {
                        if (error_msg == '') jQuery(this).get(0).focus();
                        error_msg += '<p class="error_item">' + (typeof(opt.rules[i].min_length.message) != 'undefined' ? opt.rules[i].min_length.message : opt.error_message_text) + '</p>'
                        error = true;
                    }
                }
                if ((!error || !opt.exit_after_first_error) && typeof(opt.rules[i].max_length) == 'object') {
                    if (opt.rules[i].max_length.value > 0 && val.length > opt.rules[i].max_length.value) {
                        if (error_msg == '') jQuery(this).get(0).focus();
                        error_msg += '<p class="error_item">' + (typeof(opt.rules[i].max_length.message) != 'undefined' ? opt.rules[i].max_length.message : opt.error_message_text) + '</p>'
                        error = true;
                    }
                }
                if ((!error || !opt.exit_after_first_error) && typeof(opt.rules[i].mask) == 'object') {
                    if (opt.rules[i].mask.value != '') {
                        var regexp = new RegExp(opt.rules[i].mask.value);
                        if (!regexp.test(val)) {
                            if (error_msg == '') jQuery(this).get(0).focus();
                            error_msg += '<p class="error_item">' + (typeof(opt.rules[i].mask.message) != 'undefined' ? opt.rules[i].mask.message : opt.error_message_text) + '</p>'
                            error = true;
                        }
                    }
                }
                if ((!error || !opt.exit_after_first_error) && typeof(opt.rules[i].equal_to) == 'object') {
                    if (opt.rules[i].equal_to.value != '' && val != jQuery(jQuery(this).get(0).form[opt.rules[i].equal_to.value]).val()) {
                        if (error_msg == '') jQuery(this).get(0).focus();
                        error_msg += '<p class="error_item">' + (typeof(opt.rules[i].equal_to.message) != 'undefined' ? opt.rules[i].equal_to.message : opt.error_message_text) + '</p>'
                        error = true;
                    }
                }
                if (opt.error_fields_class != '') jQuery(this).toggleClass(opt.error_fields_class, error);
            }
        }
    });
    if (error_msg != '' && opt.error_message_show) {
        var error_message_box = form.find(".result");
        if (error_message_box.length == 0) error_message_box = form.parent().find(".result");
        if (error_message_box.length == 0) {
            form.append('<div class="result"></div>');
            error_message_box = form.find(".result");
        }
        if (opt.error_message_class) error_message_box.toggleClass(opt.error_message_class, true);
        error_message_box.html(error_msg).fadeIn();
        setTimeout(function() {
            error_message_box.fadeOut();
        }, opt.error_message_time);
    }
    return error_msg != '';
}



/* Document manipulations
---------------------------------------------------------------- */

// Animated scroll to selected id
function themerex_document_animate_to(id) {
    if (id.indexOf('#') == -1) id = '#' + id;
    var obj = jQuery(id).eq(0);
    if (obj.length == 0) return;
    var oft = jQuery(id).offset().top;
    var st = jQuery(window).scrollTop();
    var speed = Math.min(1600, Math.max(400, Math.round(Math.abs(oft - st) / jQuery(window).height() * 100)));
    jQuery('body,html').animate({
        scrollTop: oft - jQuery('#wpadminbar').height() - jQuery('header.fixedTopMenu .topWrap').height()
    }, speed, 'swing');
}

// Change browser address without reload page
function themerex_document_set_location(curLoc) {
    try {
        history.pushState(null, null, curLoc);
        return;
    } catch (e) {}
    location.href = curLoc;
}

// Add hidden elements init functions after tab, accordion, toggles activate
function themerex_add_hidden_elements_handler(key, handler) {
    themerex_set_global_array('init_hidden_elements', key, handler);
}

// Init hidden elements after tab, accordion, toggles activate
function themerex_init_hidden_elements(cont) {
    if (THEMEREX_GLOBALS['init_hidden_elements']) {
        for (key in THEMEREX_GLOBALS['init_hidden_elements']) {
            THEMEREX_GLOBALS['init_hidden_elements'][key](cont);
        }
    }
}



/* Browsers detection
---------------------------------------------------------------- */

function themerex_browser_is_mobile() {
    var check = false;
    (function(a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

function themerex_browser_is_ios() {
    return navigator.userAgent.match(/iPad|iPhone|iPod/i) != null;
}



/* File functions
---------------------------------------------------------------- */

function themerex_get_file_name(path) {
    path = path.replace(/\\/g, '/');
    var pos = path.lastIndexOf('/');
    if (pos >= 0)
        path = path.substr(pos + 1);
    return path;
}

function themerex_get_file_ext(path) {
    var pos = path.lastIndexOf('.');
    path = pos >= 0 ? path.substr(pos + 1) : '';
    return path;
}



/* Image functions
---------------------------------------------------------------- */

// Return true, if all images in the specified container are loaded
function themerex_check_images_complete(cont) {
    var complete = true;
    cont.find('img').each(function() {
        if (!complete) return;
        if (!jQuery(this).get(0).complete) complete = false;
    });
    return complete;
}
THEMEREX_GLOBALS["ajax_url"] = "#";
THEMEREX_GLOBALS["ajax_nonce"] = "afefc26b7a";
THEMEREX_GLOBALS["ajax_nonce_editor"] = "b4f7f39b8a";
THEMEREX_GLOBALS["site_url"] = "index.html";
THEMEREX_GLOBALS["vc_edit_mode"] = false;
THEMEREX_GLOBALS["theme_font"] = "";
THEMEREX_GLOBALS["theme_skin"] = "education";
THEMEREX_GLOBALS["theme_skin_bg"] = "";
THEMEREX_GLOBALS["slider_height"] = 100;
THEMEREX_GLOBALS["system_message"] = {
    message: "",
    status: "",
    header: ""
};
THEMEREX_GLOBALS["user_logged_in"] = true;
THEMEREX_GLOBALS["toc_menu"] = "no";
THEMEREX_GLOBALS["toc_menu_home"] = false;
THEMEREX_GLOBALS["toc_menu_top"] = true;
THEMEREX_GLOBALS["menu_fixed"] = true;
THEMEREX_GLOBALS["menu_relayout"] = 960;
THEMEREX_GLOBALS["menu_responsive"] = 800;
THEMEREX_GLOBALS["menu_slider"] = true;
THEMEREX_GLOBALS["demo_time"] = 0;
THEMEREX_GLOBALS["media_elements_enabled"] = true;
THEMEREX_GLOBALS["ajax_search_enabled"] = true;
THEMEREX_GLOBALS["ajax_search_min_length"] = 3;
THEMEREX_GLOBALS["ajax_search_delay"] = 200;
THEMEREX_GLOBALS["css_animation"] = true;
THEMEREX_GLOBALS["menu_animation_in"] = "bounceIn";
THEMEREX_GLOBALS["menu_animation_out"] = "fadeOutDown";
THEMEREX_GLOBALS["popup_engine"] = "pretty";
THEMEREX_GLOBALS["popup_gallery"] = true;
THEMEREX_GLOBALS["email_mask"] = "^([a-zA-Z0-9_\-]+\.)*[a-zA-Z0-9_\-]+@[a-z0-9_\-]+(\.[a-z0-9_\-]+)*\.[a-z]{2,6}$";
THEMEREX_GLOBALS["contacts_maxlength"] = 1000;
THEMEREX_GLOBALS["comments_maxlength"] = 1000;
THEMEREX_GLOBALS["remember_visitors_settings"] = false;
THEMEREX_GLOBALS["admin_mode"] = false;
THEMEREX_GLOBALS["isotope_resize_delta"] = 0.3;
THEMEREX_GLOBALS["error_message_box"] = null;
THEMEREX_GLOBALS["viewmore_busy"] = false;
THEMEREX_GLOBALS["video_resize_inited"] = false;
THEMEREX_GLOBALS["top_panel_height"] = 0;



if (THEMEREX_GLOBALS["theme_font"] == "") THEMEREX_GLOBALS["theme_font"] = "Roboto";
THEMEREX_GLOBALS["link_color"] = "#1eaace";
THEMEREX_GLOBALS["menu_color"] = "#1dbb90";
THEMEREX_GLOBALS["user_color"] = "#ffb20e";


THEMEREX_GLOBALS["reviews_allow_user_marks"] = true;
THEMEREX_GLOBALS["reviews_max_level"] = 100;
THEMEREX_GLOBALS["reviews_levels"] = "bad,poor,normal,good,great";
THEMEREX_GLOBALS["reviews_vote"] = "";
THEMEREX_GLOBALS["reviews_marks"] = "58,47,62,47".split(",");
THEMEREX_GLOBALS["reviews_users"] = 12;
THEMEREX_GLOBALS["post_id"] = 688;

THEMEREX_GLOBALS["reviews_allow_user_marks"] = true;
THEMEREX_GLOBALS["reviews_max_level"] = 100;
THEMEREX_GLOBALS["reviews_levels"] = "bad,poor,normal,good,great";
THEMEREX_GLOBALS["reviews_vote"] = "";
THEMEREX_GLOBALS["reviews_marks"] = "50,77,63,60".split(",");
THEMEREX_GLOBALS["reviews_users"] = 8;
THEMEREX_GLOBALS["post_id"] = 636;

THEMEREX_GLOBALS["reviews_allow_user_marks"] = true;
THEMEREX_GLOBALS["reviews_max_level"] = 100;
THEMEREX_GLOBALS["reviews_levels"] = "bad,poor,normal,good,great";
THEMEREX_GLOBALS["reviews_vote"] = "";
THEMEREX_GLOBALS["reviews_marks"] = "89,79,72,71".split(",");
THEMEREX_GLOBALS["reviews_users"] = 4;
THEMEREX_GLOBALS["post_id"] = 26;

THEMEREX_GLOBALS["reviews_allow_user_marks"] = true;
THEMEREX_GLOBALS["reviews_max_level"] = 100;
THEMEREX_GLOBALS["reviews_levels"] = "bad,poor,normal,good,great";
THEMEREX_GLOBALS["reviews_vote"] = "";
THEMEREX_GLOBALS["reviews_marks"] = "68,75,88,96".split(",");
THEMEREX_GLOBALS["reviews_users"] = 2;
THEMEREX_GLOBALS["post_id"] = 32;

THEMEREX_GLOBALS["reviews_allow_user_marks"] = true;
THEMEREX_GLOBALS["reviews_max_level"] = 100;
THEMEREX_GLOBALS["reviews_levels"] = "bad,poor,normal,good,great";
THEMEREX_GLOBALS["reviews_vote"] = "";
THEMEREX_GLOBALS["reviews_marks"] = "79,87,89,92".split(",");
THEMEREX_GLOBALS["reviews_users"] = 3;
THEMEREX_GLOBALS["post_id"] = 92;

THEMEREX_GLOBALS['ppp'] = 6;
jQuery(".isotope_filters.isotope-courses-streampage").append('<a href="#" data-filter="*" class="isotope_filters_button active">All</a><a href="#" data-filter=".flt_55" class="isotope_filters_button">One To One</a><a href="#" data-filter=".flt_43" class="isotope_filters_button">One To Many</a>');
function themerex_animation_shortcodes() {
    jQuery('[data-animation^="animated"]:not(.animated)').each(function() {
        "use strict";
        jQuery(this).offset().top < jQuery(window).scrollTop() + jQuery(window).height() && jQuery(this).addClass(jQuery(this).data("animation"))
    })
}

function themerex_init_shortcodes(e) {
    e.find(".sc_accordion:not(.inited)").length > 0 && e.find(".sc_accordion:not(.inited)").each(function() {
        "use strict";
        var e = jQuery(this).data("active");
        e = isNaN(e) ? 0 : Math.max(0, e), jQuery(this).addClass("inited").accordion({
            active: e,
            heightStyle: "content",
            header: "> .sc_accordion_item > .sc_accordion_title",
            create: function(e, t) {
                themerex_init_shortcodes(t.panel), window.themerex_init_hidden_elements && themerex_init_hidden_elements(t.panel), t.header.each(function() {
                    jQuery(this).parent().addClass("sc_active")
                })
            },
            activate: function(e, t) {
                themerex_init_shortcodes(t.newPanel), window.themerex_init_hidden_elements && themerex_init_hidden_elements(t.newPanel), t.newHeader.each(function() {
                    jQuery(this).parent().addClass("sc_active")
                }), t.oldHeader.each(function() {
                    jQuery(this).parent().removeClass("sc_active")
                })
            }
        })
    }), e.find(".sc_contact_form:not(.inited) form").length > 0 && e.find(".sc_contact_form:not(.inited) form").addClass("inited").submit(function(e) {
        "use strict";
        return themerex_contact_form_validate(jQuery(this)), e.preventDefault(), !1
    }), e.find(".sc_countdown:not(.inited)").length > 0 && e.find(".sc_countdown:not(.inited)").each(function() {
        "use strict";
        jQuery(this).addClass("inited");
        var e = (jQuery(this).attr("id"), new Date),
            t = e.getFullYear() + "-" + (e.getMonth() < 9 ? "0" : "") + (e.getMonth() + 1) + "-" + (e.getDate() < 10 ? "0" : "") + e.getDate() + " " + (e.getHours() < 10 ? "0" : "") + e.getHours() + ":" + (e.getMinutes() < 10 ? "0" : "") + e.getMinutes() + ":" + (e.getSeconds() < 10 ? "0" : "") + e.getSeconds(),
            i = 1,
            n = jQuery(this).data("date"),
            a = n.split("-"),
            s = jQuery(this).data("time"),
            r = s.split(":");
        r.length < 3 && (r[2] = "00");
        var o = n + " " + s;
        jQuery(this).find(".sc_countdown_placeholder").countdown(o > t ? {
            until: new Date(a[0], a[1] - 1, a[2], r[0], r[1], r[2]),
            tickInterval: i,
            onTick: themerex_countdown
        } : {
            since: new Date(a[0], a[1] - 1, a[2], r[0], r[1], r[2]),
            tickInterval: i,
            onTick: themerex_countdown
        })
    }), e.find(".sc_emailer:not(.inited)").length > 0 && e.find(".sc_emailer:not(.inited)").addClass("inited").find(".sc_emailer_button").click(function(e) {
        "use strict";
        var t = jQuery(this).parents("form"),
            i = jQuery(this).parents(".sc_emailer");
        if (i.hasClass("sc_emailer_opened"))
            if (t.length > 0 && "" != t.find("input").val()) {
                var n = jQuery(this).data("group"),
                    a = t.find("input").val(),
                    s = new RegExp(THEMEREX_GLOBALS.email_mask);
                s.test(a) ? jQuery.post(THEMEREX_GLOBALS.ajax_url, {
                    action: "emailer_submit",
                    nonce: THEMEREX_GLOBALS.ajax_nonce,
                    group: n,
                    email: a
                }).done(function(e) {
                    var i = JSON.parse(e);
                    "" === i.error ? (themerex_message_info(THEMEREX_GLOBALS.strings.email_confirm.replace("%s", a)), t.find("input").val("")) : themerex_message_warning(i.error)
                }) : (t.find("input").get(0).focus(), themerex_message_warning(THEMEREX_GLOBALS.strings.email_not_valid))
            } else t.get(0).submit();
        else i.addClass("sc_emailer_opened");
        return e.preventDefault(), !1
    }), e.find(".sc_googlemap:not(.inited)").length > 0 && e.find(".sc_googlemap:not(.inited)").each(function() {
        "use strict";
        if (!(jQuery(this).parents("div:hidden,article:hidden").length > 0)) {
            var e = jQuery(this).addClass("inited"),
                t = e.data("address"),
                i = e.data("latlng"),
                n = e.attr("id"),
                a = e.data("zoom"),
                s = e.data("style"),
                r = e.data("description"),
                o = e.data("title"),
                d = e.data("point");
            themerex_googlemap_init(jQuery("#" + n).get(0), {
                address: t,
                latlng: i,
                style: s,
                zoom: a,
                description: r,
                title: o,
                point: d
            })
        }
    }), e.find(".sc_infobox.sc_infobox_closeable:not(.inited)").length > 0 && e.find(".sc_infobox.sc_infobox_closeable:not(.inited)").addClass("inited").click(function() {
        jQuery(this).slideUp()
    }), e.find(".popup_link:not(.inited)").length > 0 && e.find(".popup_link:not(.inited)").addClass("inited").magnificPopup({
        type: "inline",
        removalDelay: 500,
        midClick: !0,
        callbacks: {
            beforeOpen: function() {
                this.st.mainClass = "mfp-zoom-in"
            },
            open: function() {},
            close: function() {}
        }
    }), e.find(".search_wrap:not(.inited)").length > 0 && e.find(".search_wrap:not(.inited)").each(function() {
        if (jQuery(this).addClass("inited"), jQuery(this).find(".search_icon").click(function(e) {
                "use strict";
                var t = jQuery(this).parent();
                return t.hasClass("search_fixed") ? (t.find(".search_field").val(""), t.find(".search_results").fadeOut()) : t.hasClass("search_opened") ? (t.find(".search_form_wrap").animate({
                    width: "hide"
                }, 200, function() {
                    t.parents(".menu_main_wrap").length > 0 && t.parents(".menu_main_wrap").removeClass("search_opened")
                }), t.find(".search_results").fadeOut(), t.removeClass("search_opened")) : (t.find(".search_form_wrap").animate({
                    width: "show"
                }, 200, function() {
                    jQuery(this).parents(".search_wrap").addClass("search_opened"), jQuery(this).find("input").get(0).focus()
                }), t.parents(".menu_main_wrap").length > 0 && t.parents(".menu_main_wrap").addClass("search_opened")), e.preventDefault(), !1
            }), jQuery(this).find(".search_results_close").click(function(e) {
                "use strict";
                return jQuery(this).parent().fadeOut(), e.preventDefault(), !1
            }), jQuery(this).on("click", ".search_submit,.search_more", function(e) {
                "use strict";
                return "" != jQuery(this).parents(".search_wrap").find(".search_field").val() && jQuery(this).parents(".search_wrap").find(".search_form_wrap form").get(0).submit(), e.preventDefault(), !1
            }), jQuery(this).hasClass("search_ajax")) {
            var e = null;
            jQuery(this).find(".search_field").keyup(function() {
                "use strict";
                var t = jQuery(this),
                    i = t.val();
                e && (clearTimeout(e), e = null), i.length >= THEMEREX_GLOBALS.ajax_search_min_length && (e = setTimeout(function() {
                    jQuery.post(THEMEREX_GLOBALS.ajax_url, {
                        action: "ajax_search",
                        nonce: THEMEREX_GLOBALS.ajax_nonce,
                        text: i
                    }).done(function(i) {
                        clearTimeout(e), e = null;
                        var n = JSON.parse(i);
                        "" === n.error ? (t.parents(".search_ajax").find(".search_results_content").empty().append(n.data), t.parents(".search_ajax").find(".search_results").fadeIn()) : themerex_message_warning(THEMEREX_GLOBALS.strings.search_error)
                    })
                }, THEMEREX_GLOBALS.ajax_search_delay))
            })
        }
    }), e.find(".sc_pan:not(.inited_pan)").length > 0 && e.find(".sc_pan:not(.inited_pan)").each(function() {
        "use strict";
        if (!(jQuery(this).parents("div:hidden,article:hidden").length > 0)) {
            var e = jQuery(this).addClass("inited_pan"),
                t = e.parent();
            t.mousemove(function(i) {
                var n = e.width(),
                    a = e.height(),
                    s = t.width(),
                    r = t.height(),
                    o = t.offset();
                e.hasClass("sc_pan_vertical") && e.css("top", -Math.floor((i.pageY - o.top) / r * (a - r))), e.hasClass("sc_pan_horizontal") && e.css("left", -Math.floor((i.pageX - o.left) / s * (n - s)))
            }), t.mouseout(function() {
                e.css({
                    left: 0,
                    top: 0
                })
            })
        }
    }), e.find(".sc_scroll:not(.inited)").length > 0 && e.find(".sc_scroll:not(.inited)").each(function() {
        "use strict";
        jQuery(this).parents("div:hidden,article:hidden").length > 0 || (THEMEREX_GLOBALS.scroll_init_counter = 0, themerex_init_scroll_area(jQuery(this)))
    }), e.find(".sc_slider_swiper:not(.inited)").length > 0 && e.find(".sc_slider_swiper:not(.inited)").each(function() {
        "use strict";
        if (!(jQuery(this).parents("div:hidden,article:hidden").length > 0)) {
            jQuery(this).addClass("inited"), themerex_slider_autoheight(jQuery(this)), jQuery(this).parents(".sc_slider_pagination_area").length > 0 && jQuery(this).parents(".sc_slider_pagination_area").find(".sc_slider_pagination .post_item").eq(0).addClass("active");
            var e = jQuery(this).attr("id");
            void 0 == e && (e = "swiper_" + Math.random(), e = e.replace(".", ""), jQuery(this).attr("id", e)), jQuery(this).addClass(e), jQuery(this).find(".slides .swiper-slide").css("position", "relative"), void 0 === THEMEREX_GLOBALS.swipers && (THEMEREX_GLOBALS.swipers = {}), THEMEREX_GLOBALS.swipers[e] = new Swiper("." + e, {
                calculateHeight: !jQuery(this).hasClass("sc_slider_height_fixed"),
                resizeReInit: !0,
                autoResize: !0,
                loop: !0,
                grabCursor: !0,
                pagination: jQuery(this).hasClass("sc_slider_pagination") ? "#" + e + " .sc_slider_pagination_wrap" : !1,
                paginationClickable: !0,
                autoplay: jQuery(this).hasClass("sc_slider_noautoplay") ? !1 : isNaN(jQuery(this).data("interval")) ? 7e3 : jQuery(this).data("interval"),
                autoplayDisableOnInteraction: !1,
                initialSlide: 0,
                speed: 600,
                onFirstInit: function(e) {
                    var t = jQuery(e.container);
                    if (t.hasClass("sc_slider_height_auto")) {
                        var i = t.find(".swiper-slide").eq(1),
                            n = i.data("height_auto");
                        if (n > 0) {
                            var a = parseInt(i.css("paddingTop")),
                                s = parseInt(i.css("paddingBottom"));
                            i.height(n), t.height(n + (isNaN(a) ? 0 : a) + (isNaN(s) ? 0 : s)), t.find(".swiper-wrapper").height(n + (isNaN(a) ? 0 : a) + (isNaN(s) ? 0 : s))
                        }
                    }
                },
                onSlideChangeStart: function(e) {
                    var t = jQuery(e.container);
                    if (t.hasClass("sc_slider_height_auto")) {
                        var i = e.activeIndex,
                            n = t.find(".swiper-slide").eq(i),
                            a = n.data("height_auto");
                        if (a > 0) {
                            var s = parseInt(n.css("paddingTop")),
                                r = parseInt(n.css("paddingBottom"));
                            n.height(a), t.height(a + (isNaN(s) ? 0 : s) + (isNaN(r) ? 0 : r)), t.find(".swiper-wrapper").height(a + (isNaN(s) ? 0 : s) + (isNaN(r) ? 0 : r))
                        }
                    }
                },
                onSlideChangeEnd: function(e) {
                    var t = jQuery(e.container);
                    if (t.parents(".sc_slider_pagination_area").length > 0) {
                        var i = t.parents(".sc_slider_pagination_area").find(".sc_slider_pagination .post_item"),
                            n = e.activeIndex > i.length ? 0 : e.activeIndex - 1;
                        themerex_change_active_pagination_in_slider(t, n)
                    }
                }
            }), jQuery(this).data("settings", {
                mode: "horizontal"
            });
            var t = jQuery(this).find(".slides").data("current-slide");
            t > 0 && THEMEREX_GLOBALS.swipers[e].swipeTo(t - 1), themerex_prepare_slider_navi(jQuery(this))
        }
    }), e.find(".sc_skills_item:not(.inited)").length > 0 && (themerex_init_skills(e), jQuery(window).scroll(function() {
        themerex_init_skills(e)
    })), e.find(".sc_skills_arc:not(.inited)").length > 0 && (themerex_init_skills_arc(e), jQuery(window).scroll(function() {
        themerex_init_skills_arc(e)
    })), e.find(".sc_tabs:not(.inited),.tabs_area:not(.inited)").length > 0 && e.find(".sc_tabs:not(.inited),.tabs_area:not(.inited)").each(function() {
        var e = jQuery(this).data("active");
        e = isNaN(e) ? 0 : Math.max(0, e), jQuery(this).addClass("inited").tabs({
            active: e,
            show: {
                effect: "fadeIn",
                duration: 300
            },
            hide: {
                effect: "fadeOut",
                duration: 300
            },
            create: function(e, t) {
                themerex_init_shortcodes(t.panel), window.themerex_init_hidden_elements && themerex_init_hidden_elements(t.panel)
            },
            activate: function(e, t) {
                themerex_init_shortcodes(t.newPanel), window.themerex_init_hidden_elements && themerex_init_hidden_elements(t.newPanel)
            }
        })
    }), e.find(".sc_toggles .sc_toggles_title:not(.inited)").length > 0 && e.find(".sc_toggles .sc_toggles_title:not(.inited)").addClass("inited").click(function() {
        jQuery(this).toggleClass("ui-state-active").parent().toggleClass("sc_active"), jQuery(this).parent().find(".sc_toggles_content").slideToggle(300, function() {
            themerex_init_shortcodes(jQuery(this).parent().find(".sc_toggles_content")), window.themerex_init_hidden_elements && themerex_init_hidden_elements(jQuery(this).parent().find(".sc_toggles_content"))
        })
    }), e.find(".sc_zoom:not(.inited)").length > 0 && e.find(".sc_zoom:not(.inited)").each(function() {
        "use strict";
        jQuery(this).parents("div:hidden,article:hidden").length > 0 || (jQuery(this).addClass("inited"), jQuery(this).find("img").elevateZoom({
            zoomType: "lens",
            lensShape: "round",
            lensSize: 200,
            lensBorderSize: 4,
            lensBorderColour: "#ccc"
        }))
    })
}

function themerex_init_scroll_area(e) {
    if (!themerex_check_images_complete(e) && THEMEREX_GLOBALS.scroll_init_counter++ < 30) return void setTimeout(function() {
        themerex_init_scroll_area(e)
    }, 200);
    e.addClass("inited");
    var t = e.attr("id");
    void 0 == t && (t = "scroll_" + Math.random(), t = t.replace(".", ""), e.attr("id", t)), e.addClass(t);
    var i = e.find("#" + t + "_bar");
    i.length > 0 && !i.hasClass(t + "_bar") && i.addClass(t + "_bar"), void 0 === THEMEREX_GLOBALS.swipers && (THEMEREX_GLOBALS.swipers = {}), THEMEREX_GLOBALS.swipers[t] = new Swiper("." + t, {
        calculateHeight: !1,
        resizeReInit: !0,
        autoResize: !0,
        freeMode: !0,
        freeModeFluid: !0,
        grabCursor: !0,
        noSwiping: e.hasClass("scroll-no-swiping"),
        mode: e.hasClass("sc_scroll_vertical") ? "vertical" : "horizontal",
        slidesPerView: e.hasClass("sc_scroll") ? "auto" : 1,
        mousewheelControl: !0,
        mousewheelAccelerator: 4,
        scrollContainer: e.hasClass("sc_scroll_vertical"),
        scrollbar: {
            container: "." + t + "_bar",
            hide: !0,
            draggable: !0
        }
    }), e.data("settings", {
        mode: "horizontal"
    }), themerex_prepare_slider_navi(e)
}

function themerex_prepare_slider_navi(e) {
    var t = e.find("> .sc_slider_controls_wrap, > .sc_scroll_controls_wrap");
    0 == t.length && (t = e.siblings(".sc_slider_controls_wrap,.sc_scroll_controls_wrap")), t.length > 0 && (t.find(".sc_slider_prev,.sc_scroll_prev").click(function(e) {
        var t = jQuery(this).parents(".swiper-slider-container");
        0 == t.length && (t = jQuery(this).parents(".sc_slider_controls_wrap,.sc_scroll_controls_wrap").siblings(".swiper-slider-container"));
        var i = t.attr("id");
        return THEMEREX_GLOBALS.swipers[i].swipePrev(), e.preventDefault(), !1
    }), t.find(".sc_slider_next,.sc_scroll_next").click(function(e) {
        var t = jQuery(this).parents(".swiper-slider-container");
        0 == t.length && (t = jQuery(this).parents(".sc_slider_controls_wrap,.sc_scroll_controls_wrap").siblings(".swiper-slider-container"));
        var i = t.attr("id");
        return THEMEREX_GLOBALS.swipers[i].swipeNext(), e.preventDefault(), !1
    })), t = e.siblings(".sc_slider_pagination"), t.length > 0 && t.find(".post_item").click(function(e) {
        var t = jQuery(this).parents(".sc_slider_pagination_area").find(".swiper-slider-container"),
            i = t.attr("id");
        return THEMEREX_GLOBALS.swipers[i].swipeTo(jQuery(this).index()), e.preventDefault(), !1
    })
}

function themerex_change_active_pagination_in_slider(e, t) {
    var i = e.parents(".sc_slider_pagination_area").find(".sc_slider_pagination");
    if (0 != i.length) {
        i.find(".post_item").removeClass("active").eq(t).addClass("active");
        var n = i.height(),
            a = i.find(".active").offset().top - i.offset().top,
            s = i.find(".sc_scroll_wrapper").offset().top - i.offset().top,
            r = i.find(".active").height();
        0 > a ? i.find(".sc_scroll_wrapper").css({
            transform: "translate3d(0px, 0px, 0px)",
            "transition-duration": "0.3s"
        }) : a + r >= n && i.find(".sc_scroll_wrapper").css({
            transform: "translate3d(0px, -" + (Math.abs(s) + a - n / 4) + "px, 0px)",
            "transition-duration": "0.3s"
        })
    }
}

function themerex_slider_autoheight(e) {
    e.hasClass(".sc_slider_height_auto") && e.find(".swiper-slide").each(function() {
        void 0 == jQuery(this).data("height_auto") && jQuery(this).attr("data-height_auto", jQuery(this).height())
    })
}

function themerex_init_skills(e) {
    if (0 == arguments.length) var e = jQuery("body");
    var t = jQuery(window).scrollTop() + jQuery(window).height();
    e.find(".sc_skills_item:not(.inited)").each(function() {
        var e = jQuery(this),
            i = e.offset().top;
        if (t > i) {
            e.addClass("inited");
            var n = e.parents(".sc_skills").eq(0),
                a = n.data("type"),
                s = "pie" == a && n.hasClass("sc_skills_compact_on") ? e.find(".sc_skills_data .pie") : e.find(".sc_skills_total").eq(0),
                r = parseInt(s.data("start")),
                o = parseInt(s.data("stop")),
                d = parseInt(s.data("max")),
                c = Math.round(r / d * 100),
                l = Math.round(o / d * 100),
                _ = s.data("ed"),
                h = parseInt(s.data("duration")),
                u = parseInt(s.data("speed")),
                p = parseInt(s.data("step"));
            if ("bar" == a) {
                var f = n.data("dir"),
                    m = e.find(".sc_skills_count").eq(0);
                "horizontal" == f ? m.css("width", c + "%").animate({
                    width: l + "%"
                }, h) : "vertical" == f && m.css("height", c + "%").animate({
                    height: l + "%"
                }, h), themerex_animate_skills_counter(r, o, u - ("unknown" != f ? 5 : 0), p, _, s)
            } else if ("counter" == a) themerex_animate_skills_counter(r, o, u - 5, p, _, s);
            else if ("pie" == a) {
                var g = parseInt(s.data("steps")),
                    v = s.data("bg_color"),
                    w = s.data("border_color"),
                    y = parseInt(s.data("cutout")),
                    j = s.data("easing"),
                    Q = {
                        segmentShowStroke: !0,
                        segmentStrokeColor: w,
                        segmentStrokeWidth: 1,
                        percentageInnerCutout: y,
                        animationSteps: g,
                        animationEasing: j,
                        animateRotate: !0,
                        animateScale: !1
                    },
                    x = [];
                s.each(function() {
                    var e = jQuery(this).data("color"),
                        t = parseInt(jQuery(this).data("stop")),
                        i = Math.round(t / d * 100);
                    x.push({
                        value: i,
                        color: e
                    })
                }), 1 == s.length && (themerex_animate_skills_counter(r, o, Math.round(1500 / g), p, _, s), x.push({
                    value: 100 - l,
                    color: v
                }));
                var E = e.find("canvas");
                E.attr({
                    width: e.width(),
                    height: e.width()
                }).css({
                    width: e.width(),
                    height: e.height()
                }), new Chart(E.get(0).getContext("2d")).Doughnut(x, Q)
            }
        }
    })
}

function themerex_animate_skills_counter(e, t, i, n, a, s) {
    e = Math.min(t, e + n), s.text(e + a), t > e && setTimeout(function() {
        themerex_animate_skills_counter(e, t, i, n, a, s)
    }, i)
}

function themerex_init_skills_arc(e) {
    if (0 == arguments.length) var e = jQuery("body");
    e.find(".sc_skills_arc:not(.inited)").each(function() {
        var e = jQuery(this);
        e.addClass("inited");
        var t = e.find(".sc_skills_data .arc"),
            i = e.find(".sc_skills_arc_canvas").eq(0),
            n = e.find(".sc_skills_legend").eq(0),
            a = Math.round(e.width() - n.width()),
            s = Math.floor(a / 2),
            r = {
                random: function(e, t) {
                    return Math.floor(Math.random() * (t - e + 1) + e)
                },
                diagram: function() {
                    var n = Raphael(i.attr("id"), a, a),
                        o = hover = Math.round(a / 2 / t.length),
                        d = Math.round(((a - 20) / 2 - o) / t.length),
                        c = Math.round(a / 9 / t.length),
                        l = 400;
                    n.circle(s, s, Math.round(a / 2)).attr({
                        stroke: "none",
                        fill: THEMEREX_GLOBALS.theme_skin_bg ? THEMEREX_GLOBALS.theme_skin_bg : "#ffffff"
                    });
                    var _ = n.text(s, s, e.data("subtitle")).attr({
                        font: "lighter " + Math.round(.7 * o) + 'px "' + THEMEREX_GLOBALS.theme_font + '"',
                        fill: "#888888"
                    }).toFront();
                    o -= Math.round(d / 2), n.customAttributes.arc = function(e, t, i) {
                        var n = 3.6 * e,
                            a = 360 == n ? 359.99 : n,
                            o = r.random(91, 240),
                            d = (o - a) * Math.PI / 180,
                            c = o * Math.PI / 180,
                            l = s + i * Math.cos(c),
                            _ = s - i * Math.sin(c),
                            h = s + i * Math.cos(d),
                            u = s - i * Math.sin(d),
                            p = [
                                ["M", l, _],
                                ["A", i, i, 0, +(a > 180), 1, h, u]
                            ];
                        return {
                            path: p,
                            stroke: t
                        }
                    }, t.each(function() {
                        var t = jQuery(this),
                            i = t.find(".color").val(),
                            a = t.find(".percent").val(),
                            s = t.find(".text").text();
                        o += d;
                        var r = n.path().attr({
                            arc: [a, i, o],
                            "stroke-width": c
                        });
                        r.mouseover(function() {
                            this.animate({
                                "stroke-width": hover,
                                opacity: .75
                            }, 1e3, "elastic"), "VML" != Raphael.type && this.toFront(), _.stop().animate({
                                opacity: 0
                            }, l, ">", function() {
                                this.attr({
                                    text: (s ? s + "\n" : "") + a + "%"
                                }).animate({
                                    opacity: 1
                                }, l, "<")
                            })
                        }).mouseout(function() {
                            this.stop().animate({
                                "stroke-width": c,
                                opacity: 1
                            }, 4 * l, "elastic"), _.stop().animate({
                                opacity: 0
                            }, l, ">", function() {
                                _.attr({
                                    text: e.data("subtitle")
                                }).animate({
                                    opacity: 1
                                }, l, "<")
                            })
                        })
                    })
                }
            };
        r.diagram()
    })
}

function themerex_countdown(e) {
    for (var t = jQuery(this).parent(), i = 3; i < e.length; i++) {
        var n = (e[i] < 10 ? "0" : "") + e[i];
        t.find(".sc_countdown_item").eq(i - 3).find(".sc_countdown_digits span").addClass("hide");
        for (var a = n.length - 1; a >= 0; a--) t.find(".sc_countdown_item").eq(i - 3).find(".sc_countdown_digits span").eq(a + (3 == i && n.length < 3 ? 1 : 0)).removeClass("hide").text(n.substr(a, 1))
    }
}
jQuery(document).ready(function() {
    "use strict";
    setTimeout(themerex_animation_shortcodes, 600)
});
/* global jQuery:false */
/* global THEMEREX_GLOBALS:false */


// Theme init actions
function themerex_init_actions() {
    "use strict";

    $.material.init();
    $.material.ripples();
    $.material.input();
    $.material.checkbox();
    $.material.radio();


    themerex_resize_actions();
    themerex_scroll_actions();

    // Resize handlers
    jQuery(window).resize(function() {
        "use strict";
        themerex_resize_actions();
    });

    // Scroll handlers
    jQuery(window).scroll(function() {
        "use strict";
        themerex_scroll_actions();
    });
}



// Theme first load actions
//==============================================
function themerex_ready_actions() {
    "use strict";

    // Call skin specific action (if exists)
    //----------------------------------------------
    if (window.themerex_skin_ready_actions) themerex_skin_ready_actions();








    // Media setup
    //----------------------------------------------

    // Video background init
    jQuery('.video_background').each(function() {
        var youtube = jQuery(this).data('youtube-code');
        if (youtube) {
            jQuery(this).tubular({
                videoId: youtube
            });
        }
    });



    // Menu
    //----------------------------------------------

    // Clone main menu for responsive


    jQuery('.menu_main_wrap ul.menu_main_nav')
        .addClass('hide')
        .clone()
        .removeClass('menu_main_nav')
        .addClass('menu_main_responsive hide')
        .appendTo('span#responsiveMenu');

    // Responsive menu button
    jQuery('.menu_main_responsive_button').click(function(e) {
        "use strict";
        var id = jQuery('.menu_main_nav:not(.hide)').attr('id');
        jQuery('.menu_main_responsive#' + id).slideToggle();
        e.preventDefault();
        return false;
    });


    // Submenu click handler for the responsive menu
    jQuery('.menu_main_wrap .menu_main_responsive li a').click(function(e) {
        "use strict";
        if (jQuery('body').hasClass('responsive_menu') && jQuery(this).parent().hasClass('menu-item-has-children')) {
            if (jQuery(this).siblings('ul:visible').length > 0)
                jQuery(this).siblings('ul').slideUp().parent().removeClass('opened');
            else {
                jQuery(this).siblings('ul').slideDown().parent().addClass('opened');
            }

        }
        if (jQuery(this).attr('href') == '#') {
            e.preventDefault();
            if (jQuery(this).siblings('ul').length > 0) {
                if (jQuery(this).is('.opened'))
                    jQuery(this).siblings('ul').hide();
                else {
                    jQuery(this).siblings('ul').show();
                }
            }
            return false;
        }
        jQuery('.menu_main_responsive').hide();
    });

    // Init superfish menus
    themerex_init_sfmenu('.menu_main_wrap ul.menu_main_nav, .menu_user_wrap ul#menu_user');

    // Slide effect for main menu
    if (THEMEREX_GLOBALS['menu_slider']) {
        jQuery('.menu_main_nav').spasticNav({
            color: THEMEREX_GLOBALS['menu_color']
        });
    }


    // Store height of the top panel
    THEMEREX_GLOBALS['top_panel_height'] = 0; //Math.max(0, jQuery('.top_panel_wrap').height());


    // Pagination
    //----------------------------------------------

    // Page navigation (style slider)
    jQuery('.pager_cur').click(function(e) {
        "use strict";
        jQuery('.pager_slider').slideDown(300, function() {
            themerex_init_shortcodes(jQuery('.pager_slider').eq(0));
        });
        e.preventDefault();
        return false;
    });




    // Woocommerce
    //----------------------------------------------

    // Change display mode
    jQuery('.woocommerce .mode_buttons a,.woocommerce-page .mode_buttons a').click(function(e) {
        "use strict";
        var mode = jQuery(this).hasClass('woocommerce_thumbs') ? 'thumbs' : 'list';
        jQuery.cookie('themerex_shop_mode', mode, {
            expires: 365,
            path: '/'
        });
        jQuery(this).siblings('input').val(mode).parents('form').get(0).submit();
        e.preventDefault();
        return false;
    });


    // Scroll to top button
    jQuery('.scroll_to_top').click(function(e) {
        "use strict";
        jQuery('html,body').animate({
            scrollTop: 0
        }, 'slow');
        e.preventDefault();
        return false;
    });

    // Init post format specific scripts
    themerex_init_post_formats();

    // Init shortcodes scripts
    themerex_init_shortcodes(jQuery('body').eq(0));

    // Init hidden elements (if exists)
    if (window.themerex_init_hidden_elements) themerex_init_hidden_elements(jQuery('body').eq(0));

} //end ready




// Scroll actions
//==============================================

// Do actions when page scrolled
function themerex_scroll_actions() {
    "use strict";

    var scroll_offset = jQuery(window).scrollTop();
    var scroll_to_top_button = jQuery('.scroll_to_top');
    var adminbar_height = Math.max(0, jQuery('#wpadminbar').height());

    if (THEMEREX_GLOBALS['top_panel_height'] == 0) THEMEREX_GLOBALS['top_panel_height'] = jQuery('.top_panel_wrap').height();

    // Call skin specific action (if exists)
    //----------------------------------------------
    if (window.themerex_skin_scroll_actions) themerex_skin_scroll_actions();


    // Scroll to top button show/hide
    if (scroll_offset > THEMEREX_GLOBALS['top_panel_height'])
        scroll_to_top_button.addClass('show');
    else
        scroll_to_top_button.removeClass('show');

    // Fix/unfix top panel
    if (!jQuery('body').hasClass('responsive_menu') && THEMEREX_GLOBALS['menu_fixed']) {
        var slider_height = 0;
        if (jQuery('.top_panel_below .slider_wrap').length > 0) {
            slider_height = jQuery('.top_panel_below .slider_wrap').height();
            if (slider_height < 10) {
                slider_height = jQuery('.slider_wrap').hasClass('.slider_fullscreen') ? jQuery(window).height() : THEMEREX_GLOBALS['slider_height'];
            }
        }
        if (scroll_offset <= slider_height + THEMEREX_GLOBALS['top_panel_height']) {
            if (jQuery('body').hasClass('top_panel_fixed')) {
                jQuery('body').removeClass('top_panel_fixed');
            }
        } else if (scroll_offset > slider_height + THEMEREX_GLOBALS['top_panel_height']) {
            if (!jQuery('body').hasClass('top_panel_fixed')) {
                jQuery('.top_panel_fixed_wrap').height(THEMEREX_GLOBALS['top_panel_height']);
                jQuery('.top_panel_wrap').css('marginTop', '-150px').animate({
                    'marginTop': 0
                }, 500);
                jQuery('body').addClass('top_panel_fixed');
            }
        }
    }


    // Parallax scroll
    themerex_parallax_scroll();

    // Scroll actions for shortcodes
    themerex_animation_shortcodes();
}



// Parallax scroll
function themerex_parallax_scroll() {
    jQuery('.sc_parallax').each(function() {
        var windowHeight = jQuery(window).height();
        var scrollTops = jQuery(window).scrollTop();
        var offsetPrx = Math.max(jQuery(this).offset().top, windowHeight);
        if (offsetPrx <= scrollTops + windowHeight) {
            var speed = Number(jQuery(this).data('parallax-speed'));
            var xpos = jQuery(this).data('parallax-x-pos');
            var ypos = Math.round((offsetPrx - scrollTops - windowHeight) * speed + (speed < 0 ? windowHeight * speed : 0));
            jQuery(this).find('.sc_parallax_content').css('backgroundPosition', xpos + ' ' + ypos + 'px');
            // Uncomment next line if you want parallax video (else - video position is static)
            jQuery(this).find('div.sc_video_bg').css('top', ypos + 'px');
        }
    });
}





// Resize actions
//==============================================

// Do actions when page scrolled
function themerex_resize_actions() {
    "use strict";

    // Call skin specific action (if exists)
    //----------------------------------------------
    if (window.themerex_skin_resize_actions) themerex_skin_resize_actions();
    themerex_responsive_menu();
    themerex_video_dimensions();
    themerex_resize_video_background();
    themerex_resize_fullscreen_slider();
}


// Check window size and do responsive menu
// Check window size and do responsive menu
function themerex_responsive_menu() {
    if (themerex_is_responsive_need(THEMEREX_GLOBALS['menu_responsive'])) {
        if (!jQuery('body').hasClass('responsive_menu')) {
            jQuery('body').removeClass('top_panel_fixed').addClass('responsive_menu');
            if (jQuery('body').hasClass('menu_relayout'))
                jQuery('body').removeClass('menu_relayout menu_left').addClass('menu_right');
            if (jQuery('ul.menu_main_nav').hasClass('inited')) {
                jQuery('ul.menu_main_nav').removeClass('inited').superfish('destroy');
            }
        }
    } else {
        if (jQuery('body').hasClass('responsive_menu')) {
            jQuery('body').removeClass('responsive_menu');
            jQuery('.menu_main_responsive').hide();
            themerex_init_sfmenu('ul.menu_main_nav');
            jQuery('.menu_main_nav_area').show();
        }
        if (themerex_is_responsive_need(THEMEREX_GLOBALS['menu_relayout'])) {
            if (jQuery('body').hasClass('menu_right')) {
                jQuery('body').removeClass('menu_right').addClass('menu_relayout menu_left');
                //THEMEREX_GLOBALS['top_panel_height'] = Math.max(0, jQuery('.top_panel_wrap').height());
            }
        } else {
            if (jQuery('body').hasClass('menu_relayout')) {
                jQuery('body').removeClass('menu_relayout menu_left').addClass('menu_right');
                //THEMEREX_GLOBALS['top_panel_height'] = Math.max(0, jQuery('.top_panel_wrap').height());
            }
        }
    }
    if (!jQuery('.menu_main_wrap').hasClass('menu_show')) jQuery('.menu_main_wrap').addClass('menu_show');
}



// Check if responsive menu need
function themerex_is_responsive_need(max_width) {
    "use strict";
    var rez = false;
    if (max_width > 0) {
        var w = window.innerWidth;
        if (w == undefined) {
            w = jQuery(window).width() + (jQuery(window).height() < jQuery(document).height() || jQuery(window).scrollTop() > 0 ? 16 : 0);
        }
        rez = max_width > w;
    }
    return rez;
}



// Fit video frames to document width
function themerex_video_dimensions() {
    jQuery('.sc_video_frame').each(function() {
        "use strict";
        var frame = jQuery(this).eq(0);
        var player = frame.parent();
        var ratio = (frame.data('ratio') ? frame.data('ratio').split(':') : (frame.find('[data-ratio]').length > 0 ? frame.find('[data-ratio]').data('ratio').split(':') : [16, 9]));
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var w_attr = frame.data('width');
        var h_attr = frame.data('height');
        if (!w_attr || !h_attr) {
            return;
        }
        var percent = ('' + w_attr).substr(-1) == '%';
        w_attr = parseInt(w_attr);
        h_attr = parseInt(h_attr);
        var w_real = Math.min(percent ? 10000 : w_attr, frame.parents('div,article').width()), //player.width();
            h_real = Math.round(percent ? w_real / ratio : w_real / w_attr * h_attr);
        if (parseInt(frame.attr('data-last-width')) == w_real) return;
        if (percent) {
            frame.height(h_real);
        } else {
            frame.css({
                'width': w_real + 'px',
                'height': h_real + 'px'
            });
        }
        frame.attr('data-last-width', w_real);
    });
    jQuery('video.sc_video,video.wp-video-shortcode').each(function() {
        "use strict";
        var video = jQuery(this).eq(0);
        var ratio = (video.data('ratio') != undefined ? video.data('ratio').split(':') : [16, 9]);
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var mejs_cont = video.parents('.mejs-video');
        var frame = video.parents('.sc_video_frame');
        var w_attr = frame.length > 0 ? frame.data('width') : video.data('width');
        var h_attr = frame.length > 0 ? frame.data('height') : video.data('height');
        if (!w_attr || !h_attr) {
            w_attr = video.attr('width');
            h_attr = video.attr('height');
            if (!w_attr || !h_attr) return;
            video.data({
                'width': w_attr,
                'height': h_attr
            });
        }
        var percent = ('' + w_attr).substr(-1) == '%';
        w_attr = parseInt(w_attr);
        h_attr = parseInt(h_attr);
        var w_real = Math.round(mejs_cont.length > 0 ? Math.min(percent ? 10000 : w_attr, mejs_cont.parents('div,article').width()) : video.width()),
            h_real = Math.round(percent ? w_real / ratio : w_real / w_attr * h_attr);
        if (parseInt(video.attr('data-last-width')) == w_real) return;
        if (mejs_cont.length > 0 && mejs) {
            themerex_set_mejs_player_dimensions(video, w_real, h_real);
        }
        if (percent) {
            video.height(h_real);
        } else {
            video.attr({
                'width': w_real,
                'height': h_real
            }).css({
                'width': w_real + 'px',
                'height': h_real + 'px'
            });
        }
        video.attr('data-last-width', w_real);
    });
    jQuery('video.sc_video_bg').each(function() {
        "use strict";
        var video = jQuery(this).eq(0);
        var ratio = (video.data('ratio') != undefined ? video.data('ratio').split(':') : [16, 9]);
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var mejs_cont = video.parents('.mejs-video');
        var container = mejs_cont.length > 0 ? mejs_cont.parent() : video.parent();
        var w = container.width();
        var h = container.height();
        var w1 = Math.ceil(h * ratio);
        var h1 = Math.ceil(w / ratio);
        if (video.parents('.sc_parallax').length > 0) {
            var windowHeight = jQuery(window).height();
            var speed = Number(video.parents('.sc_parallax').data('parallax-speed'));
            var h_add = Math.ceil(Math.abs((windowHeight - h) * speed));
            if (h1 < h + h_add) {
                h1 = h + h_add;
                w1 = Math.ceil(h1 * ratio);
            }
        }
        if (h1 < h) {
            h1 = h;
            w1 = Math.ceil(h1 * ratio);
        }
        if (w1 < w) {
            w1 = w;
            h1 = Math.ceil(w1 / ratio);
        }
        var l = Math.round((w1 - w) / 2);
        var t = Math.round((h1 - h) / 2);
        if (parseInt(video.attr('data-last-width')) == w1) return;
        if (mejs_cont.length > 0) {
            themerex_set_mejs_player_dimensions(video, w1, h1);
            mejs_cont.css({
                'left': -l + 'px',
                'top': -t + 'px'
            });
        } else
            video.css({
                'left': -l + 'px',
                'top': -t + 'px'
            });
        video.attr({
            'width': w1,
            'height': h1,
            'data-last-width': w1
        }).css({
            'width': w1 + 'px',
            'height': h1 + 'px'
        });
        if (video.css('opacity') == 0) video.animate({
            'opacity': 1
        }, 3000);
    });
    jQuery('iframe').each(function() {
        "use strict";
        var iframe = jQuery(this).eq(0);
        var ratio = (iframe.data('ratio') != undefined ? iframe.data('ratio').split(':') : (iframe.find('[data-ratio]').length > 0 ? iframe.find('[data-ratio]').data('ratio').split(':') : [16, 9]));
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var w_attr = iframe.attr('width');
        var h_attr = iframe.attr('height');
        var frame = iframe.parents('.sc_video_frame');
        if (frame.length > 0) {
            w_attr = frame.data('width');
            h_attr = frame.data('height');
        }
        if (!w_attr || !h_attr) {
            return;
        }
        var percent = ('' + w_attr).substr(-1) == '%';
        w_attr = parseInt(w_attr);
        h_attr = parseInt(h_attr);
        var w_real = frame.length > 0 ? frame.width() : iframe.width(),
            h_real = Math.round(percent ? w_real / ratio : w_real / w_attr * h_attr);
        if (parseInt(iframe.attr('data-last-width')) == w_real) return;
        iframe.css({
            'width': w_real + 'px',
            'height': h_real + 'px'
        });
    });
}

// Resize fullscreen video background
function themerex_resize_video_background() {
    var bg = jQuery('.video_bg');
    if (bg.length < 1)
        return;
    if (THEMEREX_GLOBALS['media_elements_enabled'] && bg.find('.mejs-video').length == 0) {
        setTimeout(themerex_resize_video_background, 100);
        return;
    }
    var video = bg.find('video');
    var ratio = (video.data('ratio') != undefined ? video.data('ratio').split(':') : [16, 9]);
    ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
    var w = bg.width();
    var h = bg.height();
    var w1 = Math.ceil(h * ratio);
    var h1 = Math.ceil(w / ratio);
    if (h1 < h) {
        h1 = h;
        w1 = Math.ceil(h1 * ratio);
    }
    if (w1 < w) {
        w1 = w;
        h1 = Math.ceil(w1 / ratio);
    }
    var l = Math.round((w1 - w) / 2);
    var t = Math.round((h1 - h) / 2);
    if (bg.find('.mejs-container').length > 0) {
        themerex_set_mejs_player_dimensions(bg.find('video'), w1, h1);
        bg.find('.mejs-container').css({
            'left': -l + 'px',
            'top': -t + 'px'
        });
    } else
        bg.find('video').css({
            'left': -l + 'px',
            'top': -t + 'px'
        });
    bg.find('video').attr({
        'width': w1,
        'height': h1
    }).css({
        'width': w1 + 'px',
        'height': h1 + 'px'
    });
}

// Set Media Elements player dimensions
function themerex_set_mejs_player_dimensions(video, w, h) {
    if (mejs) {
        for (var pl in mejs.players) {
            if (mejs.players[pl].media.src == video.attr('src')) {
                if (mejs.players[pl].media.setVideoSize) {
                    mejs.players[pl].media.setVideoSize(w, h);
                }
                mejs.players[pl].setPlayerSize(w, h);
                mejs.players[pl].setControlsSize();
                //var mejs_cont = video.parents('.mejs-video');
                //mejs_cont.css({'width': w+'px', 'height': h+'px'}).find('.mejs-layers > div, .mejs-overlay, .mejs-poster').css({'width': w, 'height': h});
            }
        }
    }
}

// Resize Fullscreen Slider
function themerex_resize_fullscreen_slider() {
    var slider_wrap = jQuery('.slider_wrap.slider_fullscreen');
    if (slider_wrap.length < 1)
        return;
    var slider = slider_wrap.find('.sc_slider_swiper');
    if (slider.length < 1)
        return;
    var h = jQuery(window).height() - jQuery('#wpadminbar').height() - (jQuery('body').hasClass('top_panel_above') && !jQuery('body').hasClass('.top_panel_fixed') ? jQuery('.top_panel_wrap').height() : 0);
    slider.height(h);
}





// Navigation
//==============================================

// Init Superfish menu
function themerex_init_sfmenu(selector) {
    jQuery(selector).show().each(function() {
        if (themerex_is_responsive_need() && jQuery(this).attr('id') == 'menu_main') return;
        jQuery(this).superfish({
            delay: 500,
            animation: {
                opacity: 'show'
            },
            animationOut: {
                opacity: 'hide'
            },
            speed: THEMEREX_GLOBALS['css_animation'] ? 500 : (THEMEREX_GLOBALS['menu_slider'] ? 300 : 200),
            speedOut: THEMEREX_GLOBALS['css_animation'] ? 500 : (THEMEREX_GLOBALS['menu_slider'] ? 300 : 200),
            autoArrows: false,
            dropShadows: false,
            onBeforeShow: function(ul) {
                if (jQuery(this).parents("ul").length > 1) {
                    var w = jQuery(window).width();
                    var par_offset = jQuery(this).parents("ul").offset().left;
                    var par_width = jQuery(this).parents("ul").outerWidth();
                    var ul_width = jQuery(this).outerWidth();
                    if (par_offset + par_width + ul_width > w - 20 && par_offset - ul_width > 0)
                        jQuery(this).addClass('submenu_left');
                    else
                        jQuery(this).removeClass('submenu_left');
                }
                if (THEMEREX_GLOBALS['css_animation']) {
                    jQuery(this).removeClass('animated fast ' + THEMEREX_GLOBALS['menu_animation_out']);
                    jQuery(this).addClass('animated fast ' + THEMEREX_GLOBALS['menu_animation_in']);
                }
            },
            onBeforeHide: function(ul) {
                if (THEMEREX_GLOBALS['css_animation']) {
                    jQuery(this).removeClass('animated fast ' + THEMEREX_GLOBALS['menu_animation_in']);
                    jQuery(this).addClass('animated fast ' + THEMEREX_GLOBALS['menu_animation_out']);
                }
            }
        });
    });
}





// Isotope
//=====================================================

// First init isotope containers
function themerex_init_isotope() {
    "use strict";

    var all_images_complete = true;

    // Check if all images in isotope wrapper are loaded
    jQuery('.isotope_wrap:not(.inited)').each(function() {
        "use strict";
        all_images_complete = all_images_complete && themerex_check_images_complete(jQuery(this));
    });
    // Wait for images loading
    if (!all_images_complete && THEMEREX_GLOBALS['isotope_init_counter']++ < 30) {
        setTimeout(themerex_init_isotope, 200);
        return;
    }

    // Isotope filters handler
    jQuery('.isotope_filters:not(.inited)').addClass('inited').on('click', 'a', function(e) {
        "use strict";
        jQuery(this).parents('.isotope_filters').find('a').removeClass('active');
        jQuery(this).addClass('active');

        var selector = jQuery(this).data('filter');
        jQuery(this).parents('.isotope_filters').siblings('.isotope_wrap').eq(0).isotope({
            filter: selector
        });

        if (selector == '*')
            jQuery('#viewmore_link').fadeIn();
        else
            jQuery('#viewmore_link').fadeOut();

        e.preventDefault();
        return false;
    });

    // Init isotope script
    jQuery('.isotope_wrap:not(.inited)').each(function() {
        "use strict";

        var isotope_container = jQuery(this);

        // Init shortcodes
        themerex_init_shortcodes(isotope_container);

        // If in scroll container - no init isotope
        if (isotope_container.parents('.sc_scroll').length > 0) {
            isotope_container.addClass('inited').find('.isotope_item').animate({
                opacity: 1
            }, 200, function() {
                jQuery(this).addClass('isotope_item_show');
            });
            return;
        }

        // Init isotope with timeout
        // setTimeout(function() {
        //  isotope_container.addClass('inited').isotope({
        //      itemSelector: '.isotope_item',
        //      animationOptions: {
        //          duration: 750,
        //          easing: 'linear',
        //          queue: false
        //      }
        //  });

        //  // Show elements
        //  isotope_container.find('.isotope_item').animate({opacity: 1}, 200, function () { 
        //      jQuery(this).addClass('isotope_item_show'); 
        //  });

        // }, 500);

    });
}

function themerex_init_appended_isotope(posts_container, filters) {
    "use strict";

    if (posts_container.parents('.sc_scroll_horizontal').length > 0) return;

    if (!themerex_check_images_complete(posts_container) && THEMEREX_GLOBALS['isotope_init_counter']++ < 30) {
        setTimeout(function() {
            themerex_init_appended_isotope(posts_container, filters);
        }, 200);
        return;
    }
    // Add filters
    var flt = posts_container.siblings('.isotope_filter');
    for (var i in filters) {
        if (flt.find('a[data-filter=".flt_' + i + '"]').length == 0) {
            flt.append('<a href="#" class="isotope_filters_button" data-filter=".flt_' + i + '">' + filters[i] + '</a>');
        }
    }
    // Init shortcodes in added elements
    themerex_init_shortcodes(posts_container);
    // Get added elements
    var elems = posts_container.find('.isotope_item:not(.isotope_item_show)');
    // Notify isotope about added elements with timeout
    setTimeout(function() {
        posts_container.isotope('appended', elems);
        // Show appended elements
        elems.animate({
            opacity: 1
        }, 200, function() {
            jQuery(this).addClass('isotope_item_show');
        });
    }, 500);
}



// Post formats init
//=====================================================
function setHomeVideo() {
    if (jQuery('.sc_video_play_button:not(.inited)').length > 0) {
        jQuery('.sc_video_play_button:not(.inited)').each(function() {
            "use strict";
            jQuery(this)
                .addClass('inited')
                .animate({
                    opacity: 1
                }, 1000)
                .click(function(e) {
                    "use strict";
                    if (!jQuery(this).hasClass('sc_video_play_button')) return;
                    var video = jQuery(this).removeClass('sc_video_play_button hover_icon_play').data('video');
                    if (video !== '') {
                        jQuery(this).empty().html(video);
                        themerex_video_dimensions();
                        var video_tag = jQuery(this).find('video');
                        var w = video_tag.width();
                        var h = video_tag.height();
                        themerex_init_media_elements(jQuery(this));
                        // Restore WxH attributes, because Chrome broke it!
                        jQuery(this).find('video').css({
                            'width': w,
                            'height': h
                        }).attr({
                            'width': w,
                            'height': h
                        });
                    }
                    e.preventDefault();
                    return false;
                });
        });
    }
}

function themerex_init_post_formats() {
    "use strict";

    // MediaElement init
    themerex_init_media_elements(jQuery('body'));

    // Isotope first init
    if (jQuery('.isotope_wrap:not(.inited)').length > 0) {
        THEMEREX_GLOBALS['isotope_init_counter'] = 0;
        themerex_init_isotope();
    }

    // Hover Effect 'Dir'
    if (jQuery('.isotope_wrap .isotope_item_content.square.effect_dir:not(.inited)').length > 0) {
        jQuery('.isotope_wrap .isotope_item_content.square.effect_dir:not(.inited)').each(function() {
            jQuery(this).addClass('inited').hoverdir();
        });
    }

    // Popup init
    if (THEMEREX_GLOBALS['popup_engine'] == 'pretty') {
        jQuery("a[href$='jpg'],a[href$='jpeg'],a[href$='png'],a[href$='gif']").attr('rel', 'prettyPhoto' + (THEMEREX_GLOBALS['popup_gallery'] ? '[slideshow]' : ''));
        var images = jQuery("a[rel*='prettyPhoto']:not(.inited):not([data-rel*='pretty']):not([rel*='magnific']):not([data-rel*='magnific'])").addClass('inited');
        try {
            images.prettyPhoto({
                social_tools: '',
                theme: 'facebook',
                deeplinking: false
            });
        } catch (e) {};
    } else if (THEMEREX_GLOBALS['popup_engine'] == 'magnific') {
        jQuery("a[href$='jpg'],a[href$='jpeg'],a[href$='png'],a[href$='gif']").attr('rel', 'magnific');
        var images = jQuery("a[rel*='magnific']:not(.inited):not(.prettyphoto):not([rel*='pretty']):not([data-rel*='pretty'])").addClass('inited');
        try {
            images.magnificPopup({
                type: 'image',
                mainClass: 'mfp-img-mobile',
                closeOnContentClick: true,
                closeBtnInside: true,
                fixedContentPos: true,
                midClick: true,
                //removalDelay: 500, 
                preloader: true,
                tLoading: THEMEREX_GLOBALS['strings']['magnific_loading'],
                gallery: {
                    enabled: THEMEREX_GLOBALS['popup_gallery']
                },
                image: {
                    tError: THEMEREX_GLOBALS['strings']['magnific_error'],
                    verticalFit: true
                }
            });
        } catch (e) {};
    }


    // Add hover icon to products thumbnails
    jQuery(".post_item_product .product .images a.woocommerce-main-image:not(.hover_icon)").addClass('hover_icon hover_icon_view');


    // Likes counter
    if (jQuery('.post_counters_likes:not(.inited)').length > 0) {
        jQuery('.post_counters_likes:not(.inited)')
            .addClass('inited')
            .click(function(e) {
                var button = jQuery(this);
                var inc = button.hasClass('enabled') ? 1 : -1;
                var post_id = button.data('postid');
                var likes = Number(button.data('likes')) + inc;
                var cookie_likes = themerex_get_cookie('themerex_likes');
                if (cookie_likes === undefined || cookie_likes === null) cookie_likes = '';
                jQuery.post(THEMEREX_GLOBALS['ajax_url'], {
                    action: 'post_counter',
                    nonce: THEMEREX_GLOBALS['ajax_nonce'],
                    post_id: post_id,
                    likes: likes
                }).done(function(response) {
                    var rez = JSON.parse(response);
                    if (rez.error === '') {
                        if (inc == 1) {
                            var title = button.data('title-dislike');
                            button.removeClass('enabled').addClass('disabled');
                            cookie_likes += (cookie_likes.substr(-1) != ',' ? ',' : '') + post_id + ',';
                        } else {
                            var title = button.data('title-like');
                            button.removeClass('disabled').addClass('enabled');
                            cookie_likes = cookie_likes.replace(',' + post_id + ',', ',');
                        }
                        button.data('likes', likes).attr('title', title).find('.post_counters_number').html(likes);
                        themerex_set_cookie('themerex_likes', cookie_likes, 365);
                    } else {
                        themerex_message_warning(THEMEREX_GLOBALS['strings']['error_like']);
                    }
                });
                e.preventDefault();
                return false;
            });
    }

    // Add video on thumb click

    setHomeVideo();

    // Tribe Events buttons
    jQuery('a.tribe-events-read-more,.tribe-events-button,.tribe-events-nav-previous a,.tribe-events-nav-next a,.tribe-events-widget-link a,.tribe-events-viewmore a').addClass('sc_button sc_button_style_filled');
}


function themerex_init_media_elements(cont) {
    if (THEMEREX_GLOBALS['media_elements_enabled'] && cont.find('audio,video').length > 0) {
        if (window.mejs) {
            window.mejs.MepDefaults.enableAutosize = false;
            window.mejs.MediaElementDefaults.enableAutosize = false;
            cont.find('audio:not(.wp-audio-shortcode),video:not(.wp-video-shortcode)').each(function() {
                if (jQuery(this).parents('.mejs-mediaelement').length == 0) {
                    var media_tag = jQuery(this);
                    var settings = {
                        enableAutosize: true,
                        videoWidth: -1, // if set, overrides <video width>
                        videoHeight: -1, // if set, overrides <video height>
                        audioWidth: '100%', // width of audio player
                        audioHeight: 30, // height of audio player
                        success: function(mejs) {
                            var autoplay, loop;
                            if ('flash' === mejs.pluginType) {
                                autoplay = mejs.attributes.autoplay && 'false' !== mejs.attributes.autoplay;
                                loop = mejs.attributes.loop && 'false' !== mejs.attributes.loop;
                                autoplay && mejs.addEventListener('canplay', function() {
                                    mejs.play();
                                }, false);
                                loop && mejs.addEventListener('ended', function() {
                                    mejs.play();
                                }, false);
                            }
                            media_tag.parents('.sc_audio,.sc_video').addClass('inited sc_show');
                        }
                    };
                    jQuery(this).mediaelementplayer(settings);
                }
            });
        } else
            setTimeout(function() {
                themerex_init_media_elements(cont);
            }, 400);
    }
}
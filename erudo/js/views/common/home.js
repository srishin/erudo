define(function(require) {
    "use strict";
    var tpl = require('text!../../../partials/common/home.html'),
        slider = require('owl'),
        player = require('mediaplay'),
        media = require('wpmedia'),


        template = _.template(tpl);
    return Backbone.View.extend({
        initialize: function() {},
        render: function() {
            this.$el.html(template());
            return this;
        }
    });
});

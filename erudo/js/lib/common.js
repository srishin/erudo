requirejs.config({
    baseUrl: "js",

    urlArgs: "bust=" + (new Date()).getTime(),
    stubModules: ['text'],
    paths: {
        underscore: "lib/underscore",
        backbone: "lib/backbone",
        jquery: 'theme/jquery-1.11.3',
        router: 'lib/router',

        // Theme JS
        isotope: 'theme/jquery.isotope.min',
        slider:'theme/slider',
        swiper: "theme/idangerous.swiper-2.7.min",
        // mapapi: "http://maps.google.com/maps/api/js?sensor=false",
        mapcore: 'theme/core.googlemap',
        mediaplay: 'theme/mediaelement-and-player.min',
        wpmedia: 'theme/wp-mediaelement.min',
        base: '/js/theme/themescripts',
        material: '/js/theme/custom.bootstrap',
        dtpicker: '/js/theme/bootstrap-datetimepicker.min',
        rating: '/js/theme/bootstrap-rating.min',
        moment: '/js/theme/moment.min',
        tagsin: '/js/theme/bootstrap-tagsinput',
        fullcalendar: '/js/theme/fullcalendar.min',
        owl: '/js/theme/owl.carousel.min',
        typeahead: '/js/theme/typeahead.bundle',
        bloodhound: '/js/theme/typeahead.bundle'
    },
    shim: {
        'backbone': {
            deps: ["underscore", "jquery", 'moment', 'typeahead', 'swiper'],
            exports: "Backbone" //attaches "Backbone" to the window object
        },
        'jquery': {
            exports: 'jQuery',
            // if this function returns false or undefined load the script from the cdn
            validate: function() {
                return window.jQuery;
            }
        },
        'base': {
            deps: ['backbone']
        },
        'wpmedia': {
            deps: ['mediaplay']
        },
        'typeahead': {
            deps: ['jquery'],
            init: function($) {
                return require.s.contexts._.registry['typeahead.js'].factory($);
            }
        },
        'bloodhound': {
            deps: ['jquery'],
            exports: 'Bloodhound'
        }

    }

});

define('main', function(require) {


    // This is the router.js file.  Note, it gets invoked before DOM is loaded


    require(['moment', 'material'], function(moment, material) {
        var Router = require('router');
        var router = new Router();
        Backbone.history.start();
    });



    //router.navigate("project/some-slug", {trigger:true})

});

// Start the application
require(['main']);

define(function(require) {

    // Dependencies
    var Backbone = require('backbone'),
        base = require('base'),
        content = $('#contentDiv'),
        nav = $('.menu_main_nav_area ul:not(.sub-menu)'),
        hideNav = function(userType) {
            nav.addClass('hide');
            $('ul#' + userType).removeClass('hide');
        };



    themerex_ready_actions();

    var router = Backbone.Router.extend({

        // Define routes
        routes: {
            "": "home",
            "Home": "home",
            "tutorsall": "tutors",
            // "feedback": "projects",
            "about-us": "about",
            "contact-us": "contact",
            "view-tutors": "tutors",
            "Advantages": "advantages",
            "login": "login",
            "register": "register",
            "courses": "course",
            "feedback": "feedback",
            "classroom": "classroom",
            "AddStudent": "AddStudent",
            "AddTutor": "AddTutor",
            // // Tutors
            "calendersessions": "createcourse",
            "studentRequests": "studentRequests",
            "requestDetails": "requestDetails",
            "viewmyfeed": "viewmyfeed",
            "TutorDashboard": "TutorDashboard",
            "doubts": "doubts",
            "answers": "answers",
            "personalpage": "personalpage",
            "startClass": "startClass",
            "tutorFeedback": "tutorFeedback",
            "classDetails": "classDetails",
            "courseTutor": "courseTutor",
            // "reqDetails": "reqDetails",
            // //Student
            "StudentEnrolled": "StudentEnrolled",
            "MyTutors": "MyTutors",
            "MyPayments": "MyPayments",
            "MyWallet": "MyWallet",
            "Request": "Request",
            "Cart": "Cart",
            "askdoubt": "askdoubt",
            "studentCalender": "StudentCalendar",
            
            // // Admin
            "Student": "Student",
            "Tutor": "Tutor",
            "Target": "Target",
            "Grade": "Grade",
            "Duration": "Duration",
            "Course": "Courseadmin",
            "Topic": "Topic",
            "Subject": "Subject",
            "CourseSessions": "CourseSessions",
            "ViewStudentFeedback": "ViewStudentFeedback",
            "ViewPayments": "ViewPayments",
            "testimonials": "testimonials",
            "approveFeed": "approveFeed",
        },


        // requirejs(['base'],function(base){

        // });



        home: function() {
            require(["views/common/home"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                // sliderInit();
                // themerex_resize_fullscreen_slider();
                view1.render();
                loadTheme();
                themerex_init_skills();
                setHomeVideo();
            });
        },
        about: function() {
            require(["views/common/about"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        contact: function() {
            require(["views/common/contact"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        tutors: function() {
            require(["views/common/tutors"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        advantages: function() {
            require(["views/common/advantages"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        login: function() {
            require(["views/common/login"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        register: function() {
            require(["views/common/register"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        course: function() {
            require(["views/common/course"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        classroom: function() {
            require(["views/common/classroom"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        feedback: function() {
            require(["views/feedback"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
                $('input').rating();
            });
        },
        // tables: function() {
        //     require(["views/tables"], function(rtlView) {
        //         var view1 = new rtlView({ el: content });
        //         view1.render();
        //         loadTheme();
        //     });
        // },
        AddStudent: function() {
            require(["views/AddStudent"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        AddTutor: function() {
            require(["views/AddTutor"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        // // Tutors 
        createcourse: function() {
            require(["views/tutor/createcourse"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        studentRequests: function() {
            require(["views/tutor/studentRequests"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        requestDetails: function() {
            require(["views/tutor/requestDetails"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        viewmyfeed: function() {
            require(["views/viewmyfeed"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        TutorDashboard: function() {
            require(["views/tutor/TutorDashboard"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        courseTutor: function() {
            require(["views/tutor/courseTutor"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        doubts: function() {
            require(["views/common/doubts"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        personalpage: function() {
            require(["views/personalpage"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        startClass: function() {
            require(["views/common/startClass"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        reqDetails: function() {
            require(["views/getTemplate"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        tutorFeedback: function() {
            require(["views/tutor/tutorFeedback"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        classDetails: function() {
            require(["views/tutor/classDetails"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        // Student
        StudentEnrolled: function() {
            require(["views/StudentEnrolled"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        }, 
        MyTutors: function() {
            require(["views/student/MyTutors"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        }, 
        StudentCalendar: function() {
            require(["views/student/StudentCalendar"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        Request: function() {
            require(["views/Request"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        Cart: function() {
            require(["views/student/Cart"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        askdoubt: function() {
            require(["views/common/askdoubt"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        answers: function() {
            require(["views/common/answers"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        // MyTeachers: function() {
        //     require(["views/MyTeachers"], function(rtlView) {
        //         var view1 = new rtlView({ el: content });
        //         view1.render();
        //         loadTheme();
        //     });
        // },
        MyPayments: function() {
            require(["views/student/MyPayments"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        MyWallet: function() {
            require(["views/student/MyWallet"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        // // Admin
        Target: function() {
            require(["views/admin/Target"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        Duration: function() {
            require(["views/admin/Duration"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        Grade: function() {
            require(["views/admin/Grade"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        Courseadmin: function() {
            require(["views/admin/Courseadmin"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        Topic: function() {
            require(["views/admin/Topic"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        Student: function() {
            require(["views/admin/Student"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        Tutor: function() {
            require(["views/admin/Tutor"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        Subject: function() {
            require(["views/admin/Subject"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        testimonials: function() {
            require(["views/admin/testimonials"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        },
        approveFeed: function() {
            require(["views/admin/approveFeed"], function(rtlView) {
                var view1 = new rtlView({ el: content });
                view1.render();
                loadTheme();
            });
        }
    });

    function loadTheme() {
        // console.log('Loading theme..');
        // themerex_ready_actions();
        themerex_init_actions();

        $('input.rating').rating();
        hideNav('root');
        // nav.addClass('hide');
        // $('ul#' + userType).removeClass('hide');
        $('div.preload').fadeOut(3000);
    };


    return router;

});

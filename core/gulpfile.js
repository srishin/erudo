var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    path = require('path'),
    expect = require('gulp-expect-file'),
    rename = require('gulp-rename'),
    cleanCSS = require('gulp-clean-css'),
    gulpFilter = require('gulp-filter'),
    gutil = require('gulp-util'),
    prettify = require('gulp-jsbeautifier'),
    webserver = require('gulp-webserver'),
    production = false;

var scripts = {
        theme: {
            source: [
                "themescripts/superfish.js",
                "themescripts/jquery.slidemenu.js",
                "themescripts/jquery.isotope.min.js",
                "themescripts/hover/jquery.hoverdir.js",
                "themescripts/core.utils.js",
                "themescripts/global.js",
                "themescripts/shortcodes/shortcodes.min.js",
                "themescripts/core.init.js"
            ],
            dest: '../erudo/js/theme',
            name: 'themescripts.js',
            watch: 'themescripts/*.js'
        },
        custom: {
            source: [
                "themescripts/material/material.min.js",
                "themescripts/material/ripples.min.js",
                "themescripts/material/bootstrap.min.js",
                "themescripts/material/bootstrap-rating.min.js",
                "themescripts/material/bootstrap-tagsinput.js"
            ],
            dest: '../erudo/js/theme',
            name: 'custom.bootstrap.js'
        }
    },

    styles = {
        vendor: {
            source: ["themecss/css/fontello/css/fontello.css",
                // "themescripts/rs-plugin/settings.css",
                "themecss/css/style.css",
                "themecss/css/shortcodes.css",
                "themecss/css/core.animation.css",
                "themecss/css/tribe-style.css",
                "themecss/css/skins/skin.css",
                "themecss/css/core.portfolio.css",
                "themescripts/mediaelement/mediaelementplayer.min.css",
                "themescripts/mediaelement/wp-mediaelement.css",
                "themescripts/swiper/idangerous.swiper.min.css",
                "themecss/css/slider-style.css",
                "themecss/css/custom-style.css"
            ],
            dest: '../erudo/css',
            watch: 'themecss/**/*.css',
            name: 'vendor.css'
        },
        responsive: {
            source: [
                "themecss/css/responsive.css",
                "themecss/css/skins/skin-responsive.css"
            ],
            dest: '../erudo/css',
            name: 'responsive.css'
        },
        material: {
            source: [
            'themecss/bootstrap/css/bootstrap.min.css',
                'themecss/bootstrap/css/bootstrap-tagsinput.css',
                'themecss/bootstrap/css/bootstrap-datetimepicker.min.css',
                'themecss/css/material/bootstrap-material-design.min.css',
                'themecss/css/material/ripples.min.css',
                "themecss/css/material/app.css"
            ],
            dest: '../erudo/css',
            name: 'material.css'
        }
    };

//################ TASKS #######################//
// Theme js
gulp.task('scripts:theme', function() {
    return gulp.src(scripts.theme.source)
        .pipe(expect(scripts.theme.source))
        .pipe(production ? uglify({ preserveComments: 'some' }) : prettify())
        .pipe(concat(scripts.theme.name))
        .pipe(gulp.dest(scripts.theme.dest));
});
// custom js
gulp.task('scripts:custom', function() {
    return gulp.src(scripts.custom.source)
        .pipe(expect(scripts.custom.source))
        .pipe(production ? uglify({ preserveComments: 'some' }) : prettify())
        .pipe(concat(scripts.custom.name))
        .pipe(gulp.dest(scripts.custom.dest));
});
// All Scripts 
gulp.task('scripts', ['scripts:theme', 'scripts:custom']);

/******************** CSS **********************/
// Theme css
gulp.task('styles:vendor', function() {
    return gulp.src(styles.vendor.source)
        .pipe(expect(styles.vendor.source))
        .pipe(production ? cleanCSS() : prettify())
        .pipe(concat(styles.vendor.name))
        .pipe(gulp.dest(styles.vendor.dest));

});
// Responsive css
gulp.task('styles:responsive', function() {
    return gulp.src(styles.responsive.source)
        .pipe(expect(styles.responsive.source))
        .pipe(production ? cleanCSS() : prettify())
        .pipe(concat(styles.responsive.name))
        .pipe(gulp.dest(styles.responsive.dest));

});
// Material css
gulp.task('styles:material', function() {
    return gulp.src(styles.material.source)
        .pipe(expect(styles.material.source))
        .pipe(production ? cleanCSS() : prettify())
        .pipe(concat(styles.material.name))
        .pipe(gulp.dest(styles.material.dest));

});
// All Styles 
gulp.task('styles', ['styles:vendor', 'styles:responsive', 'styles:material']);

// Watch changes
gulp.task('watch', function() {
    gulp.watch(scripts.theme.watch, ['scripts']);
    gulp.watch(styles.vendor.watch, ['styles']);
    gulp.watch([
        '../erudo/**',
    ]).on('change', function(event) {
        gutil.log(gutil.colors.cyan('************'));
        gutil.log(gutil.colors.cyan('* ' + event.path + ' *'), 'changed');
        gutil.log(gutil.colors.cyan('************'));
    });
});


// Serve web
gulp.task('serve', function() {
    gulp.src('../erudo/')
        .pipe(webserver({
            open: true,
            livereload: true,
            fallback: 'index.html'
        }));
});


gulp.task('default', ['scripts', 'styles', 'serve', 'watch']);
//Build minified version
gulp.task('build', ['prod', 'default']);
gulp.task('prod', function() { isProduction = true; });

/* global jQuery:false */
/* global THEMEREX_GLOBALS:false */


// Theme init actions
function themerex_init_actions() {
    "use strict";

    $.material.init();
    $.material.ripples();
    $.material.input();
    $.material.checkbox();
    $.material.radio();


    themerex_resize_actions();
    themerex_scroll_actions();

    // Resize handlers
    jQuery(window).resize(function() {
        "use strict";
        themerex_resize_actions();
    });

    // Scroll handlers
    jQuery(window).scroll(function() {
        "use strict";
        themerex_scroll_actions();
    });
}



// Theme first load actions
//==============================================
function themerex_ready_actions() {
    "use strict";

    // Call skin specific action (if exists)
    //----------------------------------------------
    if (window.themerex_skin_ready_actions) themerex_skin_ready_actions();








    // Media setup
    //----------------------------------------------

    // Video background init
    jQuery('.video_background').each(function() {
        var youtube = jQuery(this).data('youtube-code');
        if (youtube) {
            jQuery(this).tubular({ videoId: youtube });
        }
    });



    // Menu
    //----------------------------------------------

    // Clone main menu for responsive


    jQuery('.menu_main_wrap ul.menu_main_nav')
        .addClass('hide')
        .clone()
        .removeClass('menu_main_nav')
        .addClass('menu_main_responsive hide')
        .appendTo('span#responsiveMenu');

    // Responsive menu button
    jQuery('.menu_main_responsive_button').click(function(e) {
        "use strict";
        var id = jQuery('.menu_main_nav:not(.hide)').attr('id');
        jQuery('.menu_main_responsive#' + id).slideToggle();
        e.preventDefault();
        return false;
    });


    // Submenu click handler for the responsive menu
    jQuery('.menu_main_wrap .menu_main_responsive li a').click(function(e) {
        "use strict";
        if (jQuery('body').hasClass('responsive_menu') && jQuery(this).parent().hasClass('menu-item-has-children')) {
            if (jQuery(this).siblings('ul:visible').length > 0)
                jQuery(this).siblings('ul').slideUp().parent().removeClass('opened');
            else {
                jQuery(this).siblings('ul').slideDown().parent().addClass('opened');
            }

        }
        if (jQuery(this).attr('href') == '#') {
            e.preventDefault();
            if (jQuery(this).siblings('ul').length > 0) {
                if (jQuery(this).is('.opened'))
                    jQuery(this).siblings('ul').hide();
                else {
                    jQuery(this).siblings('ul').show();
                }
            }
            return false;
        }
        jQuery('.menu_main_responsive').hide();
    });

    // Init superfish menus
    themerex_init_sfmenu('.menu_main_wrap ul.menu_main_nav, .menu_user_wrap ul#menu_user');

    // Slide effect for main menu
    if (THEMEREX_GLOBALS['menu_slider']) {
        jQuery('.menu_main_nav').spasticNav({
            color: THEMEREX_GLOBALS['menu_color']
        });
    }


    // Store height of the top panel
    THEMEREX_GLOBALS['top_panel_height'] = 0; //Math.max(0, jQuery('.top_panel_wrap').height());


    // Pagination
    //----------------------------------------------

    // Page navigation (style slider)
    jQuery('.pager_cur').click(function(e) {
        "use strict";
        jQuery('.pager_slider').slideDown(300, function() {
            themerex_init_shortcodes(jQuery('.pager_slider').eq(0));
        });
        e.preventDefault();
        return false;
    });




    // Woocommerce
    //----------------------------------------------

    // Change display mode
    jQuery('.woocommerce .mode_buttons a,.woocommerce-page .mode_buttons a').click(function(e) {
        "use strict";
        var mode = jQuery(this).hasClass('woocommerce_thumbs') ? 'thumbs' : 'list';
        jQuery.cookie('themerex_shop_mode', mode, { expires: 365, path: '/' });
        jQuery(this).siblings('input').val(mode).parents('form').get(0).submit();
        e.preventDefault();
        return false;
    });


    // Scroll to top button
    jQuery('.scroll_to_top').click(function(e) {
        "use strict";
        jQuery('html,body').animate({
            scrollTop: 0
        }, 'slow');
        e.preventDefault();
        return false;
    });

    // Init post format specific scripts
    themerex_init_post_formats();

    // Init shortcodes scripts
    themerex_init_shortcodes(jQuery('body').eq(0));

    // Init hidden elements (if exists)
    if (window.themerex_init_hidden_elements) themerex_init_hidden_elements(jQuery('body').eq(0));

} //end ready




// Scroll actions
//==============================================

// Do actions when page scrolled
function themerex_scroll_actions() {
    "use strict";

    var scroll_offset = jQuery(window).scrollTop();
    var scroll_to_top_button = jQuery('.scroll_to_top');
    var adminbar_height = Math.max(0, jQuery('#wpadminbar').height());

    if (THEMEREX_GLOBALS['top_panel_height'] == 0) THEMEREX_GLOBALS['top_panel_height'] = jQuery('.top_panel_wrap').height();

    // Call skin specific action (if exists)
    //----------------------------------------------
    if (window.themerex_skin_scroll_actions) themerex_skin_scroll_actions();


    // Scroll to top button show/hide
    if (scroll_offset > THEMEREX_GLOBALS['top_panel_height'])
        scroll_to_top_button.addClass('show');
    else
        scroll_to_top_button.removeClass('show');

    // Fix/unfix top panel
    if (!jQuery('body').hasClass('responsive_menu') && THEMEREX_GLOBALS['menu_fixed']) {
        var slider_height = 0;
        if (jQuery('.top_panel_below .slider_wrap').length > 0) {
            slider_height = jQuery('.top_panel_below .slider_wrap').height();
            if (slider_height < 10) {
                slider_height = jQuery('.slider_wrap').hasClass('.slider_fullscreen') ? jQuery(window).height() : THEMEREX_GLOBALS['slider_height'];
            }
        }
        if (scroll_offset <= slider_height + THEMEREX_GLOBALS['top_panel_height']) {
            if (jQuery('body').hasClass('top_panel_fixed')) {
                jQuery('body').removeClass('top_panel_fixed');
            }
        } else if (scroll_offset > slider_height + THEMEREX_GLOBALS['top_panel_height']) {
            if (!jQuery('body').hasClass('top_panel_fixed')) {
                jQuery('.top_panel_fixed_wrap').height(THEMEREX_GLOBALS['top_panel_height']);
                jQuery('.top_panel_wrap').css('marginTop', '-150px').animate({ 'marginTop': 0 }, 500);
                jQuery('body').addClass('top_panel_fixed');
            }
        }
    }


    // Parallax scroll
    themerex_parallax_scroll();

    // Scroll actions for shortcodes
    themerex_animation_shortcodes();
}



// Parallax scroll
function themerex_parallax_scroll() {
    jQuery('.sc_parallax').each(function() {
        var windowHeight = jQuery(window).height();
        var scrollTops = jQuery(window).scrollTop();
        var offsetPrx = Math.max(jQuery(this).offset().top, windowHeight);
        if (offsetPrx <= scrollTops + windowHeight) {
            var speed = Number(jQuery(this).data('parallax-speed'));
            var xpos = jQuery(this).data('parallax-x-pos');
            var ypos = Math.round((offsetPrx - scrollTops - windowHeight) * speed + (speed < 0 ? windowHeight * speed : 0));
            jQuery(this).find('.sc_parallax_content').css('backgroundPosition', xpos + ' ' + ypos + 'px');
            // Uncomment next line if you want parallax video (else - video position is static)
            jQuery(this).find('div.sc_video_bg').css('top', ypos + 'px');
        }
    });
}





// Resize actions
//==============================================

// Do actions when page scrolled
function themerex_resize_actions() {
    "use strict";

    // Call skin specific action (if exists)
    //----------------------------------------------
    if (window.themerex_skin_resize_actions) themerex_skin_resize_actions();
    themerex_responsive_menu();
    themerex_video_dimensions();
    themerex_resize_video_background();
    themerex_resize_fullscreen_slider();
}


// Check window size and do responsive menu
// Check window size and do responsive menu
function themerex_responsive_menu() {
    if (themerex_is_responsive_need(THEMEREX_GLOBALS['menu_responsive'])) {
        if (!jQuery('body').hasClass('responsive_menu')) {
            jQuery('body').removeClass('top_panel_fixed').addClass('responsive_menu');
            if (jQuery('body').hasClass('menu_relayout'))
                jQuery('body').removeClass('menu_relayout menu_left').addClass('menu_right');
            if (jQuery('ul.menu_main_nav').hasClass('inited')) {
                jQuery('ul.menu_main_nav').removeClass('inited').superfish('destroy');
            }
        }
    } else {
        if (jQuery('body').hasClass('responsive_menu')) {
            jQuery('body').removeClass('responsive_menu');
            jQuery('.menu_main_responsive').hide();
            themerex_init_sfmenu('ul.menu_main_nav');
            jQuery('.menu_main_nav_area').show();
        }
        if (themerex_is_responsive_need(THEMEREX_GLOBALS['menu_relayout'])) {
            if (jQuery('body').hasClass('menu_right')) {
                jQuery('body').removeClass('menu_right').addClass('menu_relayout menu_left');
                //THEMEREX_GLOBALS['top_panel_height'] = Math.max(0, jQuery('.top_panel_wrap').height());
            }
        } else {
            if (jQuery('body').hasClass('menu_relayout')) {
                jQuery('body').removeClass('menu_relayout menu_left').addClass('menu_right');
                //THEMEREX_GLOBALS['top_panel_height'] = Math.max(0, jQuery('.top_panel_wrap').height());
            }
        }
    }
    if (!jQuery('.menu_main_wrap').hasClass('menu_show')) jQuery('.menu_main_wrap').addClass('menu_show');
}



// Check if responsive menu need
function themerex_is_responsive_need(max_width) {
    "use strict";
    var rez = false;
    if (max_width > 0) {
        var w = window.innerWidth;
        if (w == undefined) {
            w = jQuery(window).width() + (jQuery(window).height() < jQuery(document).height() || jQuery(window).scrollTop() > 0 ? 16 : 0);
        }
        rez = max_width > w;
    }
    return rez;
}



// Fit video frames to document width
function themerex_video_dimensions() {
    jQuery('.sc_video_frame').each(function() {
        "use strict";
        var frame = jQuery(this).eq(0);
        var player = frame.parent();
        var ratio = (frame.data('ratio') ? frame.data('ratio').split(':') : (frame.find('[data-ratio]').length > 0 ? frame.find('[data-ratio]').data('ratio').split(':') : [16, 9]));
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var w_attr = frame.data('width');
        var h_attr = frame.data('height');
        if (!w_attr || !h_attr) {
            return;
        }
        var percent = ('' + w_attr).substr(-1) == '%';
        w_attr = parseInt(w_attr);
        h_attr = parseInt(h_attr);
        var w_real = Math.min(percent ? 10000 : w_attr, frame.parents('div,article').width()), //player.width();
            h_real = Math.round(percent ? w_real / ratio : w_real / w_attr * h_attr);
        if (parseInt(frame.attr('data-last-width')) == w_real) return;
        if (percent) {
            frame.height(h_real);
        } else {
            frame.css({ 'width': w_real + 'px', 'height': h_real + 'px' });
        }
        frame.attr('data-last-width', w_real);
    });
    jQuery('video.sc_video,video.wp-video-shortcode').each(function() {
        "use strict";
        var video = jQuery(this).eq(0);
        var ratio = (video.data('ratio') != undefined ? video.data('ratio').split(':') : [16, 9]);
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var mejs_cont = video.parents('.mejs-video');
        var frame = video.parents('.sc_video_frame');
        var w_attr = frame.length > 0 ? frame.data('width') : video.data('width');
        var h_attr = frame.length > 0 ? frame.data('height') : video.data('height');
        if (!w_attr || !h_attr) {
            w_attr = video.attr('width');
            h_attr = video.attr('height');
            if (!w_attr || !h_attr) return;
            video.data({ 'width': w_attr, 'height': h_attr });
        }
        var percent = ('' + w_attr).substr(-1) == '%';
        w_attr = parseInt(w_attr);
        h_attr = parseInt(h_attr);
        var w_real = Math.round(mejs_cont.length > 0 ? Math.min(percent ? 10000 : w_attr, mejs_cont.parents('div,article').width()) : video.width()),
            h_real = Math.round(percent ? w_real / ratio : w_real / w_attr * h_attr);
        if (parseInt(video.attr('data-last-width')) == w_real) return;
        if (mejs_cont.length > 0 && mejs) {
            themerex_set_mejs_player_dimensions(video, w_real, h_real);
        }
        if (percent) {
            video.height(h_real);
        } else {
            video.attr({ 'width': w_real, 'height': h_real }).css({ 'width': w_real + 'px', 'height': h_real + 'px' });
        }
        video.attr('data-last-width', w_real);
    });
    jQuery('video.sc_video_bg').each(function() {
        "use strict";
        var video = jQuery(this).eq(0);
        var ratio = (video.data('ratio') != undefined ? video.data('ratio').split(':') : [16, 9]);
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var mejs_cont = video.parents('.mejs-video');
        var container = mejs_cont.length > 0 ? mejs_cont.parent() : video.parent();
        var w = container.width();
        var h = container.height();
        var w1 = Math.ceil(h * ratio);
        var h1 = Math.ceil(w / ratio);
        if (video.parents('.sc_parallax').length > 0) {
            var windowHeight = jQuery(window).height();
            var speed = Number(video.parents('.sc_parallax').data('parallax-speed'));
            var h_add = Math.ceil(Math.abs((windowHeight - h) * speed));
            if (h1 < h + h_add) {
                h1 = h + h_add;
                w1 = Math.ceil(h1 * ratio);
            }
        }
        if (h1 < h) {
            h1 = h;
            w1 = Math.ceil(h1 * ratio);
        }
        if (w1 < w) {
            w1 = w;
            h1 = Math.ceil(w1 / ratio);
        }
        var l = Math.round((w1 - w) / 2);
        var t = Math.round((h1 - h) / 2);
        if (parseInt(video.attr('data-last-width')) == w1) return;
        if (mejs_cont.length > 0) {
            themerex_set_mejs_player_dimensions(video, w1, h1);
            mejs_cont.css({ 'left': -l + 'px', 'top': -t + 'px' });
        } else
            video.css({ 'left': -l + 'px', 'top': -t + 'px' });
        video.attr({ 'width': w1, 'height': h1, 'data-last-width': w1 }).css({ 'width': w1 + 'px', 'height': h1 + 'px' });
        if (video.css('opacity') == 0) video.animate({ 'opacity': 1 }, 3000);
    });
    jQuery('iframe').each(function() {
        "use strict";
        var iframe = jQuery(this).eq(0);
        var ratio = (iframe.data('ratio') != undefined ? iframe.data('ratio').split(':') : (iframe.find('[data-ratio]').length > 0 ? iframe.find('[data-ratio]').data('ratio').split(':') : [16, 9]));
        ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
        var w_attr = iframe.attr('width');
        var h_attr = iframe.attr('height');
        var frame = iframe.parents('.sc_video_frame');
        if (frame.length > 0) {
            w_attr = frame.data('width');
            h_attr = frame.data('height');
        }
        if (!w_attr || !h_attr) {
            return;
        }
        var percent = ('' + w_attr).substr(-1) == '%';
        w_attr = parseInt(w_attr);
        h_attr = parseInt(h_attr);
        var w_real = frame.length > 0 ? frame.width() : iframe.width(),
            h_real = Math.round(percent ? w_real / ratio : w_real / w_attr * h_attr);
        if (parseInt(iframe.attr('data-last-width')) == w_real) return;
        iframe.css({ 'width': w_real + 'px', 'height': h_real + 'px' });
    });
}

// Resize fullscreen video background
function themerex_resize_video_background() {
    var bg = jQuery('.video_bg');
    if (bg.length < 1)
        return;
    if (THEMEREX_GLOBALS['media_elements_enabled'] && bg.find('.mejs-video').length == 0) {
        setTimeout(themerex_resize_video_background, 100);
        return;
    }
    var video = bg.find('video');
    var ratio = (video.data('ratio') != undefined ? video.data('ratio').split(':') : [16, 9]);
    ratio = ratio.length != 2 || ratio[0] == 0 || ratio[1] == 0 ? 16 / 9 : ratio[0] / ratio[1];
    var w = bg.width();
    var h = bg.height();
    var w1 = Math.ceil(h * ratio);
    var h1 = Math.ceil(w / ratio);
    if (h1 < h) {
        h1 = h;
        w1 = Math.ceil(h1 * ratio);
    }
    if (w1 < w) {
        w1 = w;
        h1 = Math.ceil(w1 / ratio);
    }
    var l = Math.round((w1 - w) / 2);
    var t = Math.round((h1 - h) / 2);
    if (bg.find('.mejs-container').length > 0) {
        themerex_set_mejs_player_dimensions(bg.find('video'), w1, h1);
        bg.find('.mejs-container').css({ 'left': -l + 'px', 'top': -t + 'px' });
    } else
        bg.find('video').css({ 'left': -l + 'px', 'top': -t + 'px' });
    bg.find('video').attr({ 'width': w1, 'height': h1 }).css({ 'width': w1 + 'px', 'height': h1 + 'px' });
}

// Set Media Elements player dimensions
function themerex_set_mejs_player_dimensions(video, w, h) {
    if (mejs) {
        for (var pl in mejs.players) {
            if (mejs.players[pl].media.src == video.attr('src')) {
                if (mejs.players[pl].media.setVideoSize) {
                    mejs.players[pl].media.setVideoSize(w, h);
                }
                mejs.players[pl].setPlayerSize(w, h);
                mejs.players[pl].setControlsSize();
                //var mejs_cont = video.parents('.mejs-video');
                //mejs_cont.css({'width': w+'px', 'height': h+'px'}).find('.mejs-layers > div, .mejs-overlay, .mejs-poster').css({'width': w, 'height': h});
            }
        }
    }
}

// Resize Fullscreen Slider
function themerex_resize_fullscreen_slider() {
    var slider_wrap = jQuery('.slider_wrap.slider_fullscreen');
    if (slider_wrap.length < 1)
        return;
    var slider = slider_wrap.find('.sc_slider_swiper');
    if (slider.length < 1)
        return;
    var h = jQuery(window).height() - jQuery('#wpadminbar').height() - (jQuery('body').hasClass('top_panel_above') && !jQuery('body').hasClass('.top_panel_fixed') ? jQuery('.top_panel_wrap').height() : 0);
    slider.height(h);
}





// Navigation
//==============================================

// Init Superfish menu
function themerex_init_sfmenu(selector) {
    jQuery(selector).show().each(function() {
        if (themerex_is_responsive_need() && jQuery(this).attr('id') == 'menu_main') return;
        jQuery(this).superfish({
            delay: 500,
            animation: {
                opacity: 'show'
            },
            animationOut: {
                opacity: 'hide'
            },
            speed: THEMEREX_GLOBALS['css_animation'] ? 500 : (THEMEREX_GLOBALS['menu_slider'] ? 300 : 200),
            speedOut: THEMEREX_GLOBALS['css_animation'] ? 500 : (THEMEREX_GLOBALS['menu_slider'] ? 300 : 200),
            autoArrows: false,
            dropShadows: false,
            onBeforeShow: function(ul) {
                if (jQuery(this).parents("ul").length > 1) {
                    var w = jQuery(window).width();
                    var par_offset = jQuery(this).parents("ul").offset().left;
                    var par_width = jQuery(this).parents("ul").outerWidth();
                    var ul_width = jQuery(this).outerWidth();
                    if (par_offset + par_width + ul_width > w - 20 && par_offset - ul_width > 0)
                        jQuery(this).addClass('submenu_left');
                    else
                        jQuery(this).removeClass('submenu_left');
                }
                if (THEMEREX_GLOBALS['css_animation']) {
                    jQuery(this).removeClass('animated fast ' + THEMEREX_GLOBALS['menu_animation_out']);
                    jQuery(this).addClass('animated fast ' + THEMEREX_GLOBALS['menu_animation_in']);
                }
            },
            onBeforeHide: function(ul) {
                if (THEMEREX_GLOBALS['css_animation']) {
                    jQuery(this).removeClass('animated fast ' + THEMEREX_GLOBALS['menu_animation_in']);
                    jQuery(this).addClass('animated fast ' + THEMEREX_GLOBALS['menu_animation_out']);
                }
            }
        });
    });
}





// Isotope
//=====================================================

// First init isotope containers
function themerex_init_isotope() {
    "use strict";

    var all_images_complete = true;

    // Check if all images in isotope wrapper are loaded
    jQuery('.isotope_wrap:not(.inited)').each(function() {
        "use strict";
        all_images_complete = all_images_complete && themerex_check_images_complete(jQuery(this));
    });
    // Wait for images loading
    if (!all_images_complete && THEMEREX_GLOBALS['isotope_init_counter']++ < 30) {
        setTimeout(themerex_init_isotope, 200);
        return;
    }

    // Isotope filters handler
    jQuery('.isotope_filters:not(.inited)').addClass('inited').on('click', 'a', function(e) {
        "use strict";
        jQuery(this).parents('.isotope_filters').find('a').removeClass('active');
        jQuery(this).addClass('active');

        var selector = jQuery(this).data('filter');
        jQuery(this).parents('.isotope_filters').siblings('.isotope_wrap').eq(0).isotope({
            filter: selector
        });

        if (selector == '*')
            jQuery('#viewmore_link').fadeIn();
        else
            jQuery('#viewmore_link').fadeOut();

        e.preventDefault();
        return false;
    });

    // Init isotope script
    jQuery('.isotope_wrap:not(.inited)').each(function() {
        "use strict";

        var isotope_container = jQuery(this);

        // Init shortcodes
        themerex_init_shortcodes(isotope_container);

        // If in scroll container - no init isotope
        if (isotope_container.parents('.sc_scroll').length > 0) {
            isotope_container.addClass('inited').find('.isotope_item').animate({ opacity: 1 }, 200, function() { jQuery(this).addClass('isotope_item_show'); });
            return;
        }

        // Init isotope with timeout
        // setTimeout(function() {
        //  isotope_container.addClass('inited').isotope({
        //      itemSelector: '.isotope_item',
        //      animationOptions: {
        //          duration: 750,
        //          easing: 'linear',
        //          queue: false
        //      }
        //  });

        //  // Show elements
        //  isotope_container.find('.isotope_item').animate({opacity: 1}, 200, function () { 
        //      jQuery(this).addClass('isotope_item_show'); 
        //  });

        // }, 500);

    });
}

function themerex_init_appended_isotope(posts_container, filters) {
    "use strict";

    if (posts_container.parents('.sc_scroll_horizontal').length > 0) return;

    if (!themerex_check_images_complete(posts_container) && THEMEREX_GLOBALS['isotope_init_counter']++ < 30) {
        setTimeout(function() { themerex_init_appended_isotope(posts_container, filters); }, 200);
        return;
    }
    // Add filters
    var flt = posts_container.siblings('.isotope_filter');
    for (var i in filters) {
        if (flt.find('a[data-filter=".flt_' + i + '"]').length == 0) {
            flt.append('<a href="#" class="isotope_filters_button" data-filter=".flt_' + i + '">' + filters[i] + '</a>');
        }
    }
    // Init shortcodes in added elements
    themerex_init_shortcodes(posts_container);
    // Get added elements
    var elems = posts_container.find('.isotope_item:not(.isotope_item_show)');
    // Notify isotope about added elements with timeout
    setTimeout(function() {
        posts_container.isotope('appended', elems);
        // Show appended elements
        elems.animate({ opacity: 1 }, 200, function() { jQuery(this).addClass('isotope_item_show'); });
    }, 500);
}



// Post formats init
//=====================================================
function setHomeVideo() {
    if (jQuery('.sc_video_play_button:not(.inited)').length > 0) {
        jQuery('.sc_video_play_button:not(.inited)').each(function() {
            "use strict";
            jQuery(this)
                .addClass('inited')
                .animate({ opacity: 1 }, 1000)
                .click(function(e) {
                    "use strict";
                    if (!jQuery(this).hasClass('sc_video_play_button')) return;
                    var video = jQuery(this).removeClass('sc_video_play_button hover_icon_play').data('video');
                    if (video !== '') {
                        jQuery(this).empty().html(video);
                        themerex_video_dimensions();
                        var video_tag = jQuery(this).find('video');
                        var w = video_tag.width();
                        var h = video_tag.height();
                        themerex_init_media_elements(jQuery(this));
                        // Restore WxH attributes, because Chrome broke it!
                        jQuery(this).find('video').css({ 'width': w, 'height': h }).attr({ 'width': w, 'height': h });
                    }
                    e.preventDefault();
                    return false;
                });
        });
    }
}

function themerex_init_post_formats() {
    "use strict";

    // MediaElement init
    themerex_init_media_elements(jQuery('body'));

    // Isotope first init
    if (jQuery('.isotope_wrap:not(.inited)').length > 0) {
        THEMEREX_GLOBALS['isotope_init_counter'] = 0;
        themerex_init_isotope();
    }

    // Hover Effect 'Dir'
    if (jQuery('.isotope_wrap .isotope_item_content.square.effect_dir:not(.inited)').length > 0) {
        jQuery('.isotope_wrap .isotope_item_content.square.effect_dir:not(.inited)').each(function() {
            jQuery(this).addClass('inited').hoverdir();
        });
    }

    // Popup init
    if (THEMEREX_GLOBALS['popup_engine'] == 'pretty') {
        jQuery("a[href$='jpg'],a[href$='jpeg'],a[href$='png'],a[href$='gif']").attr('rel', 'prettyPhoto' + (THEMEREX_GLOBALS['popup_gallery'] ? '[slideshow]' : ''));
        var images = jQuery("a[rel*='prettyPhoto']:not(.inited):not([data-rel*='pretty']):not([rel*='magnific']):not([data-rel*='magnific'])").addClass('inited');
        try {
            images.prettyPhoto({
                social_tools: '',
                theme: 'facebook',
                deeplinking: false
            });
        } catch (e) {};
    } else if (THEMEREX_GLOBALS['popup_engine'] == 'magnific') {
        jQuery("a[href$='jpg'],a[href$='jpeg'],a[href$='png'],a[href$='gif']").attr('rel', 'magnific');
        var images = jQuery("a[rel*='magnific']:not(.inited):not(.prettyphoto):not([rel*='pretty']):not([data-rel*='pretty'])").addClass('inited');
        try {
            images.magnificPopup({
                type: 'image',
                mainClass: 'mfp-img-mobile',
                closeOnContentClick: true,
                closeBtnInside: true,
                fixedContentPos: true,
                midClick: true,
                //removalDelay: 500, 
                preloader: true,
                tLoading: THEMEREX_GLOBALS['strings']['magnific_loading'],
                gallery: {
                    enabled: THEMEREX_GLOBALS['popup_gallery']
                },
                image: {
                    tError: THEMEREX_GLOBALS['strings']['magnific_error'],
                    verticalFit: true
                }
            });
        } catch (e) {};
    }


    // Add hover icon to products thumbnails
    jQuery(".post_item_product .product .images a.woocommerce-main-image:not(.hover_icon)").addClass('hover_icon hover_icon_view');


    // Likes counter
    if (jQuery('.post_counters_likes:not(.inited)').length > 0) {
        jQuery('.post_counters_likes:not(.inited)')
            .addClass('inited')
            .click(function(e) {
                var button = jQuery(this);
                var inc = button.hasClass('enabled') ? 1 : -1;
                var post_id = button.data('postid');
                var likes = Number(button.data('likes')) + inc;
                var cookie_likes = themerex_get_cookie('themerex_likes');
                if (cookie_likes === undefined || cookie_likes === null) cookie_likes = '';
                jQuery.post(THEMEREX_GLOBALS['ajax_url'], {
                    action: 'post_counter',
                    nonce: THEMEREX_GLOBALS['ajax_nonce'],
                    post_id: post_id,
                    likes: likes
                }).done(function(response) {
                    var rez = JSON.parse(response);
                    if (rez.error === '') {
                        if (inc == 1) {
                            var title = button.data('title-dislike');
                            button.removeClass('enabled').addClass('disabled');
                            cookie_likes += (cookie_likes.substr(-1) != ',' ? ',' : '') + post_id + ',';
                        } else {
                            var title = button.data('title-like');
                            button.removeClass('disabled').addClass('enabled');
                            cookie_likes = cookie_likes.replace(',' + post_id + ',', ',');
                        }
                        button.data('likes', likes).attr('title', title).find('.post_counters_number').html(likes);
                        themerex_set_cookie('themerex_likes', cookie_likes, 365);
                    } else {
                        themerex_message_warning(THEMEREX_GLOBALS['strings']['error_like']);
                    }
                });
                e.preventDefault();
                return false;
            });
    }

    // Add video on thumb click

    setHomeVideo();

    // Tribe Events buttons
    jQuery('a.tribe-events-read-more,.tribe-events-button,.tribe-events-nav-previous a,.tribe-events-nav-next a,.tribe-events-widget-link a,.tribe-events-viewmore a').addClass('sc_button sc_button_style_filled');
}


function themerex_init_media_elements(cont) {
    if (THEMEREX_GLOBALS['media_elements_enabled'] && cont.find('audio,video').length > 0) {
        if (window.mejs) {
            window.mejs.MepDefaults.enableAutosize = false;
            window.mejs.MediaElementDefaults.enableAutosize = false;
            cont.find('audio:not(.wp-audio-shortcode),video:not(.wp-video-shortcode)').each(function() {
                if (jQuery(this).parents('.mejs-mediaelement').length == 0) {
                    var media_tag = jQuery(this);
                    var settings = {
                        enableAutosize: true,
                        videoWidth: -1, // if set, overrides <video width>
                        videoHeight: -1, // if set, overrides <video height>
                        audioWidth: '100%', // width of audio player
                        audioHeight: 30, // height of audio player
                        success: function(mejs) {
                            var autoplay, loop;
                            if ('flash' === mejs.pluginType) {
                                autoplay = mejs.attributes.autoplay && 'false' !== mejs.attributes.autoplay;
                                loop = mejs.attributes.loop && 'false' !== mejs.attributes.loop;
                                autoplay && mejs.addEventListener('canplay', function() {
                                    mejs.play();
                                }, false);
                                loop && mejs.addEventListener('ended', function() {
                                    mejs.play();
                                }, false);
                            }
                            media_tag.parents('.sc_audio,.sc_video').addClass('inited sc_show');
                        }
                    };
                    jQuery(this).mediaelementplayer(settings);
                }
            });
        } else
            setTimeout(function() { themerex_init_media_elements(cont); }, 400);
    }
}
